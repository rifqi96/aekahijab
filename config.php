<?php
// HTTP
define('HTTP_SERVER', 'http://q-dev.ga/hijab.com/');

// HTTPS
define('HTTPS_SERVER', 'http://q-dev.ga/hijab.com/');

// DIR
define('DIR_APPLICATION', '/Applications/XAMPP/htdocs/hijab.com/catalog/');
define('DIR_SYSTEM', '/Applications/XAMPP/htdocs/hijab.com/system/');
define('DIR_LANGUAGE', '/Applications/XAMPP/htdocs/hijab.com/catalog/language/');
define('DIR_TEMPLATE', '/Applications/XAMPP/htdocs/hijab.com/catalog/view/theme/');
define('DIR_CONFIG', '/Applications/XAMPP/htdocs/hijab.com/system/config/');
define('DIR_IMAGE', '/Applications/XAMPP/htdocs/hijab.com/image/');
define('DIR_CACHE', '/Applications/XAMPP/htdocs/hijab.com/system/cache/');
define('DIR_DOWNLOAD', '/Applications/XAMPP/htdocs/hijab.com/system/download/');
define('DIR_UPLOAD', '/Applications/XAMPP/htdocs/hijab.com/system/upload/');
define('DIR_MODIFICATION', '/Applications/XAMPP/htdocs/hijab.com/system/modification/');
define('DIR_LOGS', '/Applications/XAMPP/htdocs/hijab.com/system/logs/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'hijab');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
