<div id="rgen-productgroups-rgenz9EIG1" class="rgen-productgroups productgroups-rgiuq">
	<div class="mod-wrp container">
	
			<h3 class="mod-hd">Product groups module</h3>
		
		<section class="mod-customhtml t-html">Lorem ipsum dolor sit amet, consetetur sadipscing elitr<br>
sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.</section>	
		
	<div class="rw gt40">
				
		<div class="m-cl cl12 t-xl12">
			<div class="mod-content">
				
				<div class="tab-container widget-tab widget-tab-1" data-widget="autoTab">
					<ul class="tab-panel ul-reset">
												<li class="tab-item">
							<a>New arrivals</a>
						</li>
												<li class="tab-item">
							<a>Special offers</a>
						</li>
												<li class="tab-item">
							<a>Category products</a>
						</li>
												<li class="tab-item">
							<a>Brand products</a>
						</li>
											</ul>			
					<div class="tab-pane-wrp">
												<div class="tab-pane">
							
														<div 
								id            ="carousel-rgen-productgroups-rgenz9EIG1_0"
								class         ="widget-carousel ctrl-b" 
								data-stpd     ="1" 
								data-nav      ="true" 
								data-dots     ="false" 
								data-resitems ="4" 
								data-res      ="true" 
								data-margin   ="10">
																								<div class="item">
									<div class="prd-block prd-block3" id="prd-block3-rgendndwHs">
	
	
					
	
	<div class="image">
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=49&urltype=quickview" data-toggle="tooltip" title="Quick view" class="quickview-link grid-on list-on normal-on"><i class="fa fa-eye"></i> Quick view</a>
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=49"><img src="http://localhost/hijab.com/image/cache/catalog/demo/samsung_tab_1-300x300.jpg" alt="Samsung Galaxy Tab 10.1" title="Samsung Galaxy Tab 10.1" class="img-responsive" /></a>
				<div class="countdown"></div>
			</div>
	<div class="info-wrp">
		<div class="info-content">
			<h4 class="name"><a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=49">Samsung Galaxy Tab 10.1</a></h4>
			<p class="info grid-off list-on normal-off">
	Samsung Galaxy Tab 10.1, is the world&rsquo;s thinnest tablet, measuring 8.6 mm thickness, runnin..</p>	
		</div>

		<div class="price-wrp">
						<div class="price grid-on list-on normal-on">
								<span class="price-old" style="text-decoration: none;">&nbsp;</span>
				<span class="price-new">Rp 200</span>
												<span class="price-tax grid-on list-on normal-on">Ex Tax: Rp 200</span>
							</div>
								</div>

	</div>

	<div class="btn-wrp grid-on list-on normal-on grid-off list-on normal-off grid-off list-on normal-off">
		<a class="btn btn-cart grid-on list-on normal-on" title="Add to Cart" onclick="cart.add('49', '1');">Add to Cart</a>
		
		<div class="other-btn-wrp">
			<a class="vm btn btn-wish grid-off list-on normal-off" data-toggle="tooltip" title="Add to wish list" onclick="wishlist.add('49');"><i class="fa fa-heart"></i></a>

			<a class="vm btn btn-compare grid-off list-on normal-off" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('49');"><i class="fa fa-retweet"></i></a>	
		</div>
		
	</div>
</div>

								</div>
																<div class="item">
									<div class="prd-block prd-block3" id="prd-block3-rgenw7bbsM">
	
	
					
	
	<div class="image">
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=48&urltype=quickview" data-toggle="tooltip" title="Quick view" class="quickview-link grid-on list-on normal-on"><i class="fa fa-eye"></i> Quick view</a>
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=48"><img src="http://localhost/hijab.com/image/cache/catalog/demo/ipod_classic_1-300x300.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive" /></a>
				<div class="countdown"></div>
			</div>
	<div class="info-wrp">
		<div class="info-content">
			<h4 class="name"><a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=48">iPod Classic</a></h4>
			<p class="info grid-off list-on normal-off">
	
		
			More room to move.
		
			With 80GB or 160GB of storage and up to 40 hours of battery l..</p>	
		</div>

		<div class="price-wrp">
						<div class="price grid-on list-on normal-on">
								<span class="price-old" style="text-decoration: none;">&nbsp;</span>
				<span class="price-new">Rp 100</span>
												<span class="price-tax grid-on list-on normal-on">Ex Tax: Rp 100</span>
							</div>
								</div>

	</div>

	<div class="btn-wrp grid-on list-on normal-on grid-off list-on normal-off grid-off list-on normal-off">
		<a class="btn btn-cart grid-on list-on normal-on" title="Add to Cart" onclick="cart.add('48', '1');">Add to Cart</a>
		
		<div class="other-btn-wrp">
			<a class="vm btn btn-wish grid-off list-on normal-off" data-toggle="tooltip" title="Add to wish list" onclick="wishlist.add('48');"><i class="fa fa-heart"></i></a>

			<a class="vm btn btn-compare grid-off list-on normal-off" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('48');"><i class="fa fa-retweet"></i></a>	
		</div>
		
	</div>
</div>

								</div>
																<div class="item">
									<div class="prd-block prd-block3" id="prd-block3-rgengglY2V">
	
	
					
	
	<div class="image">
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=47&urltype=quickview" data-toggle="tooltip" title="Quick view" class="quickview-link grid-on list-on normal-on"><i class="fa fa-eye"></i> Quick view</a>
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=47"><img src="http://localhost/hijab.com/image/cache/catalog/demo/hp_1-300x300.jpg" alt="HP LP3065" title="HP LP3065" class="img-responsive" /></a>
				<div class="countdown"></div>
			</div>
	<div class="info-wrp">
		<div class="info-content">
			<h4 class="name"><a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=47">HP LP3065</a></h4>
			<p class="info grid-off list-on normal-off">
	Stop your co-workers in their tracks with the stunning new 30-inch diagonal HP LP3065 Flat Panel ..</p>	
		</div>

		<div class="price-wrp">
						<div class="price grid-on list-on normal-on">
								<span class="price-old" style="text-decoration: none;">&nbsp;</span>
				<span class="price-new">Rp 100</span>
												<span class="price-tax grid-on list-on normal-on">Ex Tax: Rp 100</span>
							</div>
								</div>

	</div>

	<div class="btn-wrp grid-on list-on normal-on grid-off list-on normal-off grid-off list-on normal-off">
		<a class="btn btn-cart grid-on list-on normal-on" title="Add to Cart" onclick="cart.add('47', '1');">Add to Cart</a>
		
		<div class="other-btn-wrp">
			<a class="vm btn btn-wish grid-off list-on normal-off" data-toggle="tooltip" title="Add to wish list" onclick="wishlist.add('47');"><i class="fa fa-heart"></i></a>

			<a class="vm btn btn-compare grid-off list-on normal-off" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('47');"><i class="fa fa-retweet"></i></a>	
		</div>
		
	</div>
</div>

								</div>
																<div class="item">
									<div class="prd-block prd-block3" id="prd-block3-rgen1E5SRi">
	
	
					
	
	<div class="image">
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=46&urltype=quickview" data-toggle="tooltip" title="Quick view" class="quickview-link grid-on list-on normal-on"><i class="fa fa-eye"></i> Quick view</a>
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=46"><img src="http://localhost/hijab.com/image/cache/catalog/demo/sony_vaio_1-300x300.jpg" alt="Sony VAIO" title="Sony VAIO" class="img-responsive" /></a>
				<div class="countdown"></div>
			</div>
	<div class="info-wrp">
		<div class="info-content">
			<h4 class="name"><a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=46">Sony VAIO</a></h4>
			<p class="info grid-off list-on normal-off">
	Unprecedented power. The next generation of processing technology has arrived. Built into the new..</p>	
		</div>

		<div class="price-wrp">
						<div class="price grid-on list-on normal-on">
								<span class="price-old" style="text-decoration: none;">&nbsp;</span>
				<span class="price-new">Rp 1,000</span>
												<span class="price-tax grid-on list-on normal-on">Ex Tax: Rp 1,000</span>
							</div>
								</div>

	</div>

	<div class="btn-wrp grid-on list-on normal-on grid-off list-on normal-off grid-off list-on normal-off">
		<a class="btn btn-cart grid-on list-on normal-on" title="Add to Cart" onclick="cart.add('46', '1');">Add to Cart</a>
		
		<div class="other-btn-wrp">
			<a class="vm btn btn-wish grid-off list-on normal-off" data-toggle="tooltip" title="Add to wish list" onclick="wishlist.add('46');"><i class="fa fa-heart"></i></a>

			<a class="vm btn btn-compare grid-off list-on normal-off" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('46');"><i class="fa fa-retweet"></i></a>	
		</div>
		
	</div>
</div>

								</div>
																<div class="item">
									<div class="prd-block prd-block3" id="prd-block3-rgenxlGTX2">
	
	
					
	
	<div class="image">
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=45&urltype=quickview" data-toggle="tooltip" title="Quick view" class="quickview-link grid-on list-on normal-on"><i class="fa fa-eye"></i> Quick view</a>
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=45"><img src="http://localhost/hijab.com/image/cache/catalog/demo/macbook_pro_1-300x300.jpg" alt="MacBook Pro" title="MacBook Pro" class="img-responsive" /></a>
				<div class="countdown"></div>
			</div>
	<div class="info-wrp">
		<div class="info-content">
			<h4 class="name"><a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=45">MacBook Pro</a></h4>
			<p class="info grid-off list-on normal-off">
	
		
			Latest Intel mobile architecture
		
			Powered by the most advanced mobile processors ..</p>	
		</div>

		<div class="price-wrp">
						<div class="price grid-on list-on normal-on">
								<span class="price-old" style="text-decoration: none;">&nbsp;</span>
				<span class="price-new">Rp 2,000</span>
												<span class="price-tax grid-on list-on normal-on">Ex Tax: Rp 2,000</span>
							</div>
								</div>

	</div>

	<div class="btn-wrp grid-on list-on normal-on grid-off list-on normal-off grid-off list-on normal-off">
		<a class="btn btn-cart grid-on list-on normal-on" title="Add to Cart" onclick="cart.add('45', '1');">Add to Cart</a>
		
		<div class="other-btn-wrp">
			<a class="vm btn btn-wish grid-off list-on normal-off" data-toggle="tooltip" title="Add to wish list" onclick="wishlist.add('45');"><i class="fa fa-heart"></i></a>

			<a class="vm btn btn-compare grid-off list-on normal-off" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('45');"><i class="fa fa-retweet"></i></a>	
		</div>
		
	</div>
</div>

								</div>
																<div class="item">
									<div class="prd-block prd-block3" id="prd-block3-rgenQSrbae">
	
	
					
	
	<div class="image">
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=44&urltype=quickview" data-toggle="tooltip" title="Quick view" class="quickview-link grid-on list-on normal-on"><i class="fa fa-eye"></i> Quick view</a>
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=44"><img src="http://localhost/hijab.com/image/cache/catalog/demo/macbook_air_1-300x300.jpg" alt="MacBook Air" title="MacBook Air" class="img-responsive" /></a>
				<div class="countdown"></div>
			</div>
	<div class="info-wrp">
		<div class="info-content">
			<h4 class="name"><a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=44">MacBook Air</a></h4>
			<p class="info grid-off list-on normal-off">
	MacBook Air is ultrathin, ultraportable, and ultra unlike anything else. But you don&rsquo;t lose..</p>	
		</div>

		<div class="price-wrp">
						<div class="price grid-on list-on normal-on">
								<span class="price-old" style="text-decoration: none;">&nbsp;</span>
				<span class="price-new">Rp 1,000</span>
												<span class="price-tax grid-on list-on normal-on">Ex Tax: Rp 1,000</span>
							</div>
								</div>

	</div>

	<div class="btn-wrp grid-on list-on normal-on grid-off list-on normal-off grid-off list-on normal-off">
		<a class="btn btn-cart grid-on list-on normal-on" title="Add to Cart" onclick="cart.add('44', '1');">Add to Cart</a>
		
		<div class="other-btn-wrp">
			<a class="vm btn btn-wish grid-off list-on normal-off" data-toggle="tooltip" title="Add to wish list" onclick="wishlist.add('44');"><i class="fa fa-heart"></i></a>

			<a class="vm btn btn-compare grid-off list-on normal-off" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('44');"><i class="fa fa-retweet"></i></a>	
		</div>
		
	</div>
</div>

								</div>
																<div class="item">
									<div class="prd-block prd-block3" id="prd-block3-rgenAv2rud">
	
	
					
	
	<div class="image">
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=43&urltype=quickview" data-toggle="tooltip" title="Quick view" class="quickview-link grid-on list-on normal-on"><i class="fa fa-eye"></i> Quick view</a>
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=43"><img src="http://localhost/hijab.com/image/cache/catalog/demo/macbook_1-300x300.jpg" alt="MacBook" title="MacBook" class="img-responsive" /></a>
				<div class="countdown"></div>
			</div>
	<div class="info-wrp">
		<div class="info-content">
			<h4 class="name"><a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=43">MacBook</a></h4>
			<p class="info grid-off list-on normal-off">
	
		Intel Core 2 Duo processor
	
		Powered by an Intel Core 2 Duo processor at speeds up to 2.1..</p>	
		</div>

		<div class="price-wrp">
						<div class="price grid-on list-on normal-on">
								<span class="price-old" style="text-decoration: none;">&nbsp;</span>
				<span class="price-new">Rp 500</span>
												<span class="price-tax grid-on list-on normal-on">Ex Tax: Rp 500</span>
							</div>
								</div>

	</div>

	<div class="btn-wrp grid-on list-on normal-on grid-off list-on normal-off grid-off list-on normal-off">
		<a class="btn btn-cart grid-on list-on normal-on" title="Add to Cart" onclick="cart.add('43', '1');">Add to Cart</a>
		
		<div class="other-btn-wrp">
			<a class="vm btn btn-wish grid-off list-on normal-off" data-toggle="tooltip" title="Add to wish list" onclick="wishlist.add('43');"><i class="fa fa-heart"></i></a>

			<a class="vm btn btn-compare grid-off list-on normal-off" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('43');"><i class="fa fa-retweet"></i></a>	
		</div>
		
	</div>
</div>

								</div>
																<div class="item">
									<div class="prd-block prd-block3" id="prd-block3-rgenfsJ2bK">
	
	
				<span class="offer-tag-discount"><span>10%</span></span>
			
	
	<div class="image">
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=42&urltype=quickview" data-toggle="tooltip" title="Quick view" class="quickview-link grid-on list-on normal-on"><i class="fa fa-eye"></i> Quick view</a>
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=42"><img src="http://localhost/hijab.com/image/cache/catalog/demo/apple_cinema_30-300x300.jpg" alt="Apple Cinema 30&quot;" title="Apple Cinema 30&quot;" class="img-responsive" /></a>
				<div class="countdown"></div>
			</div>
	<div class="info-wrp">
		<div class="info-content">
			<h4 class="name"><a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=42">Apple Cinema 30&quot;</a></h4>
			<p class="info grid-off list-on normal-off">
	The 30-inch Apple Cinema HD Display delivers an amazing 2560 x 1600 pixel resolution. Designed sp..</p>	
		</div>

		<div class="price-wrp">
						<div class="price grid-on list-on normal-on">
								<span class="price-old">Rp 100</span>
				<span class="price-new price-spl">Rp 90</span>
												<span class="price-tax grid-on list-on normal-on">Ex Tax: Rp 90</span>
							</div>
								</div>

	</div>

	<div class="btn-wrp grid-on list-on normal-on grid-off list-on normal-off grid-off list-on normal-off">
		<a class="btn btn-cart grid-on list-on normal-on" title="Add to Cart" onclick="cart.add('42', '1');">Add to Cart</a>
		
		<div class="other-btn-wrp">
			<a class="vm btn btn-wish grid-off list-on normal-off" data-toggle="tooltip" title="Add to wish list" onclick="wishlist.add('42');"><i class="fa fa-heart"></i></a>

			<a class="vm btn btn-compare grid-off list-on normal-off" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('42');"><i class="fa fa-retweet"></i></a>	
		</div>
		
	</div>
</div>

								</div>
															</div>
							<script type="text/javascript" ><!--
							$("#rgen-productgroups-rgenz9EIG1 .tab-container").on('click', '.tab-item a', function(event) {
								event.preventDefault();
								var obj = $(this).attr('href') + " .widget-carousel";
								var resObj = { 0: { items:1 },200: { items:1 },300: { items:1 },400: { items:1 },500: { items:1 },600: { items:2 },700: { items:2 },800: { items:4 },900: { items:4 },1000: { items:4 },1100: { items:4 }	};
								if (!$(obj).hasClass('owl-loaded')) { widgetCarousel(obj, resObj); };
							});
							$("#rgen-productgroups-rgenz9EIG1 .tab-pane.active .widget-carousel").css({	opacity: 0 });
							$(document).ready(function() {
								var obj = "#rgen-productgroups-rgenz9EIG1 .tab-pane.active .widget-carousel";
								var resObj = { 0: { items:1 },200: { items:1 },300: { items:1 },400: { items:1 },500: { items:1 },600: { items:2 },700: { items:2 },800: { items:4 },900: { items:4 },1000: { items:4 },1100: { items:4 }	};
								var settings = { autoHeight: false };
								setTimeout(function () { widgetCarousel(obj, resObj, settings); }, 500);
							});
							//--></script>
													</div>
												<div class="tab-pane">
							
														<div 
								id            ="carousel-rgen-productgroups-rgenz9EIG1_1"
								class         ="widget-carousel ctrl-b" 
								data-stpd     ="1" 
								data-nav      ="true" 
								data-dots     ="false" 
								data-resitems ="4" 
								data-res      ="true" 
								data-margin   ="10">
																								<div class="item">
									<div class="prd-block prd-block3" id="prd-block3-rgenqaFiam">
	
	
				<span class="offer-tag-discount"><span>10%</span></span>
			
	
	<div class="image">
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=42&urltype=quickview" data-toggle="tooltip" title="Quick view" class="quickview-link grid-on list-on normal-on"><i class="fa fa-eye"></i> Quick view</a>
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=42"><img src="http://localhost/hijab.com/image/cache/catalog/demo/apple_cinema_30-300x300.jpg" alt="Apple Cinema 30&quot;" title="Apple Cinema 30&quot;" class="img-responsive" /></a>
				<div class="countdown"></div>
			</div>
	<div class="info-wrp">
		<div class="info-content">
			<h4 class="name"><a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=42">Apple Cinema 30&quot;</a></h4>
			<p class="info grid-off list-on normal-off">
	The 30-inch Apple Cinema HD Display delivers an amazing 2560 x 1600 pixel resolution. Designed sp..</p>	
		</div>

		<div class="price-wrp">
						<div class="price grid-on list-on normal-on">
								<span class="price-old">Rp 100</span>
				<span class="price-new price-spl">Rp 90</span>
												<span class="price-tax grid-on list-on normal-on">Ex Tax: Rp 90</span>
							</div>
								</div>

	</div>

	<div class="btn-wrp grid-on list-on normal-on grid-off list-on normal-off grid-off list-on normal-off">
		<a class="btn btn-cart grid-on list-on normal-on" title="Add to Cart" onclick="cart.add('42', '1');">Add to Cart</a>
		
		<div class="other-btn-wrp">
			<a class="vm btn btn-wish grid-off list-on normal-off" data-toggle="tooltip" title="Add to wish list" onclick="wishlist.add('42');"><i class="fa fa-heart"></i></a>

			<a class="vm btn btn-compare grid-off list-on normal-off" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('42');"><i class="fa fa-retweet"></i></a>	
		</div>
		
	</div>
</div>

								</div>
																<div class="item">
									<div class="prd-block prd-block3" id="prd-block3-rgen3y1Oav">
	
	
				<span class="offer-tag-discount"><span>20%</span></span>
			
	
	<div class="image">
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=30&urltype=quickview" data-toggle="tooltip" title="Quick view" class="quickview-link grid-on list-on normal-on"><i class="fa fa-eye"></i> Quick view</a>
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=30"><img src="http://localhost/hijab.com/image/cache/catalog/demo/canon_eos_5d_1-300x300.jpg" alt="Canon EOS 5D" title="Canon EOS 5D" class="img-responsive" /></a>
				<div class="countdown"></div>
			</div>
	<div class="info-wrp">
		<div class="info-content">
			<h4 class="name"><a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=30">Canon EOS 5D</a></h4>
			<p class="info grid-off list-on normal-off">
	Canon's press material for the EOS 5D states that it 'defines (a) new D-SLR category', while we'r..</p>	
		</div>

		<div class="price-wrp">
						<div class="price grid-on list-on normal-on">
								<span class="price-old">Rp 100</span>
				<span class="price-new price-spl">Rp 80</span>
												<span class="price-tax grid-on list-on normal-on">Ex Tax: Rp 80</span>
							</div>
								</div>

	</div>

	<div class="btn-wrp grid-on list-on normal-on grid-off list-on normal-off grid-off list-on normal-off">
		<a class="btn btn-cart grid-on list-on normal-on" title="Add to Cart" onclick="cart.add('30', '1');">Add to Cart</a>
		
		<div class="other-btn-wrp">
			<a class="vm btn btn-wish grid-off list-on normal-off" data-toggle="tooltip" title="Add to wish list" onclick="wishlist.add('30');"><i class="fa fa-heart"></i></a>

			<a class="vm btn btn-compare grid-off list-on normal-off" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('30');"><i class="fa fa-retweet"></i></a>	
		</div>
		
	</div>
</div>

								</div>
															</div>
							<script type="text/javascript" ><!--
							$("#rgen-productgroups-rgenz9EIG1 .tab-container").on('click', '.tab-item a', function(event) {
								event.preventDefault();
								var obj = $(this).attr('href') + " .widget-carousel";
								var resObj = { 0: { items:1 },200: { items:1 },300: { items:1 },400: { items:1 },500: { items:1 },600: { items:2 },700: { items:2 },800: { items:4 },900: { items:4 },1000: { items:4 },1100: { items:4 }	};
								if (!$(obj).hasClass('owl-loaded')) { widgetCarousel(obj, resObj); };
							});
							$("#rgen-productgroups-rgenz9EIG1 .tab-pane.active .widget-carousel").css({	opacity: 0 });
							$(document).ready(function() {
								var obj = "#rgen-productgroups-rgenz9EIG1 .tab-pane.active .widget-carousel";
								var resObj = { 0: { items:1 },200: { items:1 },300: { items:1 },400: { items:1 },500: { items:1 },600: { items:2 },700: { items:2 },800: { items:4 },900: { items:4 },1000: { items:4 },1100: { items:4 }	};
								var settings = { autoHeight: false };
								setTimeout(function () { widgetCarousel(obj, resObj, settings); }, 500);
							});
							//--></script>
													</div>
												<div class="tab-pane">
							
														<div 
								id            ="carousel-rgen-productgroups-rgenz9EIG1_2"
								class         ="widget-carousel ctrl-b" 
								data-stpd     ="1" 
								data-nav      ="true" 
								data-dots     ="false" 
								data-resitems ="4" 
								data-res      ="true" 
								data-margin   ="10">
																								<div class="item">
									<div class="prd-block prd-block3" id="prd-block3-rgenCfnB1S">
	
	
				<span class="offer-tag-discount"><span>10%</span></span>
			
	
	<div class="image">
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=42&urltype=quickview" data-toggle="tooltip" title="Quick view" class="quickview-link grid-on list-on normal-on"><i class="fa fa-eye"></i> Quick view</a>
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=42"><img src="http://localhost/hijab.com/image/cache/catalog/demo/apple_cinema_30-300x300.jpg" alt="Apple Cinema 30&quot;" title="Apple Cinema 30&quot;" class="img-responsive" /></a>
				<div class="countdown"></div>
			</div>
	<div class="info-wrp">
		<div class="info-content">
			<h4 class="name"><a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=42">Apple Cinema 30&quot;</a></h4>
			<p class="info grid-off list-on normal-off">
	The 30-inch Apple Cinema HD Display delivers an amazing 2560 x 1600 pixel resolution. Designed sp..</p>	
		</div>

		<div class="price-wrp">
						<div class="price grid-on list-on normal-on">
								<span class="price-old">Rp 100</span>
				<span class="price-new price-spl">Rp 90</span>
												<span class="price-tax grid-on list-on normal-on">Ex Tax: Rp 90</span>
							</div>
								</div>

	</div>

	<div class="btn-wrp grid-on list-on normal-on grid-off list-on normal-off grid-off list-on normal-off">
		<a class="btn btn-cart grid-on list-on normal-on" title="Add to Cart" onclick="cart.add('42', '1');">Add to Cart</a>
		
		<div class="other-btn-wrp">
			<a class="vm btn btn-wish grid-off list-on normal-off" data-toggle="tooltip" title="Add to wish list" onclick="wishlist.add('42');"><i class="fa fa-heart"></i></a>

			<a class="vm btn btn-compare grid-off list-on normal-off" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('42');"><i class="fa fa-retweet"></i></a>	
		</div>
		
	</div>
</div>

								</div>
																<div class="item">
									<div class="prd-block prd-block3" id="prd-block3-rgenvPQqUD">
	
	
				<span class="offer-tag-discount"><span>20%</span></span>
			
	
	<div class="image">
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=30&urltype=quickview" data-toggle="tooltip" title="Quick view" class="quickview-link grid-on list-on normal-on"><i class="fa fa-eye"></i> Quick view</a>
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=30"><img src="http://localhost/hijab.com/image/cache/catalog/demo/canon_eos_5d_1-300x300.jpg" alt="Canon EOS 5D" title="Canon EOS 5D" class="img-responsive" /></a>
				<div class="countdown"></div>
			</div>
	<div class="info-wrp">
		<div class="info-content">
			<h4 class="name"><a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=30">Canon EOS 5D</a></h4>
			<p class="info grid-off list-on normal-off">
	Canon's press material for the EOS 5D states that it 'defines (a) new D-SLR category', while we'r..</p>	
		</div>

		<div class="price-wrp">
						<div class="price grid-on list-on normal-on">
								<span class="price-old">Rp 100</span>
				<span class="price-new price-spl">Rp 80</span>
												<span class="price-tax grid-on list-on normal-on">Ex Tax: Rp 80</span>
							</div>
								</div>

	</div>

	<div class="btn-wrp grid-on list-on normal-on grid-off list-on normal-off grid-off list-on normal-off">
		<a class="btn btn-cart grid-on list-on normal-on" title="Add to Cart" onclick="cart.add('30', '1');">Add to Cart</a>
		
		<div class="other-btn-wrp">
			<a class="vm btn btn-wish grid-off list-on normal-off" data-toggle="tooltip" title="Add to wish list" onclick="wishlist.add('30');"><i class="fa fa-heart"></i></a>

			<a class="vm btn btn-compare grid-off list-on normal-off" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('30');"><i class="fa fa-retweet"></i></a>	
		</div>
		
	</div>
</div>

								</div>
																<div class="item">
									<div class="prd-block prd-block3" id="prd-block3-rgena7q0AF">
	
	
					
	
	<div class="image">
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=47&urltype=quickview" data-toggle="tooltip" title="Quick view" class="quickview-link grid-on list-on normal-on"><i class="fa fa-eye"></i> Quick view</a>
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=47"><img src="http://localhost/hijab.com/image/cache/catalog/demo/hp_1-300x300.jpg" alt="HP LP3065" title="HP LP3065" class="img-responsive" /></a>
				<div class="countdown"></div>
			</div>
	<div class="info-wrp">
		<div class="info-content">
			<h4 class="name"><a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=47">HP LP3065</a></h4>
			<p class="info grid-off list-on normal-off">
	Stop your co-workers in their tracks with the stunning new 30-inch diagonal HP LP3065 Flat Panel ..</p>	
		</div>

		<div class="price-wrp">
						<div class="price grid-on list-on normal-on">
								<span class="price-old" style="text-decoration: none;">&nbsp;</span>
				<span class="price-new">Rp 100</span>
												<span class="price-tax grid-on list-on normal-on">Ex Tax: Rp 100</span>
							</div>
								</div>

	</div>

	<div class="btn-wrp grid-on list-on normal-on grid-off list-on normal-off grid-off list-on normal-off">
		<a class="btn btn-cart grid-on list-on normal-on" title="Add to Cart" onclick="cart.add('47', '1');">Add to Cart</a>
		
		<div class="other-btn-wrp">
			<a class="vm btn btn-wish grid-off list-on normal-off" data-toggle="tooltip" title="Add to wish list" onclick="wishlist.add('47');"><i class="fa fa-heart"></i></a>

			<a class="vm btn btn-compare grid-off list-on normal-off" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('47');"><i class="fa fa-retweet"></i></a>	
		</div>
		
	</div>
</div>

								</div>
																<div class="item">
									<div class="prd-block prd-block3" id="prd-block3-rgenpqdiGO">
	
	
					
	
	<div class="image">
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=28&urltype=quickview" data-toggle="tooltip" title="Quick view" class="quickview-link grid-on list-on normal-on"><i class="fa fa-eye"></i> Quick view</a>
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=28"><img src="http://localhost/hijab.com/image/cache/catalog/demo/htc_touch_hd_1-300x300.jpg" alt="HTC Touch HD" title="HTC Touch HD" class="img-responsive" /></a>
				<div class="countdown"></div>
			</div>
	<div class="info-wrp">
		<div class="info-content">
			<h4 class="name"><a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=28">HTC Touch HD</a></h4>
			<p class="info grid-off list-on normal-off">
	HTC Touch - in High Definition. Watch music videos and streaming content in awe-inspiring high de..</p>	
		</div>

		<div class="price-wrp">
						<div class="price grid-on list-on normal-on">
								<span class="price-old" style="text-decoration: none;">&nbsp;</span>
				<span class="price-new">Rp 100</span>
												<span class="price-tax grid-on list-on normal-on">Ex Tax: Rp 100</span>
							</div>
								</div>

	</div>

	<div class="btn-wrp grid-on list-on normal-on grid-off list-on normal-off grid-off list-on normal-off">
		<a class="btn btn-cart grid-on list-on normal-on" title="Add to Cart" onclick="cart.add('28', '1');">Add to Cart</a>
		
		<div class="other-btn-wrp">
			<a class="vm btn btn-wish grid-off list-on normal-off" data-toggle="tooltip" title="Add to wish list" onclick="wishlist.add('28');"><i class="fa fa-heart"></i></a>

			<a class="vm btn btn-compare grid-off list-on normal-off" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('28');"><i class="fa fa-retweet"></i></a>	
		</div>
		
	</div>
</div>

								</div>
																<div class="item">
									<div class="prd-block prd-block3" id="prd-block3-rgenCICGT9">
	
	
					
	
	<div class="image">
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=40&urltype=quickview" data-toggle="tooltip" title="Quick view" class="quickview-link grid-on list-on normal-on"><i class="fa fa-eye"></i> Quick view</a>
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=40"><img src="http://localhost/hijab.com/image/cache/catalog/demo/iphone_1-300x300.jpg" alt="iPhone" title="iPhone" class="img-responsive" /></a>
				<div class="countdown"></div>
			</div>
	<div class="info-wrp">
		<div class="info-content">
			<h4 class="name"><a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=40">iPhone</a></h4>
			<p class="info grid-off list-on normal-off">
	iPhone is a revolutionary new mobile phone that allows you to make a call by simply tapping a nam..</p>	
		</div>

		<div class="price-wrp">
						<div class="price grid-on list-on normal-on">
								<span class="price-old" style="text-decoration: none;">&nbsp;</span>
				<span class="price-new">Rp 101</span>
												<span class="price-tax grid-on list-on normal-on">Ex Tax: Rp 101</span>
							</div>
								</div>

	</div>

	<div class="btn-wrp grid-on list-on normal-on grid-off list-on normal-off grid-off list-on normal-off">
		<a class="btn btn-cart grid-on list-on normal-on" title="Add to Cart" onclick="cart.add('40', '1');">Add to Cart</a>
		
		<div class="other-btn-wrp">
			<a class="vm btn btn-wish grid-off list-on normal-off" data-toggle="tooltip" title="Add to wish list" onclick="wishlist.add('40');"><i class="fa fa-heart"></i></a>

			<a class="vm btn btn-compare grid-off list-on normal-off" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('40');"><i class="fa fa-retweet"></i></a>	
		</div>
		
	</div>
</div>

								</div>
																<div class="item">
									<div class="prd-block prd-block3" id="prd-block3-rgen2ZFjhc">
	
	
					
	
	<div class="image">
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=48&urltype=quickview" data-toggle="tooltip" title="Quick view" class="quickview-link grid-on list-on normal-on"><i class="fa fa-eye"></i> Quick view</a>
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=48"><img src="http://localhost/hijab.com/image/cache/catalog/demo/ipod_classic_1-300x300.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive" /></a>
				<div class="countdown"></div>
			</div>
	<div class="info-wrp">
		<div class="info-content">
			<h4 class="name"><a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=48">iPod Classic</a></h4>
			<p class="info grid-off list-on normal-off">
	
		
			More room to move.
		
			With 80GB or 160GB of storage and up to 40 hours of battery l..</p>	
		</div>

		<div class="price-wrp">
						<div class="price grid-on list-on normal-on">
								<span class="price-old" style="text-decoration: none;">&nbsp;</span>
				<span class="price-new">Rp 100</span>
												<span class="price-tax grid-on list-on normal-on">Ex Tax: Rp 100</span>
							</div>
								</div>

	</div>

	<div class="btn-wrp grid-on list-on normal-on grid-off list-on normal-off grid-off list-on normal-off">
		<a class="btn btn-cart grid-on list-on normal-on" title="Add to Cart" onclick="cart.add('48', '1');">Add to Cart</a>
		
		<div class="other-btn-wrp">
			<a class="vm btn btn-wish grid-off list-on normal-off" data-toggle="tooltip" title="Add to wish list" onclick="wishlist.add('48');"><i class="fa fa-heart"></i></a>

			<a class="vm btn btn-compare grid-off list-on normal-off" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('48');"><i class="fa fa-retweet"></i></a>	
		</div>
		
	</div>
</div>

								</div>
																<div class="item">
									<div class="prd-block prd-block3" id="prd-block3-rgenZrdhte">
	
	
					
	
	<div class="image">
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=43&urltype=quickview" data-toggle="tooltip" title="Quick view" class="quickview-link grid-on list-on normal-on"><i class="fa fa-eye"></i> Quick view</a>
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=43"><img src="http://localhost/hijab.com/image/cache/catalog/demo/macbook_1-300x300.jpg" alt="MacBook" title="MacBook" class="img-responsive" /></a>
				<div class="countdown"></div>
			</div>
	<div class="info-wrp">
		<div class="info-content">
			<h4 class="name"><a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=43">MacBook</a></h4>
			<p class="info grid-off list-on normal-off">
	
		Intel Core 2 Duo processor
	
		Powered by an Intel Core 2 Duo processor at speeds up to 2.1..</p>	
		</div>

		<div class="price-wrp">
						<div class="price grid-on list-on normal-on">
								<span class="price-old" style="text-decoration: none;">&nbsp;</span>
				<span class="price-new">Rp 500</span>
												<span class="price-tax grid-on list-on normal-on">Ex Tax: Rp 500</span>
							</div>
								</div>

	</div>

	<div class="btn-wrp grid-on list-on normal-on grid-off list-on normal-off grid-off list-on normal-off">
		<a class="btn btn-cart grid-on list-on normal-on" title="Add to Cart" onclick="cart.add('43', '1');">Add to Cart</a>
		
		<div class="other-btn-wrp">
			<a class="vm btn btn-wish grid-off list-on normal-off" data-toggle="tooltip" title="Add to wish list" onclick="wishlist.add('43');"><i class="fa fa-heart"></i></a>

			<a class="vm btn btn-compare grid-off list-on normal-off" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('43');"><i class="fa fa-retweet"></i></a>	
		</div>
		
	</div>
</div>

								</div>
																<div class="item">
									<div class="prd-block prd-block3" id="prd-block3-rgenaIeUYr">
	
	
					
	
	<div class="image">
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=44&urltype=quickview" data-toggle="tooltip" title="Quick view" class="quickview-link grid-on list-on normal-on"><i class="fa fa-eye"></i> Quick view</a>
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=44"><img src="http://localhost/hijab.com/image/cache/catalog/demo/macbook_air_1-300x300.jpg" alt="MacBook Air" title="MacBook Air" class="img-responsive" /></a>
				<div class="countdown"></div>
			</div>
	<div class="info-wrp">
		<div class="info-content">
			<h4 class="name"><a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=44">MacBook Air</a></h4>
			<p class="info grid-off list-on normal-off">
	MacBook Air is ultrathin, ultraportable, and ultra unlike anything else. But you don&rsquo;t lose..</p>	
		</div>

		<div class="price-wrp">
						<div class="price grid-on list-on normal-on">
								<span class="price-old" style="text-decoration: none;">&nbsp;</span>
				<span class="price-new">Rp 1,000</span>
												<span class="price-tax grid-on list-on normal-on">Ex Tax: Rp 1,000</span>
							</div>
								</div>

	</div>

	<div class="btn-wrp grid-on list-on normal-on grid-off list-on normal-off grid-off list-on normal-off">
		<a class="btn btn-cart grid-on list-on normal-on" title="Add to Cart" onclick="cart.add('44', '1');">Add to Cart</a>
		
		<div class="other-btn-wrp">
			<a class="vm btn btn-wish grid-off list-on normal-off" data-toggle="tooltip" title="Add to wish list" onclick="wishlist.add('44');"><i class="fa fa-heart"></i></a>

			<a class="vm btn btn-compare grid-off list-on normal-off" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('44');"><i class="fa fa-retweet"></i></a>	
		</div>
		
	</div>
</div>

								</div>
															</div>
							<script type="text/javascript" ><!--
							$("#rgen-productgroups-rgenz9EIG1 .tab-container").on('click', '.tab-item a', function(event) {
								event.preventDefault();
								var obj = $(this).attr('href') + " .widget-carousel";
								var resObj = { 0: { items:1 },200: { items:1 },300: { items:1 },400: { items:1 },500: { items:1 },600: { items:2 },700: { items:2 },800: { items:4 },900: { items:4 },1000: { items:5 },1100: { items:5 }	};
								if (!$(obj).hasClass('owl-loaded')) { widgetCarousel(obj, resObj); };
							});
							$("#rgen-productgroups-rgenz9EIG1 .tab-pane.active .widget-carousel").css({	opacity: 0 });
							$(document).ready(function() {
								var obj = "#rgen-productgroups-rgenz9EIG1 .tab-pane.active .widget-carousel";
								var resObj = { 0: { items:1 },200: { items:1 },300: { items:1 },400: { items:1 },500: { items:1 },600: { items:2 },700: { items:2 },800: { items:4 },900: { items:4 },1000: { items:5 },1100: { items:5 }	};
								var settings = { autoHeight: false };
								setTimeout(function () { widgetCarousel(obj, resObj, settings); }, 500);
							});
							//--></script>
													</div>
												<div class="tab-pane">
							
														<div 
								id            ="carousel-rgen-productgroups-rgenz9EIG1_3"
								class         ="widget-carousel ctrl-b" 
								data-stpd     ="1" 
								data-nav      ="true" 
								data-dots     ="false" 
								data-resitems ="4" 
								data-res      ="true" 
								data-margin   ="10">
																								<div class="item">
									<div class="prd-block prd-block3" id="prd-block3-rgenPb0Xmi">
	
	
				<span class="offer-tag-discount"><span>10%</span></span>
			
	
	<div class="image">
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=42&urltype=quickview" data-toggle="tooltip" title="Quick view" class="quickview-link grid-on list-on normal-on"><i class="fa fa-eye"></i> Quick view</a>
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=42"><img src="http://localhost/hijab.com/image/cache/catalog/demo/apple_cinema_30-300x300.jpg" alt="Apple Cinema 30&quot;" title="Apple Cinema 30&quot;" class="img-responsive" /></a>
				<div class="countdown"></div>
			</div>
	<div class="info-wrp">
		<div class="info-content">
			<h4 class="name"><a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=42">Apple Cinema 30&quot;</a></h4>
			<p class="info grid-off list-on normal-off">
	The 30-inch Apple Cinema HD Display delivers an amazing 2560 x 1600 pixel resolution. Designed sp..</p>	
		</div>

		<div class="price-wrp">
						<div class="price grid-on list-on normal-on">
								<span class="price-old">Rp 100</span>
				<span class="price-new price-spl">Rp 90</span>
												<span class="price-tax grid-on list-on normal-on">Ex Tax: Rp 90</span>
							</div>
								</div>

	</div>

	<div class="btn-wrp grid-on list-on normal-on grid-off list-on normal-off grid-off list-on normal-off">
		<a class="btn btn-cart grid-on list-on normal-on" title="Add to Cart" onclick="cart.add('42', '1');">Add to Cart</a>
		
		<div class="other-btn-wrp">
			<a class="vm btn btn-wish grid-off list-on normal-off" data-toggle="tooltip" title="Add to wish list" onclick="wishlist.add('42');"><i class="fa fa-heart"></i></a>

			<a class="vm btn btn-compare grid-off list-on normal-off" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('42');"><i class="fa fa-retweet"></i></a>	
		</div>
		
	</div>
</div>

								</div>
																<div class="item">
									<div class="prd-block prd-block3" id="prd-block3-rgen5drXyH">
	
	
					
	
	<div class="image">
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=41&urltype=quickview" data-toggle="tooltip" title="Quick view" class="quickview-link grid-on list-on normal-on"><i class="fa fa-eye"></i> Quick view</a>
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=41"><img src="http://localhost/hijab.com/image/cache/catalog/demo/imac_1-300x300.jpg" alt="iMac" title="iMac" class="img-responsive" /></a>
				<div class="countdown"></div>
			</div>
	<div class="info-wrp">
		<div class="info-content">
			<h4 class="name"><a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=41">iMac</a></h4>
			<p class="info grid-off list-on normal-off">
	Just when you thought iMac had everything, now there´s even more. More powerful Intel Core 2 Duo ..</p>	
		</div>

		<div class="price-wrp">
						<div class="price grid-on list-on normal-on">
								<span class="price-old" style="text-decoration: none;">&nbsp;</span>
				<span class="price-new">Rp 100</span>
												<span class="price-tax grid-on list-on normal-on">Ex Tax: Rp 100</span>
							</div>
								</div>

	</div>

	<div class="btn-wrp grid-on list-on normal-on grid-off list-on normal-off grid-off list-on normal-off">
		<a class="btn btn-cart grid-on list-on normal-on" title="Add to Cart" onclick="cart.add('41', '1');">Add to Cart</a>
		
		<div class="other-btn-wrp">
			<a class="vm btn btn-wish grid-off list-on normal-off" data-toggle="tooltip" title="Add to wish list" onclick="wishlist.add('41');"><i class="fa fa-heart"></i></a>

			<a class="vm btn btn-compare grid-off list-on normal-off" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('41');"><i class="fa fa-retweet"></i></a>	
		</div>
		
	</div>
</div>

								</div>
																<div class="item">
									<div class="prd-block prd-block3" id="prd-block3-rgen9ngm4g">
	
	
					
	
	<div class="image">
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=40&urltype=quickview" data-toggle="tooltip" title="Quick view" class="quickview-link grid-on list-on normal-on"><i class="fa fa-eye"></i> Quick view</a>
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=40"><img src="http://localhost/hijab.com/image/cache/catalog/demo/iphone_1-300x300.jpg" alt="iPhone" title="iPhone" class="img-responsive" /></a>
				<div class="countdown"></div>
			</div>
	<div class="info-wrp">
		<div class="info-content">
			<h4 class="name"><a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=40">iPhone</a></h4>
			<p class="info grid-off list-on normal-off">
	iPhone is a revolutionary new mobile phone that allows you to make a call by simply tapping a nam..</p>	
		</div>

		<div class="price-wrp">
						<div class="price grid-on list-on normal-on">
								<span class="price-old" style="text-decoration: none;">&nbsp;</span>
				<span class="price-new">Rp 101</span>
												<span class="price-tax grid-on list-on normal-on">Ex Tax: Rp 101</span>
							</div>
								</div>

	</div>

	<div class="btn-wrp grid-on list-on normal-on grid-off list-on normal-off grid-off list-on normal-off">
		<a class="btn btn-cart grid-on list-on normal-on" title="Add to Cart" onclick="cart.add('40', '1');">Add to Cart</a>
		
		<div class="other-btn-wrp">
			<a class="vm btn btn-wish grid-off list-on normal-off" data-toggle="tooltip" title="Add to wish list" onclick="wishlist.add('40');"><i class="fa fa-heart"></i></a>

			<a class="vm btn btn-compare grid-off list-on normal-off" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('40');"><i class="fa fa-retweet"></i></a>	
		</div>
		
	</div>
</div>

								</div>
																<div class="item">
									<div class="prd-block prd-block3" id="prd-block3-rgenzGkSiC">
	
	
					
	
	<div class="image">
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=48&urltype=quickview" data-toggle="tooltip" title="Quick view" class="quickview-link grid-on list-on normal-on"><i class="fa fa-eye"></i> Quick view</a>
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=48"><img src="http://localhost/hijab.com/image/cache/catalog/demo/ipod_classic_1-300x300.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive" /></a>
				<div class="countdown"></div>
			</div>
	<div class="info-wrp">
		<div class="info-content">
			<h4 class="name"><a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=48">iPod Classic</a></h4>
			<p class="info grid-off list-on normal-off">
	
		
			More room to move.
		
			With 80GB or 160GB of storage and up to 40 hours of battery l..</p>	
		</div>

		<div class="price-wrp">
						<div class="price grid-on list-on normal-on">
								<span class="price-old" style="text-decoration: none;">&nbsp;</span>
				<span class="price-new">Rp 100</span>
												<span class="price-tax grid-on list-on normal-on">Ex Tax: Rp 100</span>
							</div>
								</div>

	</div>

	<div class="btn-wrp grid-on list-on normal-on grid-off list-on normal-off grid-off list-on normal-off">
		<a class="btn btn-cart grid-on list-on normal-on" title="Add to Cart" onclick="cart.add('48', '1');">Add to Cart</a>
		
		<div class="other-btn-wrp">
			<a class="vm btn btn-wish grid-off list-on normal-off" data-toggle="tooltip" title="Add to wish list" onclick="wishlist.add('48');"><i class="fa fa-heart"></i></a>

			<a class="vm btn btn-compare grid-off list-on normal-off" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('48');"><i class="fa fa-retweet"></i></a>	
		</div>
		
	</div>
</div>

								</div>
																<div class="item">
									<div class="prd-block prd-block3" id="prd-block3-rgenuTNEEy">
	
	
					
	
	<div class="image">
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=36&urltype=quickview" data-toggle="tooltip" title="Quick view" class="quickview-link grid-on list-on normal-on"><i class="fa fa-eye"></i> Quick view</a>
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=36"><img src="http://localhost/hijab.com/image/cache/catalog/demo/ipod_nano_1-300x300.jpg" alt="iPod Nano" title="iPod Nano" class="img-responsive" /></a>
				<div class="countdown"></div>
			</div>
	<div class="info-wrp">
		<div class="info-content">
			<h4 class="name"><a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=36">iPod Nano</a></h4>
			<p class="info grid-off list-on normal-off">
	
		Video in your pocket.
	
		Its the small iPod with one very big idea: video. The worlds most..</p>	
		</div>

		<div class="price-wrp">
						<div class="price grid-on list-on normal-on">
								<span class="price-old" style="text-decoration: none;">&nbsp;</span>
				<span class="price-new">Rp 100</span>
												<span class="price-tax grid-on list-on normal-on">Ex Tax: Rp 100</span>
							</div>
								</div>

	</div>

	<div class="btn-wrp grid-on list-on normal-on grid-off list-on normal-off grid-off list-on normal-off">
		<a class="btn btn-cart grid-on list-on normal-on" title="Add to Cart" onclick="cart.add('36', '1');">Add to Cart</a>
		
		<div class="other-btn-wrp">
			<a class="vm btn btn-wish grid-off list-on normal-off" data-toggle="tooltip" title="Add to wish list" onclick="wishlist.add('36');"><i class="fa fa-heart"></i></a>

			<a class="vm btn btn-compare grid-off list-on normal-off" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('36');"><i class="fa fa-retweet"></i></a>	
		</div>
		
	</div>
</div>

								</div>
																<div class="item">
									<div class="prd-block prd-block3" id="prd-block3-rgenUKqzYn">
	
	
					
	
	<div class="image">
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=34&urltype=quickview" data-toggle="tooltip" title="Quick view" class="quickview-link grid-on list-on normal-on"><i class="fa fa-eye"></i> Quick view</a>
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=34"><img src="http://localhost/hijab.com/image/cache/catalog/demo/ipod_shuffle_1-300x300.jpg" alt="iPod Shuffle" title="iPod Shuffle" class="img-responsive" /></a>
				<div class="countdown"></div>
			</div>
	<div class="info-wrp">
		<div class="info-content">
			<h4 class="name"><a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=34">iPod Shuffle</a></h4>
			<p class="info grid-off list-on normal-off">
	Born to be worn.
	
		Clip on the worlds most wearable music player and take up to 240 songs wit..</p>	
		</div>

		<div class="price-wrp">
						<div class="price grid-on list-on normal-on">
								<span class="price-old" style="text-decoration: none;">&nbsp;</span>
				<span class="price-new">Rp 100</span>
												<span class="price-tax grid-on list-on normal-on">Ex Tax: Rp 100</span>
							</div>
								</div>

	</div>

	<div class="btn-wrp grid-on list-on normal-on grid-off list-on normal-off grid-off list-on normal-off">
		<a class="btn btn-cart grid-on list-on normal-on" title="Add to Cart" onclick="cart.add('34', '1');">Add to Cart</a>
		
		<div class="other-btn-wrp">
			<a class="vm btn btn-wish grid-off list-on normal-off" data-toggle="tooltip" title="Add to wish list" onclick="wishlist.add('34');"><i class="fa fa-heart"></i></a>

			<a class="vm btn btn-compare grid-off list-on normal-off" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('34');"><i class="fa fa-retweet"></i></a>	
		</div>
		
	</div>
</div>

								</div>
																<div class="item">
									<div class="prd-block prd-block3" id="prd-block3-rgenTaQuD3">
	
	
					
	
	<div class="image">
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=32&urltype=quickview" data-toggle="tooltip" title="Quick view" class="quickview-link grid-on list-on normal-on"><i class="fa fa-eye"></i> Quick view</a>
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=32"><img src="http://localhost/hijab.com/image/cache/catalog/demo/ipod_touch_1-300x300.jpg" alt="iPod Touch" title="iPod Touch" class="img-responsive" /></a>
				<div class="countdown"></div>
			</div>
	<div class="info-wrp">
		<div class="info-content">
			<h4 class="name"><a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=32">iPod Touch</a></h4>
			<p class="info grid-off list-on normal-off">
	Revolutionary multi-touch interface.
	iPod touch features the same multi-touch screen technology..</p>	
		</div>

		<div class="price-wrp">
						<div class="price grid-on list-on normal-on">
								<span class="price-old" style="text-decoration: none;">&nbsp;</span>
				<span class="price-new">Rp 100</span>
												<span class="price-tax grid-on list-on normal-on">Ex Tax: Rp 100</span>
							</div>
								</div>

	</div>

	<div class="btn-wrp grid-on list-on normal-on grid-off list-on normal-off grid-off list-on normal-off">
		<a class="btn btn-cart grid-on list-on normal-on" title="Add to Cart" onclick="cart.add('32', '1');">Add to Cart</a>
		
		<div class="other-btn-wrp">
			<a class="vm btn btn-wish grid-off list-on normal-off" data-toggle="tooltip" title="Add to wish list" onclick="wishlist.add('32');"><i class="fa fa-heart"></i></a>

			<a class="vm btn btn-compare grid-off list-on normal-off" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('32');"><i class="fa fa-retweet"></i></a>	
		</div>
		
	</div>
</div>

								</div>
																<div class="item">
									<div class="prd-block prd-block3" id="prd-block3-rgenSzLsSB">
	
	
					
	
	<div class="image">
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=43&urltype=quickview" data-toggle="tooltip" title="Quick view" class="quickview-link grid-on list-on normal-on"><i class="fa fa-eye"></i> Quick view</a>
		<a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=43"><img src="http://localhost/hijab.com/image/cache/catalog/demo/macbook_1-300x300.jpg" alt="MacBook" title="MacBook" class="img-responsive" /></a>
				<div class="countdown"></div>
			</div>
	<div class="info-wrp">
		<div class="info-content">
			<h4 class="name"><a href="http://localhost/hijab.com/index.php?route=product/product&amp;product_id=43">MacBook</a></h4>
			<p class="info grid-off list-on normal-off">
	
		Intel Core 2 Duo processor
	
		Powered by an Intel Core 2 Duo processor at speeds up to 2.1..</p>	
		</div>

		<div class="price-wrp">
						<div class="price grid-on list-on normal-on">
								<span class="price-old" style="text-decoration: none;">&nbsp;</span>
				<span class="price-new">Rp 500</span>
												<span class="price-tax grid-on list-on normal-on">Ex Tax: Rp 500</span>
							</div>
								</div>

	</div>

	<div class="btn-wrp grid-on list-on normal-on grid-off list-on normal-off grid-off list-on normal-off">
		<a class="btn btn-cart grid-on list-on normal-on" title="Add to Cart" onclick="cart.add('43', '1');">Add to Cart</a>
		
		<div class="other-btn-wrp">
			<a class="vm btn btn-wish grid-off list-on normal-off" data-toggle="tooltip" title="Add to wish list" onclick="wishlist.add('43');"><i class="fa fa-heart"></i></a>

			<a class="vm btn btn-compare grid-off list-on normal-off" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('43');"><i class="fa fa-retweet"></i></a>	
		</div>
		
	</div>
</div>

								</div>
															</div>
							<script type="text/javascript" ><!--
							$("#rgen-productgroups-rgenz9EIG1 .tab-container").on('click', '.tab-item a', function(event) {
								event.preventDefault();
								var obj = $(this).attr('href') + " .widget-carousel";
								var resObj = { 0: { items:1 },200: { items:1 },300: { items:1 },400: { items:1 },500: { items:1 },600: { items:2 },700: { items:2 },800: { items:4 },900: { items:4 },1000: { items:5 },1100: { items:5 }	};
								if (!$(obj).hasClass('owl-loaded')) { widgetCarousel(obj, resObj); };
							});
							$("#rgen-productgroups-rgenz9EIG1 .tab-pane.active .widget-carousel").css({	opacity: 0 });
							$(document).ready(function() {
								var obj = "#rgen-productgroups-rgenz9EIG1 .tab-pane.active .widget-carousel";
								var resObj = { 0: { items:1 },200: { items:1 },300: { items:1 },400: { items:1 },500: { items:1 },600: { items:2 },700: { items:2 },800: { items:4 },900: { items:4 },1000: { items:5 },1100: { items:5 }	};
								var settings = { autoHeight: false };
								setTimeout(function () { widgetCarousel(obj, resObj, settings); }, 500);
							});
							//--></script>
													</div>
											</div>
				</div>
				
			</div>
		</div>
			</div>

	
</div>
</div>
