<div id="rgen-contentblocks-rgenBpnGR8" class="rgen-contentblocks contentblocks-rg5H1">
	<div class="mod-wrp container">
		
						
				
		<div class="rw gt40">
						
			<div class="m-cl cl12 t-xl12">
				<div class="mod-content">
					
											<div class="rw gt30 mb20">
														<div class="cl cl4 d-xl4 t-xl4 m-xl12 m-sm12 m-xs12 ">

																								<h3 class="sub-mod-hd" style="">Contact Information</h3>
																
																<div class="contentblock-grid rw eq1 d-eq1 t-eq1 mxl-eq1 msm-eq1 mxs-eq1 gt0 mb20">
																		<div class="cl">
										
<section class="ctn-block ctn-block-small2 l"
	>
	<div class="ctn-inner-wrp">
		
									
								<span class="img-wrp" style="max-width:40px; ">
								<figure class="icon vm" style="font-size: 20px;"><i class="glyphicon glyphicon-map-marker"></i></figure>
				</span>
					
		<div class="text-wrp" style="margin-left:50px;">
							<h3 class="name">
										Address:									</h3>
			
						<div class="text">123 Street Name, City, London.</div>
			
					</div>

	</div>
</section>									</div>
									<div class="cl">
										
<section class="ctn-block ctn-block-small2 l"
	>
	<div class="ctn-inner-wrp">
		
									
								<span class="img-wrp" style="max-width:40px; ">
								<figure class="icon vm" style="font-size: 20px;"><i class="fa fa-phone"></i></figure>
				</span>
					
		<div class="text-wrp" style="margin-left:50px;">
							<h3 class="name">
										Phone:									</h3>
			
						<div class="text">(123) 456-7890</div>
			
					</div>

	</div>
</section>									</div>
									<div class="cl">
										
<section class="ctn-block ctn-block-small2 l"
	>
	<div class="ctn-inner-wrp">
		
									
								<span class="img-wrp" style="max-width:40px; ">
								<figure class="icon vm" style="font-size: 20px;"><i class="fa fa-envelope"></i></figure>
				</span>
					
		<div class="text-wrp" style="margin-left:50px;">
							<h3 class="name">
										Email:									</h3>
			
						<div class="text"><a href="mailto: mail@example.com">mail@example.com</a></div>
			
					</div>

	</div>
</section>									</div>
									<div class="cl">
										
<section class="ctn-block ctn-block-small2 l"
	>
	<div class="ctn-inner-wrp">
		
									
								<span class="img-wrp" style="max-width:40px; ">
								<figure class="icon vm" style="font-size: 20px;"><i class="fa fa-clock-o"></i></figure>
				</span>
					
		<div class="text-wrp" style="margin-left:50px;">
							<h3 class="name">
										Working Days / Hours:									</h3>
			
						<div class="text">Mon - Sun / 9:00AM - 8:00PM</div>
			
					</div>

	</div>
</section>									</div>
																	</div>
																<script>
								jQuery(document).ready(function($) {
									equalH('#rgen-contentblocks-rgenBpnGR8 .contentblock-grid', '#rgen-contentblocks-rgenBpnGR8 .contentblock-grid > .cl');
								});
								</script>
																
								
							</div>
														<div class="cl cl4 d-xl4 t-xl4 m-xl12 m-sm12 m-xs12 ">

																								<h3 class="sub-mod-hd" style="">Theme features</h3>
																
																<div class="contentblock-grid rw eq1 d-eq1 t-eq1 mxl-eq1 msm-eq1 mxs-eq1 gt0 mb4">
																		<div class="cl">
										
<section class="ctn-block ctn-block-small2 l"
	>
	<div class="ctn-inner-wrp">
		
									
								<span class="img-wrp" style="max-width:25px; top:-6px; ">
								<figure class="icon vm" style="font-size: 20px;background-color: rgba(255, 255, 255, 0); font-size: 20px; text-align: center; width:25px;height:25px;"><i class="fa fa-check"></i></figure>
				</span>
					
		<div class="text-wrp" style="margin-left:35px;">
							<h3 class="name">
										Advanced Grid manager									</h3>
			
			
					</div>

	</div>
</section>									</div>
									<div class="cl">
										
<section class="ctn-block ctn-block-small2 l"
	>
	<div class="ctn-inner-wrp">
		
									
								<span class="img-wrp" style="max-width:25px; top:-6px; ">
								<figure class="icon vm" style="font-size: 20px;background-color: rgba(255, 255, 255, 0); font-size: 20px; text-align: center; width:25px;height:25px;"><i class="fa fa-check"></i></figure>
				</span>
					
		<div class="text-wrp" style="margin-left:35px;">
							<h3 class="name">
										SEO Image gallery									</h3>
			
			
					</div>

	</div>
</section>									</div>
									<div class="cl">
										
<section class="ctn-block ctn-block-small2 l"
	>
	<div class="ctn-inner-wrp">
		
									
								<span class="img-wrp" style="max-width:25px; top:-6px; ">
								<figure class="icon vm" style="font-size: 20px;background-color: rgba(255, 255, 255, 0); font-size: 20px; text-align: center; width:25px;height:25px;"><i class="fa fa-check"></i></figure>
				</span>
					
		<div class="text-wrp" style="margin-left:35px;">
							<h3 class="name">
										Advanced styling options									</h3>
			
			
					</div>

	</div>
</section>									</div>
									<div class="cl">
										
<section class="ctn-block ctn-block-small2 l"
	>
	<div class="ctn-inner-wrp">
		
									
								<span class="img-wrp" style="max-width:25px; top:-6px; ">
								<figure class="icon vm" style="font-size: 20px;background-color: rgba(255, 255, 255, 0); font-size: 20px; text-align: center; width:25px;height:25px;"><i class="fa fa-check"></i></figure>
				</span>
					
		<div class="text-wrp" style="margin-left:35px;">
							<h3 class="name">
										Mega menu module									</h3>
			
			
					</div>

	</div>
</section>									</div>
									<div class="cl">
										
<section class="ctn-block ctn-block-small2 l"
	>
	<div class="ctn-inner-wrp">
		
									
								<span class="img-wrp" style="max-width:25px; top:-6px; ">
								<figure class="icon vm" style="font-size: 20px;background-color: rgba(255, 255, 255, 0); font-size: 20px; text-align: center; width:25px;height:25px;"><i class="fa fa-check"></i></figure>
				</span>
					
		<div class="text-wrp" style="margin-left:35px;">
							<h3 class="name">
										Custom positions									</h3>
			
			
					</div>

	</div>
</section>									</div>
									<div class="cl">
										
<section class="ctn-block ctn-block-small2 l"
					onclick="window.open('http://themeforest.net/item/rgen-opencart-modern-store-design/2699592?ref=R_GENESIS','mywindow');" style="cursor: pointer;"
			>
	<div class="ctn-inner-wrp">
		
									
								<span class="img-wrp" style="max-width:25px; top:-6px; ">
								<figure class="icon vm" style="font-size: 20px;background-color: rgba(255, 255, 255, 0); font-size: 20px; text-align: center; width:25px;height:25px;"><i class="fa fa-check"></i></figure>
				</span>
					
		<div class="text-wrp" style="margin-left:35px;">
							<h3 class="name">
										Product groups									</h3>
			
			
					</div>

	</div>
</section>									</div>
									<div class="cl">
										
<section class="ctn-block ctn-block-small2 l"
	>
	<div class="ctn-inner-wrp">
		
									
								<span class="img-wrp" style="max-width:25px; top:-6px; ">
								<figure class="icon vm" style="font-size: 20px;background-color: rgba(255, 255, 255, 0); font-size: 20px; text-align: center; width:25px;height:25px;"><i class="fa fa-check"></i></figure>
				</span>
					
		<div class="text-wrp" style="margin-left:35px;">
							<h3 class="name">
										Revolution Slider OpenCart									</h3>
			
			
					</div>

	</div>
</section>									</div>
									<div class="cl">
										
<section class="ctn-block ctn-block-small2 l"
					onclick="window.open('http://themeforest.net/item/rgen-opencart-modern-store-design/2699592?ref=R_GENESIS','mywindow');" style="cursor: pointer;"
			>
	<div class="ctn-inner-wrp">
		
									
								<span class="img-wrp" style="max-width:25px; top:-6px; ">
								<figure class="icon vm" style="font-size: 20px;background-color: rgba(255, 255, 255, 0); font-size: 20px; text-align: center; width:25px;height:25px;"><i class="fa fa-arrow-circle-right"></i></figure>
				</span>
					
		<div class="text-wrp" style="margin-left:35px;">
							<h3 class="name">
										Try out demo admin									</h3>
			
			
					</div>

	</div>
</section>									</div>
																	</div>
																<script>
								jQuery(document).ready(function($) {
									equalH('#rgen-contentblocks-rgenBpnGR8 .contentblock-grid', '#rgen-contentblocks-rgenBpnGR8 .contentblock-grid > .cl');
								});
								</script>
																
								
							</div>
														<div class="cl cl4 d-xl4 t-xl4 m-xl12 m-sm12 m-xs12 ">

																								<h3 class="sub-mod-hd" style="">About us</h3>
																
																<div class="contentblock-grid rw eq1 d-eq1 t-eq1 mxl-eq1 msm-eq1 mxs-eq1 gt0 mb0">
																		<div class="cl">
										<section class="ctn-block ctn-block1 "
	>
	<div class="ctn-inner-wrp">

				
				
		<div class="text-wrp">
						
						<div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent condimentum ligula in felis blandit eleifend. Aliquam erat volutpat. Aenean tempor lectus ut augue bibendum consequat. Morbi vitae ex malesuada, laoreet sem eu, ullamcorper arcu. Fusce nunc erat, gravida et massa a, interdum congue eros. Sed in ornare nisl. Donec nec purus non elit maximus convallis. Vestibulum a gravida eros. Nulla ac lacus nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus</div>
					</div>

		
	</div>
</section>									</div>
																	</div>
																
								
							</div>
													</div>
					
				</div>
			</div>
					</div>

		
	</div>
</div>

