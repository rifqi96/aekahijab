<div id="rgen-contentblocks-rgenipTQu2" class="rgen-contentblocks contentblocks-rgCKl">
	<div class="mod-wrp container">
		
						
				<section class="mod-customhtml t-html"><h2 style="font-size: 30px; font-weight: 400; margin-bottom: 5px;">R.Gen content blocks module</h2>
<hr style="width: 100px; margin: 20px auto; border-top: 4px solid rgba(0,0,0,0.06);">
<p style="font-size: 16px; line-height:1.4;">
	R.Gen content blocks module display any content with custom design and layout.<br>
	Create information blocks with full design customization options.
</p></section>	
				
		<div class="rw gt40">
						
			<div class="m-cl cl12 t-xl12">
				<div class="mod-content">
					
											<div class="rw gt20 mb20">
														<div class="cl cl12 d-xl12 t-xl12 m-xl12 m-sm12 m-xs12 ">

																								
																<div class="contentblock-grid rw eq3 d-eq3 t-eq3 mxl-eq1 msm-eq1 mxs-eq1 gt40 mb40">
																		<div class="cl">
										
<section class="ctn-block ctn-block-small2 l"
	>
	<div class="ctn-inner-wrp">
		
									
								<span class="img-wrp" style="max-width:60px; ">
								<figure class="icon vm" style="font-size: 14px;background-color: rgba(102, 102, 102, 0.5); color: rgb(255, 255, 255); font-size: 25px; text-align: center; border: 0px; border-style: solid;border-color: #eeeeee;border-radius: 30px;width:60px;height:60px;"><i class="fa fa-sliders"></i></figure>
				</span>
					
		<div class="text-wrp" style="margin-left:75px;">
							<h3 class="name">
										Custom layout design									</h3>
			
						<div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
			
					</div>

	</div>
</section>									</div>
									<div class="cl">
										
<section class="ctn-block ctn-block-small2 l"
	>
	<div class="ctn-inner-wrp">
		
									
								<span class="img-wrp" style="max-width:60px; ">
								<figure class="icon vm" style="font-size: 14px;background-color: rgba(102, 102, 102, 0.5); color: rgb(255, 255, 255); font-size: 25px; text-align: center; border: 0px; border-style: solid;border-color: #eeeeee;border-radius: 30px;width:60px;height:60px;"><i class="fa fa-eyedropper"></i></figure>
				</span>
					
		<div class="text-wrp" style="margin-left:75px;">
							<h3 class="name">
										Easy customization									</h3>
			
						<div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
			
					</div>

	</div>
</section>									</div>
									<div class="cl">
										
<section class="ctn-block ctn-block-small2 l"
	>
	<div class="ctn-inner-wrp">
		
									
								<span class="img-wrp" style="max-width:60px; ">
								<figure class="icon vm" style="font-size: 14px;background-color: rgba(102, 102, 102, 0.5); color: rgb(255, 255, 255); font-size: 25px; text-align: center; border: 0px; border-style: solid;border-color: #eeeeee;border-radius: 30px;width:60px;height:60px;"><i class="glyphicon glyphicon-th-large"></i></figure>
				</span>
					
		<div class="text-wrp" style="margin-left:75px;">
							<h3 class="name">
										Grid / Carousel options									</h3>
			
						<div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
			
					</div>

	</div>
</section>									</div>
																	</div>
																<script>
								jQuery(document).ready(function($) {
									equalH('#rgen-contentblocks-rgenipTQu2 .contentblock-grid', '#rgen-contentblocks-rgenipTQu2 .contentblock-grid > .cl');
								});
								</script>
																
								
							</div>
														<div class="cl cl12 d-xl12 t-xl12 m-xl12 m-sm12 m-xs12 ">

																								
																<div class="contentblock-grid rw eq3 d-eq3 t-eq3 mxl-eq1 msm-eq1 mxs-eq1 gt40 mb0">
																		<div class="cl">
										
<section class="ctn-block ctn-block-small2 l"
	>
	<div class="ctn-inner-wrp">
		
									
								<span class="img-wrp" style="max-width:60px; ">
								<figure class="icon vm" style="font-size: 14px;background-color: rgba(102, 102, 102, 0.5); color: rgb(255, 255, 255); font-size: 25px; text-align: center; border: 0px; border-style: solid;border-color: #eeeeee;border-radius: 30px;width:60px;height:60px;"><i class="fa fa-code"></i></figure>
				</span>
					
		<div class="text-wrp" style="margin-left:75px;">
							<h3 class="name">
										Custom HTML around blocks									</h3>
			
						<div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
			
					</div>

	</div>
</section>									</div>
									<div class="cl">
										
<section class="ctn-block ctn-block-small2 l"
	>
	<div class="ctn-inner-wrp">
		
									
								<span class="img-wrp" style="max-width:60px; ">
								<figure class="icon vm" style="font-size: 14px;background-color: rgba(102, 102, 102, 0.5); color: rgb(255, 255, 255); font-size: 25px; text-align: center; border: 0px; border-style: solid;border-color: #eeeeee;border-radius: 30px;width:60px;height:60px;"><i class="fa fa-language"></i></figure>
				</span>
					
		<div class="text-wrp" style="margin-left:75px;">
							<h3 class="name">
										Multi language support									</h3>
			
						<div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
			
					</div>

	</div>
</section>									</div>
									<div class="cl">
										
<section class="ctn-block ctn-block-small2 l"
	>
	<div class="ctn-inner-wrp">
		
									
								<span class="img-wrp" style="max-width:60px; ">
								<figure class="icon vm" style="font-size: 14px;background-color: rgba(102, 102, 102, 0.5); color: rgb(255, 255, 255); font-size: 25px; text-align: center; border: 0px; border-style: solid;border-color: #eeeeee;border-radius: 30px;width:60px;height:60px;"><i class="fa fa-check-square-o"></i></figure>
				</span>
					
		<div class="text-wrp" style="margin-left:75px;">
							<h3 class="name">
										Easy icon selection									</h3>
			
						<div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
			
					</div>

	</div>
</section>									</div>
																	</div>
																<script>
								jQuery(document).ready(function($) {
									equalH('#rgen-contentblocks-rgenipTQu2 .contentblock-grid', '#rgen-contentblocks-rgenipTQu2 .contentblock-grid > .cl');
								});
								</script>
																
								
							</div>
													</div>
					
				</div>
			</div>
					</div>

				<section class="mod-customhtml b-html"><hr style="width: 100px; margin: 30px auto; border-top: 4px solid rgba(0,0,0,0.06);">
<a href="http://themeforest.net/item/rgen-opencart-modern-store-design/2699592?ref=R_GENESIS" style="font-size: 24px; font-weight: 400; background-color: rgb(64, 64, 72); border-color: rgb(64, 64, 72); padding: 10px 25px; line-height: 1; border-radius: 3px; margin: 0 auto;" class="default-btn">Purchase</a></section>	
		
	</div>
</div>

