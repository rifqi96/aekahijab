
<div id="rgen-customhtml-rgentKezCZ" class="rgen-customhtml customhtml-rgOc2">
	<div class="mod-wrp container">
		
						
		<div class="mod-content">

						<section class="mod-customhtml t-html"><h2 style="font-size: 30px; font-weight: 400; margin-bottom: 5px; color: inherit">R.Gen Custom HTML module</h2>
<hr style="width: 100px; margin: 20px auto; border-top: 4px solid rgba(255,255,255,0.06);">
<p style="font-size: 16px; line-height:1.4; color: rgba(255,255,255,0.5)">Insert any code according to needs and easy to manage</p></section>	
						
						<div class="rw gt0">
								<div class="l-cl cl6 t-xl12">
										<section class="mod-customhtml l-html"><h2 style="color: inherit; text-align: center;">Youtube Video</h2>
<iframe width="100%" height="400" src="https://www.youtube.com/embed/dorZ3vag5PI" frameborder="0" allowfullscreen></iframe></section>	
									</div>	
								
				
								<div class="r-cl cl6 t-xl12">
										<section class="mod-customhtml r-html"><h2 style="color: inherit; text-align: center;">Flickr Slideshow</h2>
<iframe src="https://www.flickr.com/photos/we-are-envato/13090422515/player/" width="100%" height="400" frameborder="0" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe></section>	
									</div>
							</div>
						
			
		</div>
	</div>
</div>
