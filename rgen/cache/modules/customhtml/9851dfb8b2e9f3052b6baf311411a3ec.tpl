
<div id="rgen-customhtml-rgenO6RlKP" class="rgen-customhtml customhtml-rgmDW">
	<div class="mod-wrp container">
		
						
		<div class="mod-content">

						
						<div class="rw gt60">
								<div class="l-cl cl4 t-xl12">
										<section class="mod-customhtml l-html"><h3 class="h3" style="margin-bottom:20px;">About us</h3>
<p style="line-height: 1.6">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</p></section>	
									</div>	
								
								<div class="m-cl cl4 t-xl12">
										<section class="mod-customhtml m-html"><h3 class="h3" style="margin-bottom:20px;">Twitter</h3>
<a 
class="twitter-timeline" 
href="https://twitter.com/RGenesisArt" 
data-widget-id="345031699077410817"
width="100%"
data-chrome="noheader nofooter noscrollbar transparent"
data-tweet-limit="2"
>Tweets by @RGenesisArt</a>

<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></section>	
									</div>
				
								<div class="r-cl cl4 t-xl12">
										<section class="mod-customhtml r-html"><h3 class="h3" style="margin-bottom:20px;">Facebook</h3>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=481834025161188";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-page" data-width="615" data-href="https://www.facebook.com/envato" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/envato"><a href="https://www.facebook.com/envato">Envato</a></blockquote></div></div></section>	
									</div>
							</div>
						
			
		</div>
	</div>
</div>
