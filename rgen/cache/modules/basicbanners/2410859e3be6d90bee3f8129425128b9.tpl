<div id="rgen-basicbanners-rgenc7Nibg" class="rgen-basicbanners basicbanners-rgf4A">
	<div class="mod-wrp">
				
				<section class="mod-customhtml"><h2 style="font-size: 30px; font-weight: 400; margin-bottom: 5px; color: inherit">R.Gen banner module</h2>
<hr style="width: 100px; margin: 20px auto; border-top: 4px solid rgba(255,255,255,0.06);">
<p style="font-size: 16px; line-height:1.4; color: rgba(255,255,255,0.5)">
	Generate banners with 6 pre-define hover effects.
	Easy to manage with full design customization options.
</p></section>	
		
		<div class="mod-content">
						<div class="rw  eq4 d-eq4 t-eq4 mxl-eq2 msm-eq2 mxs-eq1 gt1 mb1">
								<div class="cl">
							<figure class="effect-6 h-effect">
			<img src="http://localhost/hijab.com/image/cache/catalog/rgen/demo01_images/banners/gallery/01-400x400.jpg" alt="R.Gen - OpenCart Modern Store Design" class="img-responsive" />
			<figcaption>
				<h2 class="h3">Save up to 40%</h2>
				<p><a href="http://themeforest.net/item/rgen-opencart-modern-store-design/2699592?ref=R_GENESIS" style="border-radius: 3px;" class="default-btn">View all</a></p>
								<a href="http://themeforest.net/item/rgen-opencart-modern-store-design/2699592?ref=R_GENESIS" target="_blank"><img src="image/spacer.gif" alt="R.Gen - OpenCart Modern Store Design"></a>
							</figcaption>
		</figure>
					</div>
								<div class="cl">
							<figure class="effect-6 h-effect">
			<img src="http://localhost/hijab.com/image/cache/catalog/rgen/demo01_images/banners/gallery/02-400x400.jpg" alt="R.Gen - OpenCart Modern Store Design" class="img-responsive" />
			<figcaption>
				<h2 class="h3">Extra 30% OFF</h2>
				<p><a href="http://themeforest.net/item/rgen-opencart-modern-store-design/2699592?ref=R_GENESIS" style="border-radius: 3px;" class="default-btn">View all</a></p>
								<a href="http://themeforest.net/item/rgen-opencart-modern-store-design/2699592?ref=R_GENESIS" target="_blank"><img src="image/spacer.gif" alt="R.Gen - OpenCart Modern Store Design"></a>
							</figcaption>
		</figure>
					</div>
								<div class="cl">
							<figure class="effect-6 h-effect">
			<img src="http://localhost/hijab.com/image/cache/catalog/rgen/demo01_images/banners/gallery/03-400x400.jpg" alt="R.Gen - OpenCart Modern Store Design" class="img-responsive" />
			<figcaption>
				<h2 class="h3">Buy 1 Get 1 FREE</h2>
				<p><a href="http://themeforest.net/item/rgen-opencart-modern-store-design/2699592?ref=R_GENESIS" style="border-radius: 3px;" class="default-btn">View all</a></p>
								<a href="http://themeforest.net/item/rgen-opencart-modern-store-design/2699592?ref=R_GENESIS" target="_blank"><img src="image/spacer.gif" alt="R.Gen - OpenCart Modern Store Design"></a>
							</figcaption>
		</figure>
					</div>
								<div class="cl">
							<figure class="effect-6 h-effect">
			<img src="http://localhost/hijab.com/image/cache/catalog/rgen/demo01_images/banners/gallery/05-400x400.jpg" alt="R.Gen - OpenCart Modern Store Design" class="img-responsive" />
			<figcaption>
				<h2 class="h3">Special Summer collection</h2>
				<p><a href="http://themeforest.net/item/rgen-opencart-modern-store-design/2699592?ref=R_GENESIS" style="border-radius: 3px;" class="default-btn">View all</a></p>
								<a href="http://themeforest.net/item/rgen-opencart-modern-store-design/2699592?ref=R_GENESIS" target="_blank"><img src="image/spacer.gif" alt="R.Gen - OpenCart Modern Store Design"></a>
							</figcaption>
		</figure>
					</div>
								<div class="cl">
							<figure class="effect-6 h-effect">
			<img src="http://localhost/hijab.com/image/cache/catalog/rgen/demo01_images/banners/gallery/06-400x400.jpg" alt="R.Gen - OpenCart Modern Store Design" class="img-responsive" />
			<figcaption>
				<h2 class="h3">Great SAVING up to 35%</h2>
				<p><a href="http://themeforest.net/item/rgen-opencart-modern-store-design/2699592?ref=R_GENESIS" style="border-radius: 3px;" class="default-btn">View all</a></p>
								<a href="http://themeforest.net/item/rgen-opencart-modern-store-design/2699592?ref=R_GENESIS" target="_blank"><img src="image/spacer.gif" alt="R.Gen - OpenCart Modern Store Design"></a>
							</figcaption>
		</figure>
					</div>
								<div class="cl">
							<figure class="effect-6 h-effect">
			<img src="http://localhost/hijab.com/image/cache/catalog/rgen/demo01_images/banners/gallery/04-400x400.jpg" alt="R.Gen - OpenCart Modern Store Design" class="img-responsive" />
			<figcaption>
				<h2 class="h3">Men's Suit + Shirt<br>starts from $99</h2>
				<p><a href="http://themeforest.net/item/rgen-opencart-modern-store-design/2699592?ref=R_GENESIS" style="border-radius: 3px;" class="default-btn">View all</a></p>
								<a href="http://themeforest.net/item/rgen-opencart-modern-store-design/2699592?ref=R_GENESIS" target="_blank"><img src="image/spacer.gif" alt="R.Gen - OpenCart Modern Store Design"></a>
							</figcaption>
		</figure>
					</div>
								<div class="cl">
							<figure class="effect-6 h-effect">
			<img src="http://localhost/hijab.com/image/cache/catalog/rgen/demo01_images/banners/gallery/07-400x400.jpg" alt="R.Gen - OpenCart Modern Store Design" class="img-responsive" />
			<figcaption>
				<h2 class="h3">ACCESSORIES<br>Limited edition</h2>
				<p><a href="http://themeforest.net/item/rgen-opencart-modern-store-design/2699592?ref=R_GENESIS" style="border-radius: 3px;" class="default-btn">View all</a></p>
								<a href="http://themeforest.net/item/rgen-opencart-modern-store-design/2699592?ref=R_GENESIS" target="_blank"><img src="image/spacer.gif" alt="R.Gen - OpenCart Modern Store Design"></a>
							</figcaption>
		</figure>
					</div>
								<div class="cl">
							<figure class="effect-6 h-effect">
			<img src="http://localhost/hijab.com/image/cache/catalog/rgen/demo01_images/banners/gallery/08-400x400.jpg" alt="R.Gen - OpenCart Modern Store Design" class="img-responsive" />
			<figcaption>
				<h2 class="h3">New collection for<br>Party and Occasion</h2>
				<p><a href="http://themeforest.net/item/rgen-opencart-modern-store-design/2699592?ref=R_GENESIS" style="border-radius: 3px;" class="default-btn">View all</a></p>
								<a href="http://themeforest.net/item/rgen-opencart-modern-store-design/2699592?ref=R_GENESIS" target="_blank"><img src="image/spacer.gif" alt="R.Gen - OpenCart Modern Store Design"></a>
							</figcaption>
		</figure>
					</div>
							</div>
					</div>

	</div>
</div>
