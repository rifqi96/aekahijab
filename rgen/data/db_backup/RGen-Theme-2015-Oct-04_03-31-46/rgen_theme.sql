-- ------------------------------------
-- 
-- R.Gen theme setting
-- 
-- ------------------------------------

TRUNCATE TABLE `oc_rgen_theme`;

INSERT INTO `oc_rgen_theme` (`store_id`, `group`, `section`, `key`, `value`) VALUES ('0', 'rgen_theme', 'rgen_topbar', 'topbar_common', '{"style":1,"layout":"fw","autosearch":true}');


TRUNCATE TABLE `oc_rgen_settings`;

INSERT INTO `oc_rgen_settings` (`store_id`, `group`, `section`, `key`, `value`) VALUES ('0', 'rgen_settings', 'rgen_optimization', 'system_optimization', '{"cssminify":true,"jsminify":true,"menu":true,"basicslideshow":true,"basicbanners":true,"bannergrids":true,"imagegallery":true,"productgroups":true,"customhtml":true,"contentblocks":true,"revslider":true,"gridmanager":true,"stickyhtml":true,"catshowcase":true,"deals":true}');
INSERT INTO `oc_rgen_settings` (`store_id`, `group`, `section`, `key`, `value`) VALUES ('0', 'rgen_settings', 'rgen_topbar', 'style', '1');
INSERT INTO `oc_rgen_settings` (`store_id`, `group`, `section`, `key`, `value`) VALUES ('0', 'rgen_settings', 'rgen_topbar', 'layout', '"fw"');
INSERT INTO `oc_rgen_settings` (`store_id`, `group`, `section`, `key`, `value`) VALUES ('0', 'rgen_settings', 'rgen_topbar', 'autosearch', 'true');
