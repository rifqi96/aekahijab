-- ------------------------------------
-- 
-- Module - contentblocks
-- 
-- ------------------------------------

DELETE FROM `oc_rgen_modules` WHERE `section` LIKE '%contentblocks%';



DELETE FROM `oc_rgen_modules_customize` WHERE `key` LIKE '%contentblocks%';



DELETE FROM `oc_layout_module` WHERE `code` LIKE '%contentblocks%';



DELETE FROM `oc_setting` WHERE `code` LIKE '%contentblocks%';

