function getURLVar(key) {
    var value = [];
    
    var query = String(document.location).split('?');
    
    if (query[1]) {
        var part = query[1].split('&');

        for (i = 0; i < part.length; i++) {
            var data = part[i].split('=');
            
            if (data[0] && data[1]) {
                value[data[0]] = data[1];
            }
        }
        
        if (value[key]) {
            return value[key];
        } else {
            return '';
        }
    }
} 

function addToCart(product_id, quantity) {
    quantity = typeof(quantity) != 'undefined' ? quantity : 1;

    $.ajax({
	    url: 'index.php?route=checkout/cart/add',
	    type: 'post',
	    data: 'product_id=' + product_id + '&quantity=' + quantity,
	    dataType: 'json',
	    success: function(json) {
		    $('.success, .warning, .attention, .information, .error').remove();
		    
		    if (json['redirect']) {
			    location = json['redirect'];
		    }
		    
		    if (json['success']) {
			    $('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/moda/img/close.png" alt="" class="close" /></div>');
			    
			    $('.success').fadeIn('slow');
			    
                if (opencart_version >= 2000) 
                {
					$('.cart').load('index.php?route=common/cart/info  .cart > *', function () {$(".cart .cart-info").css("visibility", "visible");});
                } else 
                {
					$('.cart').load('index.php?route=module/cart .cart > *', function () {$(".cart .cart-info").css("visibility", "visible");});
                }
                
                setTimeout(function() {$('.success').fadeOut('slow');}, 5000);
		    }	
	    }
    });
    
    return false;
}
		

$(window).on('load', function () 
{
	$('.selectpicker').selectpicker({});

	
	  // Cart Dropdown
	  /*
	$(document).on('click', '#cart.dropdown-toggle', function() {
	    $('#cart').load('/index.php?route=module/cart .cart > *');
	});*/
	

	var cart_hover = true;
	$(document).on("mouseenter", "#cart.dropdown-toggle",function(e)
	{

		if (cart_hover)
		{
			//$(".cart .cart-info").css("visibility", "visible");
			if (opencart_version >= 2000) 
			{
				$('.cart').load('index.php?route=common/cart/info  .cart > *', function () {$(".cart .cart-info").css("visibility", "visible");});
			} else 
			{
				$('.cart').load('index.php?route=module/cart .cart > *', function () {$(".cart .cart-info").css("visibility", "visible");});
			}
		}
		
		cart_hover = false;
		setTimeout(function () {cart_hover= true}, 1000);
		
		e.preventDefault();		
	});


	 $(".header .navbar input.search-query").autocomplete("/index.php?route=product/search&filter_name=");
	 
	 
	/* Search */
	$('.navbar-search button.icon-search').on('click', function(e) {
		url = $('base').prop('href') + 'index.php?route=product/search';
				 
		var search = $('.navbar-search input.search-query').prop('value');

		if (search) {
			url += '&search=' + encodeURIComponent(search);
		} else
		{
			var filter_name = $('input[name=\'filter_name\']').prop('value');
		
			if (filter_name) {
				url += '&filter_name=' + encodeURIComponent(filter_name);
			}
		}
		
		window.location = url;
		e.preventDefault();
		return false;
	});
	
	$('.navbar-form input.search-query').on('keydown', function(e) {
		if (e.keyCode == 13) {
			url = $('base').prop('href') + 'index.php?route=product/search';
			 
			var search = $('.navbar-search input.search-query').prop('value');
			
			if (search) {
				url += '&search=' + encodeURIComponent(search);
			}
			
			window.location = url;
		}
	}).click(function (e) 
	{
		this.value = '';
	});


	$(".navbar-form input.search-query").autocomplete($('base').attr('href')  + "index.php?route=product/search&filter_name=");

	
	$('#header input[name=\'filter_name\']').on('keydown', function(e) {
		this.style.color = '#000000';
		if (e.keyCode == 13) {
			url = $('base').prop('href') + 'index.php?route=product/search';
			 
			var filter_name = $('input[name=\'filter_name\']').prop('value');
			
			if (filter_name) {
				url += '&filter_name=' + encodeURIComponent(filter_name);
			}
			
			window.location = url;
		}
	}).click(function (e) 
	{
		this.value = '';
	});
	


	// Product List
	$('#list-view').click(function() {
		
		$('#content .product-layout > div').addClass('product-list');
		$('#content .product-layout > div > div').attr('class','col-md-12');

		$('#content .product-layout > div .product > div.image').attr('class', 'image ' + _nico_category_cols);
		$('#content .product-layout > div .product > div.actions').attr('class', 'actions col-md-7');
		
		localStorage.setItem('display', 'list');
		 $('#list-view').addClass("selected");
		 $('#grid-view').removeClass("selected");
	});

	// Product Grid
	$('#grid-view').click(function() {
			
		 $('#content .product-layout > div').removeClass('product-list');

		 $('#content .product-layout > div > div').attr('class',_nico_category_cols);
		 $('#content .product-layout > div .product > div.image').attr('class', 'image');
		 $('#content .product-layout > div .product > div.actions').attr('class', 'actions');
		
		 localStorage.setItem('display', 'grid');
		 $('#list-view').removeClass("selected");
		 $('#grid-view').addClass("selected");
	});

	if (localStorage.getItem('display') == 'list') {
		$('#list-view').trigger('click');
	} else {
		$('#grid-view').trigger('click');
	}
	
	jQuery("span.quickview").click(function (event) 
	{
		$.colorbox({iframe:false,width:800,height:700,href:this.parentNode.href + "&ajax=true"});
		event.stopPropagation();
		event.stopImmediatePropagation();
		event.preventDefault();
		return false;
	});

	$('#notification').delegate('.success img, .warning img, .attention img, .information img','click', function() {
		$(this).parent().fadeOut('slow', function() {
			$(this).remove();
		});
	});	
});





// Cart add remove functions	
var cart = {
	'add': function(product_id, quantity) {
		$.ajax({
			url: 'index.php?route=checkout/cart/add',
			type: 'post',
			data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},      
			success: function(json) {
				$('.alert, .text-danger').remove();
				
				$('#cart > button').button('reset');
				
				if (json['redirect']) {
					location = json['redirect'];
				}
				
				if (json['success']) {
					$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					
					$('#cart-total').html(json['total']);
					
					//check if not popup
					if (jQuery("#cboxLoadedContent .container").length = 0)
					$('html, body').animate({ scrollTop: 0 }, 'slow');

					if (opencart_version >= 2000) 
					{
						$('.cart').load('index.php?route=common/cart/info  .cart > *', function () {$(".cart .cart-info").css("visibility", "visible");});
					} else 
					{
						$('.cart').load('index.php?route=module/cart .cart > *', function () {$(".cart .cart-info").css("visibility", "visible");});
					}
				}
			}
		});
	},
	'update': function(key, quantity) {
		if (opencart_version >= 2000) 
		{
			cart_action = 'edit';
		} else 
		{
			cart_action = 'update';
		}
		$.ajax({
			url: 'index.php?route=checkout/cart/' + cart_action,
			type: 'post',
			data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},      
			success: function(json) {
				$('#cart > button').button('reset');
				
				$('#cart-total').html(json['total']);
				
				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					if (opencart_version >= 2000) 
					{
						$('.cart').load('index.php?route=common/cart/info  .cart > *', function () {$(".cart .cart-info").css("visibility", "visible");});
					} else 
					{
						$('.cart').load('index.php?route=module/cart .cart > *', function () {$(".cart .cart-info").css("visibility", "visible");});
					}
				}			
			}
		});			
	},
	'remove': function(key) {
		$.ajax({
			url: 'index.php?route=checkout/cart/remove',
			type: 'post',
			data: 'key=' + key,
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},      			
			success: function(json) {
				$('#cart > button').button('reset');
				
				$('#cart-total').html(json['total']);
				
				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					if (opencart_version >= 2000) 
					{
						$('.cart').load('index.php?route=common/cart/info  .cart > *', function () {$(".cart .cart-info").css("visibility", "visible");});
					} else 
					{
						$('.cart').load('index.php?route=module/cart .cart > *', function () {$(".cart .cart-info").css("visibility", "visible");});
					}
				}
			}
		});			
	}
}

var voucher = {
	'add': function() {
		
	},
	'remove': function(key) {
		if (opencart_version >= 2000) 
		{
			cart_action = 'checkout/cart/remove';
		} else 
		{
			cart_action = 'account/voucher/remove';
		}

		$.ajax({
			url: 'index.php?route=' + cart_action ,
			type: 'post',
			data: 'key=' + key,
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},      
			complete: function() {
				$('#cart > button').button('reset');
			},			
			success: function(json) {
				$('#cart-total').html(json['total']);
				
				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					if (opencart_version >= 2000) 
					{
						$('.cart').load('index.php?route=common/cart/info  .cart > *', function () {$(".cart .cart-info").css("visibility", "visible");});
					} else 
					{
						$('.cart').load('index.php?route=module/cart .cart > *', function () {$(".cart .cart-info").css("visibility", "visible");});
					}
				}			
			}
		});	
	}
}

var wishlist = {
	'add': function(product_id) {
		$.ajax({
			url: 'index.php?route=account/wishlist/add',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
				$('.alert').remove();
							
				if (json['success']) {
					$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				}

				if (json['info']) {
					$('#content').parent().before('<div class="alert alert-info"><i class="fa fa-info-circle"></i> ' + json['info'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				}
					
				$('#wishlist-total').html(json['total']);
				
				//check if not popup
				if (jQuery("#cboxLoadedContent .container").length = 0)
				$('html, body').animate({ scrollTop: 0 }, 'slow');
			}   
		});
	},
	'remove': function() {
	
	}
}

var compare = {
	'add': function(product_id) {
		$.ajax({
			url: 'index.php?route=product/compare/add',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
				$('.alert').remove();
							
				if (json['success']) {
					$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					
					$('#compare-total').html(json['total']);
					
					//check if not popup
					if (jQuery("#cboxLoadedContent .container").length = 0)
					$('html, body').animate({ scrollTop: 0 }, 'slow');
				}   
			}
		});
	},
	'remove': function() {
	
	}
}

/* Agree to Terms */
$(document).delegate('.agree, .colorbox', 'click', function(e) {
	e.preventDefault();

	$('#modal-agree').remove();

	var element = this;

	$.ajax({
		url: $(element).attr('href'),
		type: 'get',
		dataType: 'html',
		success: function(data) {
			html  = '<div id="modal-agree" class="modal">';
			html += '  <div class="modal-dialog">';
			html += '    <div class="modal-content">';
			html += '      <div class="modal-header">';
			html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
			html += '      </div>';
			html += '      <div class="modal-body">' + data + '</div>';
			html += '    </div';
			html += '  </div>';
			html += '</div>';

			$('body').append(html);

			$('#modal-agree').modal('show');
		}
	});
});

$(window).on('load', function () 
{
	$('.selectpicker').selectpicker({});
});

function isMobile()
{
	a = navigator.userAgent||navigator.vendor||window.opera;

	if(
	 typeof window.orientation !== 'undefined'
	 || window.innerWidth <= 800 && window.innerHeight <= 600
	 || ('ontouchstart' in document.documentElement)
	 || (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))
	 )
	 return true;
	return false;
}

function nico_grid(grid_element)
{
	var handler;		
	var options = {
	  autoResize: true, // This will auto-update the layout when the browser window is resized.
	  align:"left",
	  container: $('#' + grid_element), // Optional, used for some extra CSS styling
	  offset: 0,// Optional, the distance between grid items
	  itemWidth: jQuery('#' + grid_element + '.grid > div:first').outerWidth() - 1, // Optional, the width of a grid item
	  //fillEmptySpace: true // Optional, fill the bottom of each column with widths of flexible height
	};	    
		
//	  width = Math.round(($('#<?php echo $module_id;?>').outerWidth() - 20) / jQuery('.grid > div:first').width());
	  // Prepare layout options.
	    // Get a reference to your grid items.
	    handler = $('#' + grid_element + ' > div'),
		filters = $('.filters.' + grid_element  + ' li');

		handler.wookmark(options);
	    // Call the layout function.
	    setTimeout(function()
		{
//			    width = jQuery('#' + grid_element + '.grid > div:first').outerWidth() - 1;
//			    options.itemWidth =  width;
				handler.wookmark(options);
			}, 1000
		);
		/*handler.imagesLoaded(function() 
		{
				width = jQuery('.grid > div:first').outerWidth() - 1;
				options.width = options;
				handler.wookmark(options);
		});*/	

	    //jQuery(window).load(function() {handler.wookmark(options);});
	    jQuery(window).load(function() {handler.wookmark(options);});

	    /**
	     * When a filter is clicked, toggle it's active state and refresh.
	     */
	    var onClickFilter = function(event) {
	      var item = $(event.currentTarget),
		  activeFilters = [];

	      if (!item.hasClass('active')) {
		filters.removeClass('active');
	      }
	      item.toggleClass('active');

	      // Filter by the currently selected filter
	      if (item.hasClass('active')) {
		activeFilters.push(item.data('filter'));
	      }

	      handler.wookmarkInstance.filter(activeFilters);
	    }

	    // Capture filter click events.
	    filters.click(onClickFilter);    
}


function nico_carousel(module_id, cols_xs, cols_sm, cols_md, cols_lg)
{
		// store the slider in a local variable
	  var $window = $(window),
		  flexslider;    
		/*
	 function getGridSize<?php echo $module_id;?>() {
		return (window.innerWidth < 320) ? cols_xs:
			   (window.innerWidth < 767) ? cols_s,:
			   (window.innerWidth < 992) ? 12 / cols_md : 12 / cols_lg;
	  }*/

	//$window.load(function () {jQuery("#" + module_id + ".carousel > div").data('flexslider').resize();})  
	
	nico_carousel_resize = function(cols_xs, cols_sm, cols_md, cols_lg) 
	{
		return ((window.innerWidth <= 320) ? 12 / cols_xs:
		   (window.innerWidth < 767) ? 12 / cols_sm:
		   (window.innerWidth < 992) ? 12 / cols_md : 12 / cols_lg);
	};
	$("#" + module_id + '.carousel > div').flexslider({
		animation:"slide",
		easing:"",
		direction:"horizontal",
		startAt:0,
		initDelay:0,
		slideshowSpeed:7000,
		animationSpeed:600,
		prevText:"",
		nextText:"",
		pauseText:"Pause",
		playText:"Play",
		pausePlay:false,
		controlNav:false,
		slideshow:false,
		animationLoop:true,
		randomize:false,
		smoothHeight:false,
		useCSS:true,
		pauseOnHover:true,
		pauseOnAction:true,
		touch:true,
		video:false,
		mousewheel:false,
		keyboard:false,
		itemWidth:jQuery("#" + module_id + '.carousel .slides > li > div:first').outerWidth(),
		minItems: nico_carousel_resize(cols_xs, cols_sm, cols_md, cols_lg), // use function to pull in initial value
		maxItems: nico_carousel_resize(cols_xs, cols_sm, cols_md, cols_lg)// use function to pull in initial value
	});
}

var nico_tabs_carousel_resize;
function nico_tabs_carousel(module_id, cols_xs, cols_sm, cols_md, cols_lg)
{
  // store the slider in a local variable
  var $window = $(window),
	  flexslider;    

	nico_tabs_carousel_resize = function(cols_xs, cols_sm, cols_md, cols_lg) 
	{
		return ((window.innerWidth <= 320) ? 12 / cols_xs:
		   (window.innerWidth < 767) ? 12 / cols_sm:
		   (window.innerWidth < 992) ? 12 / cols_md : 12 / cols_lg);
	};

    $("#" + module_id + '_tabs a').click(function (e) {
	    e.preventDefault();
	    $(this).tab('show');
	    //_flexslider = jQuery("#" + module_id + " .carousel > div");
	    if (flexslider != null) 
	    {
			flexslider.resize()
		}
    });
    
    $("#" + module_id + '_tabs a:first').tab('show');

	  $window.resize(function() 
	  {
		 if (flexslider != null) flexslider.data('flexslider').vars.minItems = nico_tabs_carousel_resize(cols_xs, cols_sm, cols_md, cols_lg);
	  });
	  
	flexslider = $("#" + module_id + ' .carousel > div').flexslider({
		animation:"slide",
		easing:"",
		direction:"horizontal",
		startAt:0,
		initDelay:0,
		slideshowSpeed:7000,
		animationSpeed:600,
		prevText:"",
		nextText:"",
		pauseText:"Pause",
		playText:"Play",
		pausePlay:false,
		controlNav:false,
		slideshow:false,
		animationLoop:true,
		randomize:false,
		smoothHeight:false,
		useCSS:true,
		pauseOnHover:true,
		pauseOnAction:true,
		touch:true,
		video:false,
		mousewheel:false,
		keyboard:false,
		itemWidth:100,//jQuery("#" + module_id + ' .carousel > div:first').outerWidth() +  'px',
		minItems: nico_tabs_carousel_resize(cols_xs, cols_sm, cols_md, cols_lg), // use function to pull in initial value
		maxItems: nico_tabs_carousel_resize(cols_xs, cols_sm, cols_md, cols_lg)// use function to pull in initial value
	});
	
	if (flexslider != null) 
	{
		setTimeout(function () {flexslider.resize()}, 500);
	}	
}

function nico_sequence_slider(seq_module_id, autoplay, autoplay_interval)
{


	function sequence_height()
	{
		$("#sequence-" + seq_module_id ).height($("#sequence-" + $seq_module_id + " .background:first").height());	
	}

	var sequenceOptions = {
        autoPlay: autoplay ,
        autoPlayDelay: autoplay_interval,
        pauseOnHover: true,
		hidePreloaderUsingCSS: true,                   
        preloader: true,
        hidePreloaderDelay: 500,
	
        nextButton: true,
        prevButton: true,
        pauseButton: false,
        animateStartingFrameIn: false,    
        navigationSkipThreshold: 750,
        preventDelayWhenReversingAnimations: true,
        fadeFrameWhenSkipped:false,
        /*moveActiveFrameToTop:false,*/
    };

    var sequence = $("#sequence-" +  seq_module_id).sequence(sequenceOptions).data("sequence");

	function youtube_control(frame, func)
    {
			//func = pauseVideo/playVideo;
			youtube = jQuery("iframe.youtube", frame);
			if (youtube.length)
			{
					youtube.get(0).contentWindow.postMessage('{"event":"command","func":"' + func + '","args":""}',"*"); 
			}
	}

    function vimeo_control(frame, func)
    {
			//func = pause/play;
			vimeo = jQuery("iframe.vimeo", frame);
			if (vimeo.length)
			{
				vimeo.get(0).contentWindow.postMessage('{"method":"' + func + '"}',"*"); 
			}
	}

	sequence.beforeNextFrameAnimatesIn = function()
	{
		youtube_control(sequence.currentFrame, "pauseVideo");
		youtube_control(sequence.nextFrame, "playVideo");

		vimeo_control(sequence.currentFrame, "pause");
		vimeo_control(sequence.nextFrame, "play");
	}
}

function nico_google_maps(module_id, lat, long, zoom, markers)
{
	var mapProp = 
	{
	  center:new google.maps.LatLng(lat, long),
	  zoom:zoom,
	  mapTypeId:google.maps.MapTypeId.ROADMAP
	};

	var map=new google.maps.Map(document.getElementById(module_id),mapProp);
	var infowindow = new google.maps.InfoWindow();
	
	var i;
	for (i in markers)
	{
		var m=new google.maps.Marker(
		{
		  position: new google.maps.LatLng(markers[i]['lat'], markers[i]['long']),
		  map: map
		});
		
		 google.maps.event.addListener(m, 'click', (function(m, i) {
			return function() {
			  infowindow.setContent(markers[i]['description']);
			  infowindow.open(map, m);
			}
		  })(m, i));
   }
}