<?php
global $nico_theme_name, $nico_theme, $nico_theme_positions;
$nico_theme_name = '<span style="color:#000">Moda</span>';
$nico_theme = 'moda';
$nico_theme_positions = 
array(
	'menu' => 'Menu',
	'header' => 'Header',
	'footer_top' => 'Footer top',
	'content_top' => 'Content top',
	'content_bottom' => 'Content bottom',
	'column_left' => 'Column left',
	'column_right' => 'Column right',
	'product_tabs' => 'Product tabs (product page)'
);
