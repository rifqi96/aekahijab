<?php

$custom_tabs = 
array(
	'General Options' => 
	array('General', 'Layout', 'Social band', 'Footer', 'Translations', 'Checkout', 'Custom css')/*,
	'Footer' => 
	array('Contact details', 'Follow us', 'Custom columns'),*/
);



$module_config = array( 
	1 => //general options
	array(
		0 => //general
		array(
		'text_direction'=>array('select', 
				array(
					  'auto' => 'Auto (read from language config)',
					  'ltr' => 'Left to right',
					  'rtl' => 'Right to Left'),
				'Text direction'
			),

		'container_width'=>array('select', 
				array('default' => 'Default',
					  'wide' => 'Wide'),
				'Container width'
			),
		'concatenate_css'=>array('select', 
				array('0' => 'No',
					  '1' => 'Yes'),
				'Concatenate css<span class="help help-block">for faster page loading<br/><b>css folder must be php writable</b></span>'
			),

		'concatenate_js'=>array('select', 
				array('0' => 'No',
					  '1' => 'Yes'),
				'Concatenate js<span class="help help-block">for faster page loading<br/><b>js folder must be php writable</b></span>'
			),

		'fonts'=>array('select', 
				array('0' => 'Google fonts',
					  '1' => 'Built in'),
				'Fonts'
			),

		
		'google_fonts' =>array('input',null,'Google fonts <span class="help help-block">separate font name by using |</b></span>', 'Montserrat:400,600,700|Open+Sans'),
		/*'fixed_header'=>array('select', 
				array('0' => 'No',
					  '1' => 'Yes'),
				'Fixed header'
			),*/
		'position'=>array('hidden',	null, 'Position', 'hidden'),
		'layout_id'=>array('hidden',	null, 'Layout id', 'hidden'),
		),
		1 => //layout
		array(
		'category_page_products_row'=>array('select', 
					array('12' => '1',
					  '6' => '2',
					  '4' => '3',
					  '3' => '4',
					  '2' => '6',
					  '1' => '12',
					  ),
					'Category page products per row', 3
					),
		
		'product_page_image_cols'=>array('select', 
					array(
						  '1' => '1',
						  '2' => '2',
						  '3' => '3',
						  '4' => '4',
						  '5' => '5',
						  '6' => '6',
						  '7' => '7',
						  '8' => '8',
						  '9' => '9',
						  '10' => '10',
						  '11' => '11',
						  ),
					'Product page image column size', 5
					),

		'product_page_image_gallery'=>array('select', 
					array('zoom' => 'Cloud zoom',
					  'popup' => 'Popup',
					  ),
					'Product page image gallery', 3
					),

		'show_main_image_thumb'=>array('select', 
					array('0' => 'Yes',
					  '1' => 'No',
					  ),
					'Show main image thumb <span class="help help-block">To allow going back to main image</span>'
					),

		'sale_badge_text'=>array('input',null,'Sale badge text <span class="help help-block">leave empty to display percentage discount</span>', ''),
		),		
		2 => //social band
		array(
			'about_text'=>array('multilanguage_html',null,'About text', ''),
			'twitter_profile_id'=>array('input',null,'Twitter id (<a target="_blank" href="http://themeforest.net/item/nico-fullscreen-responsive-opencart-theme/3554648/faqs/16899">how to get</a>)', ''),
			'twitter_count'=>array('input',null,'Tweet count', '2'),
			'fb_profile_id'=>array('input',null,'Facebook id (<a target="_blank" href="http://findmyfacebookid.com/">get id</a>)', ''),
			'fb_width'=>array('input',null,'Facebook like box width', '300'),
			'twitter_url'=>array('input',null,'Twitter url', ''),
		),		
		3 => //Footer
		array(
			'twitter_url'=>array('input',null,'Twitter url', ''),
			'pinterest_url'=>array('input',null,'Pinteres url', ''),
			'github_url'=>array('input',null,'Github url', ''),
			'rss_url'=>array('input',null,'Rss url', ''),
			'google_url'=>array('input',null,'Google url', ''),
			'instagram_url'=>array('input',null,'Instagram url', ''),
			'linkedin_url'=>array('input',null,'linkedin url', ''),
			'tumblr_url'=>array('input',null,'tumblr url', ''),
		'show_information_column'=>array('select', 
				array('0' => 'Yes',
					  '1' => 'No'),
				'Show information column'
			),

		'show_extra_column'=>array('select', 
				array('0' => 'Yes',
					  '1' => 'No'),
				'Show extra column'
			),


		'show_extra_voucher'=>array('select', 
				array('0' => 'Yes',
					  '1' => 'No'),
				'Show voucher link <span class="help help-block">Extra column</span>'
			),

		'show_extra_affiliate'=>array('select', 
				array('0' => 'Yes',
					  '1' => 'No'),
				'Show affiliate link <span class="help help-block">Extra column</span>'
			),

		'show_service_column'=>array('select', 
				array('0' => 'Yes',
					  '1' => 'No'),
				'Show service column'
			),

			
		'show_account_column'=>array('select', 
				array('0' => 'Yes',
					  '1' => 'No'),
				'Show account column'
			),

		'show_powered'=>array('select', 
				array('0' => 'Yes',
					  '1' => 'No'),
				'Show powered by opencart'
			),
		),		
		4 => //translations
		array(
			'buynow_text'=>array('multilanguage',null,'Buy now text', ''),
			'newsletter_text'=>array('multilanguage',null,'Newsletter text', ''),
			'subscribe_text'=>array('multilanguage',null,'Subscribe text', ''),
			'loading_text'=>array('multilanguage',null,'Loading text', ''),
		),		
		5 => //checkout
		array(
			'checkout'=>array('select', 
				array('0' => 'Opencart default',
					  '1' => 'One page quick checkout'),
				'Checkout'
			),

			'checkout_hide_fax'=>array('select', 
				array('0' => 'Show',
					  '1' => 'Hide'),
				'Fax input <span class="help help-block">one page checkout</span>'
			),

			'checkout_hide_tax_id'=>array('select', 
				array('0' => 'Show',
					  '1' => 'Hide'),
				'Tax id input <span class="help help-block">one page checkout</span>'
			),

			'checkout_hide_company_id'=>array('select', 
				array('0' => 'Show',
					  '1' => 'Hide'),
				'Company id input <span class="help help-block">one page checkout</span>'
			),

			'checkout_hide_company'=>array('select', 
				array('0' => 'Show',
					  '1' => 'Hide'),
				'Company input <span class="help help-block">one page checkout</span>'
			),

			'checkout_payment_auto_cofirm'=>array('select', 
				array('0' => 'Yes',
					  '1' => 'No'),
				'Automatically confirm order <span class="help help-block">avoid second click for confirm order after checkout button is pressed, useful if the payment method module does not require additional input</span>'
			),

		),		
		6 => //custom css
		array(
			'custom_css'=>array('textarea',null,'Custom css', ''),
		),		
	)/*,
	2 => //Footer
	array(
		0 => //contact
		array(
		'layout_id'=>array('select', $layouts, 'Layout footer 1'),
		'position'=>array('hidden',	null, 'Position', 'hidden'),

		'status'=>array('select', 
						array('1' => 'Enabled',
							  '0' => 'Disabled'),
						'Status'
					),
		'sort_order'=>array('input',null,'Sort Order', 1),
		'image_width'=>array('input',null,'Image width', 1027),
		'image_height'=>array('input',null,'Image Height', 768),

		'effect'=>array('select', 
						array(
							  'none' => 'None',
							  'grayscale' => 'Grayscale',
							  'blur' => 'Blur'),
						'Image effect'
					),

		'fade'=>array('input',null,'Fade', 750),
		'duration'=>array('input',null,'Duration', 4000),
		),
		1 => //follow us
		array(
		'layout_id'=>array('select', $layouts, 'Layout footer 2'),
		'position'=>array('hidden',	null, 'Position', 'hidden'),

		'status'=>array('select', 
						array('1' => 'Enabled',
							  '0' => 'Disabled'),
						'Status'
					),
		'sort_order'=>array('input',null,'Sort Order', 1),
		'image_width'=>array('input',null,'Image width', 1027),
		'image_height'=>array('input',null,'Image Height', 768),

		'effect'=>array('select', 
						array(
							  'none' => 'None',
							  'grayscale' => 'Grayscale',
							  'blur' => 'Blur'),
						'Image effect'
					),

		'fade'=>array('input',null,'Fade', 750),
		'duration'=>array('input',null,'Duration', 4000),
		),		
	),*/	
);
