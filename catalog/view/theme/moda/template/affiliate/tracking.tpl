<?php 
$nico_include_path = __DIR__. '/../../';
//vqmod changes paths and the above path fails, check other paths
if (!file_exists($nico_include_path . 'nico_theme_editor/common.inc')) 
{
	if (isset($_SERVER['SCRIPT_FILENAME']))
	{
		$nico_include_path = dirname($_SERVER['SCRIPT_FILENAME']) . '/catalog/view/theme/moda/';
	}
	
	if (!file_exists($nico_include_path . '/nico_theme_editor/common.inc') && isset($_SERVER['DOCUMENT_ROOT']))
	{
		$nico_include_path = $_SERVER['DOCUMENT_ROOT'] . '/catalog/view/theme/moda/';
	}	
	
	if (!file_exists($nico_include_path . '/nico_theme_editor/common.inc')) $nico_include_path = dirname(__FILE__) . '/../../';
}

if (file_exists($nico_include_path . 'nico_theme_editor/common.inc')) require_once($nico_include_path . 'nico_theme_editor/common.inc');
global $_config;
echo $header; 
?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
    <div class="row">
	<div class="col-md-12">
		<div class="cat_header">
			<h2><?php echo $heading_title; ?></h2>
		</div>
	    </div>
    </div>
      <p><?php echo $text_description; ?></p>
      <form class="form-horizontal">
        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-code"><?php if (isset($entry_code)) echo $entry_code;else echo $text_code; ?></label>
          <div class="col-sm-10">
            <textarea cols="40" rows="5" placeholder="<?php if (isset($entry_code)) echo $entry_code; ?>" id="input-code" class="form-control"><?php echo $code; ?></textarea>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-generator"><?php if (isset($entry_generator)) echo $entry_generator;else echo $text_generator; ?></label>
          <div class="col-sm-10">
            <input type="text" name="product" value="" placeholder="" id="input-generator" class="form-control" />
            <span class="help-block"><?php if (isset($help_generator)) echo $help_generator; ?></span> </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-link"><?php if (isset($entry_link)) echo $entry_link;else echo $text_link; ?></label>
          <div class="col-sm-10">
            <textarea name="link" cols="40" rows="5" placeholder="<?php if (isset($entry_link)) echo $entry_link; ?>" id="input-link" class="form-control"></textarea>
          </div>
        </div>
      </form>
      <div class="buttons clearfix">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/moda/css/ui-lightness/jquery-ui-1.10.4.css">
<script src="catalog/view/theme/moda/js/jquery-ui-1.10.4.min.js"></script>

<script type="text/javascript"><!--
$('input[name=\'product\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=affiliate/tracking/autocomplete&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',			
			success: function(json) {
				response($.map(json, function(item) 
				{
					return {
						value: item['name'],
						url: item['link'],
					}
				}));
			}
		});
		return false;
	},
	'select': function(e,item) {
		$('input[name=\'product\']').val(item.item['value']);
		$('textarea[name=\'link\']').val(item.item['url']);	
	}	
});
//--></script> 
<?php echo $footer; ?> 
