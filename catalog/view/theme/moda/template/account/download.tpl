<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h2><?php echo $heading_title; ?></h2>
	  <?php foreach ($downloads as $download) { ?>
	  <div class="download-list">
		<div class="download-id"><b><?php echo $text_order; ?></b> <?php echo $download['order_id']; ?></div>
		<div class="download-status"><b><?php echo $text_size; ?></b> <?php echo $download['size']; ?></div>
		<div class="download-content">
		  <div><b><?php echo $text_name; ?></b> <?php echo $download['name']; ?><br />
			<b><?php echo $text_date_added; ?></b> <?php echo $download['date_added']; ?></div>
		  <div><b><?php echo $text_remaining; ?></b> <?php echo $download['remaining']; ?></div>
		  <div class="download-info">
			<?php if ($download['remaining'] > 0) { ?>
			<a href="<?php echo $download['href']; ?>"><img src="catalog/view/theme/default/image/download.png" alt="<?php echo $button_download; ?>" title="<?php echo $button_download; ?>" /></a>
			<?php } ?>
		  </div>
		</div>
	  </div>
	  <?php } ?>
      <div class="buttons clearfix">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>