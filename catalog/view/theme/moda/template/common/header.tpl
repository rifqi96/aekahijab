<?php 
global $_config, $opencart_version, $is_css_file_writable,$_nico_head_script, $_nico_head_style, $_nico_head_js, $_nico_head_css;
if (!$opencart_version) $opencart_version = (int)str_replace('.','',VERSION);
$nico_include_path = __DIR__. '/../../';
//vqmod changes paths and the above path fails, check other paths
if (!file_exists($nico_include_path . 'nico_theme_editor/common.inc')) 
{
	if (isset($_SERVER['SCRIPT_FILENAME']))
	{
		$nico_include_path = dirname($_SERVER['SCRIPT_FILENAME']) . '/catalog/view/theme/moda/';
	}
	
	if (!file_exists($nico_include_path . '/nico_theme_editor/common.inc') && isset($_SERVER['DOCUMENT_ROOT']))
	{
		$nico_include_path = $_SERVER['DOCUMENT_ROOT'] . '/catalog/view/theme/moda/';
	}	
	
	if (!file_exists($nico_include_path . '/nico_theme_editor/common.inc')) $nico_include_path = dirname(__FILE__) . '/../../';
}
if (file_exists($nico_include_path . 'nico_theme_editor/common.inc')) require_once($nico_include_path . 'nico_theme_editor/common.inc');
if (!isset($_POST['ajax']) && strpos($_SERVER['REQUEST_URI'],'ajax=true') == false) {?><!doctype html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
    <head>
	    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<base href="<?php echo $base; ?>" />
		<?php if ($description) { ?>
		<meta name="description" content="<?php echo $description; ?>" />
		<?php } ?>
		<?php if ($keywords) { ?>
		<meta name="keywords" content="<?php echo $keywords; ?>" />
		<?php } ?>
		<?php if ($icon) { ?>
		<link href="<?php echo $icon; ?>" rel="icon" />
		<?php } ?>
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="apple-touch-icon" href="/apple-touch-icon.png">

		<!--script type="text/javascript" src="https://getfirebug.com/firebug-lite-debug.js"></script-->	
		<!--[if lt IE 9]>
 		<script type="text/javascript" src="catalog/view/theme/moda/js/respond.js"></script>
 		<style>
 		img.img-responsive {max-width:none;width:auto;}
 		</style>
		<![endif]-->
		<!--[if gt IE 8]>
 		<style>
 		img.img-responsive {max-width:none;width:auto;}
 		</style>
		<![endif]-->
		
	<?php 

		$_nico_head_script ['common'][] = 'catalog/view/theme/moda/js/jquery-2.1.1.min.js';
		$_nico_head_script ['common'][] = 'catalog/view/theme/moda/js/bootstrap.min.js';
		$_nico_head_script ['common'][] = 'catalog/view/theme/moda/js/bootstrap-select.min.js';
		$_nico_head_script ['common'][] = 'catalog/view/theme/moda/js/jquery.autocomplete.js';
		$_nico_head_script ['common'][] = 'catalog/view/theme/moda/js/jquery.colorbox-min.js';
		$_nico_head_script ['common'][] = 'catalog/view/theme/moda/js/cloud-zoom.1.0.3.js';
		$_nico_head_script ['common'][] = 'catalog/view/theme/moda/js/jquery.flexslider.js';
		$_nico_head_script ['common'][] = 'catalog/view/theme/moda/js/moment.js';
		$_nico_head_script ['common'][] = 'catalog/view/theme/moda/js/bootstrap-datetimepicker.min.js';
		$_nico_head_script ['common'][] = 'catalog/view/theme/moda/js/main.js';
		?>

		<?php if (nico_get_config('container_width') == 'wide') 
		$_nico_head_style ['common'][] = 'catalog/view/theme/moda/css/bootstrap_wide.css';else
		$_nico_head_style ['common'][] = 'catalog/view/theme/moda/css/bootstrap.css';
		
		if ($direction == 'rtl' || nico_get_config('text_direction') == 'rtl') 
		$_nico_head_style ['common'][] = 'catalog/view/theme/moda/css/bootstrap-rtl.css';

		if (nico_get_config('fonts') == 1) $_nico_head_style ['common'][] = 'catalog/view/theme/moda/css/builtinfonts.css'; else 
		{?><link href='//fonts.googleapis.com/css?family=<?php if ($google_fonts = nico_get_config('google_fonts')) echo  str_replace('|','%7C',$google_fonts);else echo str_replace('|','%7C','Montserrat:400,600,700|Open+Sans|Lato');?>' rel='stylesheet' type='text/css'/><?php }  

		$_nico_head_style ['common'][] = 'catalog/view/theme/moda/css/bootstrap-select.min.css';
		$_nico_head_style ['common'][] = 'catalog/view/theme/moda/css/style.css';
		$_nico_head_style ['common'][] = 'catalog/view/theme/moda/css/colorbox.css';
		$_nico_head_style ['common'][] = 'catalog/view/theme/moda/css/font-awesome.min.css';
		$_nico_head_style ['common'][] = 'catalog/view/theme/moda/js/cloud-zoom.css';
		$_nico_head_style ['common'][] = 'catalog/view/theme/moda/css/flexslider.css';
		$_nico_head_style ['common'][] = 'catalog/view/theme/moda/css/bootstrap-datetimepicker.min.css';


		$is_css_file_writable = is_readable($nico_include_path . 'css/editor_settings.css') && is_writable($nico_include_path . 'css/editor_settings.css');
		if ($is_css_file_writable)
		$_nico_head_style ['common'][] = 'catalog/view/theme/moda/css/editor_settings.css';
		else
		$_nico_css = $_config->get('nicoeditorsettings_css');
		?>

		<?php 
		$concatenate_js = (nico_get_config('concatenate_js') == '1'); 
		
		//write js files
		if ($_nico_head_script) 
		{
			ksort($_nico_head_script);
			foreach($_nico_head_script as $group => $nico_scripts)
			{
				$nico_scripts = array_unique($nico_scripts);
				if ($concatenate_js)
				{
					$group_js = 'catalog/view/theme/moda/js/_nico_'. $group . '.js';

					if (
						(is_writable($group_js) && $group_mtime = filemtime($group_js))
						|| (@touch($group_js) && $group_mtime = -1)) 
					{

						$max_time = 0;
						$group_mtime = 0;
						foreach($nico_scripts as $script)
						{
							$time = filemtime($script);
							if ($time > $max_time) $max_time = $time;
						}

						if ($group_mtime < $max_time)
						{
							$js_contents = '';
							foreach($nico_scripts as $script)
							{
								$js_contents .= "/*$script*/\n\n" . file_get_contents($script) . ";\n";
							}
							file_put_contents($group_js, $js_contents);
						} 
							?>
							<script src="catalog/view/theme/moda/js/_nico_<?php echo $group;?>.js?t=<?php echo $max_time;?>"></script>
					<?php			
					} else $concatenate_js = false;
				} 
				if (!$concatenate_js) foreach($nico_scripts as $script)
				{
				?>
					<script src="<?php echo $script;?>"></script>
				<?php			
				}

			}
		}
		$concatenate_css = (nico_get_config('concatenate_css') == '1'); 
		
		if ($_nico_head_style) 
		{
			ksort($_nico_head_style);
			foreach($_nico_head_style as $group => $nico_styles)
			{
				$nico_styles = array_unique($nico_styles);
				if ($concatenate_css)
				{
					$group_css = 'catalog/view/theme/moda/css/_nico_'. $group . '.css';

					if (
						(is_writable($group_css) && $group_mtime = filemtime($group_css))
						|| (@touch($group_css) && $group_mtime = -1)) 
					{
					$max_time = 0;
					$group_mtime = 0;
					foreach($nico_styles as $style)
					{
						$time = filemtime($style);
						if ($time > $max_time) $max_time = $time;
					}
					
						if ($group_mtime < $max_time)
					{
						$css_contents = '';
						foreach($nico_styles as $style)
						{
							$css_contents .= "/*$style*/\n\n" .file_get_contents($style) . "\n\n";
						}
						file_put_contents('catalog/view/theme/moda/css/_nico_'. $group . '.css', $css_contents);
						} 
						?>
						<link href='catalog/view/theme/moda/css/_nico_<?php echo $group;?>.css?t=<?php echo $max_time;?>' rel='stylesheet' type='text/css'/><?php
					} else $concatenate_css = false;
				} 
				if (!$concatenate_css)  foreach($nico_styles as $style)
				{
				?>
					<link href='<?php echo $style;?>' rel='stylesheet' type='text/css'/>
				<?php			
				}

			}
		}
		?>

				
		<script>
		jQuery(window).ready(function() {
		<?php 
		if ($_nico_head_js) {
		//$_nico_head_js = array_unique($_nico_head_js);
		foreach($_nico_head_js as $js)
		{
			echo $js . "\n";
		}}
		?>});
		</script>

		<style>
		<?php 
		if ($_nico_head_css) {
		//$_nico_head_css = array_unique($_nico_head_css);
		foreach($_nico_head_css as $css)
		{
			echo $css;
		}}
		if (!$is_css_file_writable) echo $_nico_css;
		$custom_css = nico_get_config('custom_css');
		if ($custom_css) echo $custom_css;?>
		</style>

		<?php foreach ($links as $link) { ?>
		<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
		<?php } ?>

		<?php foreach ($styles as $style) if (strpos($style['href'], 'colorbox.css') == false && strpos($style['href'], 'blog_custom.css') == false && strpos($style['href'], 'pavblog.css') == false) {?>
		<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
		<?php } ?>

		<?php foreach ($scripts as $script) { if (strpos($script, 'nivo.slider') == false && strpos($script, 'colorbox') == false  && strpos($script, 'tabs.js') == false) {?>
		<script type="text/javascript" src="<?php echo $script; ?>"></script>
		<?php } } ?>

		<?php echo $google_analytics; ?>
	    <title><?php echo $title; ?></title>
	</head>
<body>
	
<div id="top">
  <div class="container">
	<div id="top-links" class="pull-left">
	
	<ul class="list-inline">
        <?php if (isset($text_welcome)) {?>
		<li>
			<?php if (!$logged && isset($text_welcome)) { ?>
			<?php echo $text_welcome; ?>
			<?php } else if (isset($text_logged) && $text_logged != 'text_logged') { ?>
			<?php echo $text_logged; ?>
			<?php } ?>
		</li>
		<?php } else { ?>
        <li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md">
			<?php 
			if ($logged) 
			{
				$customer = $this->registry->get('customer');
				$email = $customer->getEmail();
				echo $email;  
			} else echo $text_account; 
			?> 
			</span> <i class="fa fa-caret-down"></i></a>
          <ul class="dropdown-menu">
            <?php if ($logged) { ?>
            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
            <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
            <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
            <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
            <?php } else { ?>
            <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
            <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
            <?php } ?>
          </ul>
        </li>
        <?php } ?>
        <?php if (false && $logged && isset($logout)) { ?><li><a href="<?php echo $logout; ?>">( <?php echo $text_logout; ?> )</a></li><?php }?>
        <li><a href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $text_wishlist; ?>"><i class="fa fa-heart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_wishlist; ?></span></a></li>
        <li><a href="<?php echo $shopping_cart; ?>" title="<?php echo $text_shopping_cart; ?>"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_shopping_cart; ?></span></a></li>
        <li><a href="<?php echo $checkout; ?>" title="<?php echo $text_checkout; ?>"><i class="fa fa-share"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_checkout; ?></span></a></li>
        <?php if (isset($telephone)) { ?><li class="phone-number"><a href="<?php echo $contact; ?>"><i class="fa fa-phone"></i></a> <span class="hidden-xs hidden-sm hidden-md"><?php echo $telephone; ?></span></li><?php } ?>
      </ul>
	</div>
	<!-- div id="welcome" class="pull-right">
	<?php if (!$logged && isset($text_welcome)) { ?>
	<?php echo $text_welcome; ?>
	<?php } else if (isset($text_logged) && $text_logged != 'text_logged') { ?>
	<?php echo $text_logged; ?>
	<?php } ?>
	</div -->
	<div class="pull-right"><?php echo $currency; ?></div>
	<div class="pull-right"><?php echo $language; ?></div>
  </div>
</div>

	<div class="header">
	<?php $_header = nico_get_modules('header');if($_header) foreach ($_header as $module) echo $module;?>
	<?php if ($header_type = nico_get_config('header_type')) include($nico_include_path . '/template/common/header' . $header_type . '.tpl'); else include($nico_include_path . '/template/common/header1.tpl');?>
	</div>
		

  
<?php if (isset($error) && $error) { ?>
    
    <div class="warning"><?php echo $error ?><img src="catalog/view/theme/moda/img/close.png" alt="" class="close" /></div>
    
<?php } ?>
<div id="notification"></div>

<div class="page-container">
<?php }
