<?php 
$nico_include_path = __DIR__. '/../../';
//vqmod changes paths and the above path fails, check other paths
if (!file_exists($nico_include_path . 'nico_theme_editor/common.inc')) 
{
	if (isset($_SERVER['SCRIPT_FILENAME']))
	{
		$nico_include_path = dirname($_SERVER['SCRIPT_FILENAME']) . '/catalog/view/theme/moda/';
	}
	
	if (!file_exists($nico_include_path . '/nico_theme_editor/common.inc') && isset($_SERVER['DOCUMENT_ROOT']))
	{
		$nico_include_path = $_SERVER['DOCUMENT_ROOT'] . '/catalog/view/theme/moda/';
	}	
	
	if (!file_exists($nico_include_path . '/nico_theme_editor/common.inc')) $nico_include_path = dirname(__FILE__) . '/../../';
}

if (file_exists($nico_include_path . 'nico_theme_editor/common.inc')) require_once($nico_include_path . 'nico_theme_editor/common.inc');

global $_config;
if (strpos($_SERVER['REQUEST_URI'],'ajax=true') == false) {?>

<?php  /*if (isset($_nico_settings['options']['#social_band']['display']) && $_nico_settings['options']['#social_band']['display'] == 'block') */{?>
<div id="social_band">
	<div class="container">
	<div class="row">
	<?php 
		$social_col = 0;
		$about_text = nico_get_config('about_text');
		$about_text = isset($about_text[$_config->get('config_language')])?htmlspecialchars_decode($about_text[$_config->get('config_language')]):$about_text['en'];
		$fb_profile_id = nico_get_config('fb_profile_id');
		$twitter_profile_id = nico_get_config('twitter_profile_id');
		if (!empty($about_text)) $social_col++;
		if (!empty($fb_profile_id)) $social_col++;
		if (!empty($twitter_profile_id)) $social_col++;
		if ($social_col > 0) $scols = 12 / $social_col;
	?>		
	<?php if (!empty($about_text)) {?>
	<div id="social_about" class="col-md-<?php echo $scols;?>">
		<h3>About</h3>
		<div>
			<?php  if (isset($about_text) && !empty($about_text)) echo str_replace("\n",'<br/>',$about_text); else echo 'You can change this text from the Control Panel Social band > About text<br/><br/>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.';?>
		</div>
	</div>
	<?php }?>
	<?php if (!empty($twitter_profile_id)) {?>
	<div id="social_twitter" class="col-md-<?php echo $scols;?>">
		<h3>Twitter</h3>
		<div>
			<ul id="twitter_update_list"><li>Twitter feed loading</li></ul>			
			<script type="text/javascript" src="catalog/view/theme/moda/js/twitterFetcher_v9_min.js"></script>
			<script>twitterFetcher.fetch('<?php $twitter_count = nico_get_config('twitter_count'); if (isset($twitter_profile_id) && !empty($twitter_profile_id)) echo $twitter_profile_id; else echo '256524641194098690';?>', 'twitter_update_list', <?php if (isset($_nico_settings['settings']['twitter_count']) && !empty($twitter_count)) echo $twitter_count; else echo '2';?>, true, true, false);</script> 
			<!--
			<script type="text/javascript" src="https://api.twitter.com/1/statuses/user_timeline.json?screen_name=<?php if (isset($_nico_settings['settings']['twitter_profile_id']) && !empty($_nico_settings['settings']['twitter_profile_id'])) echo $_nico_settings['settings']['twitter_profile_id']; else echo 'NicoleThemes';?>&amp;callback=twitterCallback2&amp;count=<?php if (isset($_nico_settings['settings']['twitter_count']) && !empty($_nico_settings['settings']['twitter_count'])) echo $_nico_settings['settings']['twitter_count']; else echo '3';?>"></script>			
			<a href="http://twitter.com/#!/<?php if (isset($twitter_profile_id) && !empty($twitter_profile_id)) echo $twitter_profile_id; else echo 'NicoleThemes';?>" id="twitter_follow">Follow us on twitter</a>
			-->
		</div>
	</div>
	<?php }?>
	<?php if (!empty($fb_profile_id)) {?>
	<div id="social_facebook" class="col-md-<?php echo $scols;?>">
		<h3>Facebook</h3>
		<div>
		<div class="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "https://connect.facebook.net/en_US/all.js#xfbml=1";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
		<div class="fb-like-box" data-href="https://www.facebook.com/<?php $fb_width = nico_get_config('fb_width');if (isset($fb_profile_id) && !empty($fb_profile_id)) echo $fb_profile_id; else echo '201498429982413';?>" data-width="<?php if (isset($fb_width) && !empty($fb_width)) echo $fb_width; else echo '300';?>" data-color-scheme="light"  data-show-faces="true" data-stream="false" data-header="false" data-border-color="#ffffff" data-show-border="false"></div>
		</div>		
	</div>
	<?php }?>
</div>
</div>
</div>
<?php }?>


</div>

<?php 
$footer_top = nico_get_modules('footer_top');
$show_information_column = nico_get_config('show_information_column');
$show_extra_column  = nico_get_config('show_extra_column');
$show_extra_voucher = nico_get_config('show_extra_voucher');
$show_extra_affiliate = nico_get_config('show_extra_affiliate');
$show_service_column = nico_get_config('show_service_column');
$show_account_column = nico_get_config('show_account_column');
$show_powered = nico_get_config('show_powered');
$footer_top = nico_get_modules('footer_top');if($footer_top) foreach ($footer_top as $module) echo $module;?>

<div class="footer black">
	<div class="container">
		<!-- div class="arrow"><b class="caret"></b></div -->
		<div class="row">
			
		 <?php if ($informations && $show_information_column != 1) { ?>
		  <div class="col-md-2 col-sm-3">
			<div>
			<h3><?php echo $text_information; ?></h3>
			<ul>
			  <?php foreach ($informations as $information) { ?>
			  <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
			  <?php } ?>
			</ul>
			</div>
		  </div>
		  <?php } ?>
		  
		  <?php if ($show_information_column != 1) {?>
		  <div class="col-md-2 col-sm-3">
			<div>
			<h3><?php echo $text_extra; ?></h3>
			<ul>
			  <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
			  <?php if ($show_extra_voucher != 1) {?><li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li><?php } ?>
			  <?php if ($show_extra_affiliate != 1) {?><li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li><?php } ?>
			  <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
			</ul>
			</div>
		  </div>
		  <?php } ?>
		  
		  <?php if ($show_service_column != 1) {?>
		  <div class="col-md-2 col-sm-3">
			<div>
			<h3><?php echo $text_service; ?></h3>
			<ul>
			  <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
			  <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
			  <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
			</ul>
			</div>
		  </div>
		 <?php } ?>

		  <?php if ($show_account_column != 1) {?>
		  <div class="col-md-2 col-sm-3">
			<div>
			<h3><?php echo $text_account; ?></h3>
			<ul>
			  <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
			  <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
			  <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
			  <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
			</ul>
			</div>
		  </div>
		  <?php } ?>

		  <div class="col-md-4 social">
				<?php if ($show_powered != 1) {?><div class="copy"><?php echo $powered; ?></div><?php } ?>
				<ul class="social-network">
					<?php $fb_profile_id = nico_get_config('fb_profile_id');if (isset($fb_profile_id) && !empty($fb_profile_id))  { ?>
					<li><a href="http://www.facebook.com/<?php echo $fb_profile_id;?>"><i class="fa fa-facebook"></i></a></li>
					<?php } ?>
					<?php $pinterest_url = nico_get_config('pinterest_url');if (isset($pinterest_url) && !empty($pinterest_url))  { ?>
					<li><a href="<?php echo $pinterest_url;?>"><i class="fa fa-pinterest"></i></a></li>
					<?php } ?>
					<?php $instagram_url = nico_get_config('instagram_url');if (isset($instagram_url) && !empty($instagram_url))  { ?>
					<li><a href="<?php echo $instagram_url;?>"><i class="fa fa-instagram"></i></a></li>
					<?php } ?>
					<?php $github_url = nico_get_config('github_url');if (isset($github_url) && !empty($github_url))  { ?>
					<li><a href="<?php echo $github_url;?>"><i class="fa fa-github"></i></a></li>
					<?php } ?>
					<?php $twitter_url = nico_get_config('twitter_url');if (isset($twitter_url) && !empty($twitter_url))  { ?>
					<li><a href="<?php echo $twitter_url;?>"><i class="fa fa-twitter"></i></a></li>
					<?php } ?>
					<?php $rss_url = nico_get_config('rss_url');if (isset($rss_url) && !empty($rss_url))  { ?>
					<li><a href="<?php echo $rss_url;?>"><i class="fa fa-rss"></i></a></li>
					<?php } ?>
					<?php $linkedin_url = nico_get_config('linkedin_url');if (isset($linkedin_url) && !empty($linkedin_url))  { ?>
					<li><a href="<?php echo $linkedin_url;?>"><i class="fa fa-linkedin"></i></a></li>
					<?php } ?>
					<?php $google_url = nico_get_config('google_url');if (isset($google_url) && !empty($google_url))  { ?>
					<li><a href="<?php echo $google_url;?>"><i class="fa fa-google-plus"></i></a></li>
					<?php } ?>
					<?php $tumblr_url = nico_get_config('tumblr_url');if (isset($tumblr_url) && !empty($tumblr_url))  { ?>
					<li><a href="<?php echo $tumblr_url;?>"><i class="fa fa-tumblr"></i></a></li>
					<?php } ?>
				</ul>
			</div>

   </div>	
</div>
</div>

<?php 
	$footer_bottom = nico_get_modules('footer_bottom');if($footer_bottom) foreach ($footer_bottom as $module) echo $module;
	include($nico_include_path. 'nico_theme_editor/editor.inc');
?>

</body>
</html><?php }
