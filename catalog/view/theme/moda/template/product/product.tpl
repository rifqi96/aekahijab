<?php 
$nico_include_path = __DIR__. '/../../';
//vqmod changes paths and the above path fails, check other paths
if (!file_exists($nico_include_path . 'nico_theme_editor/common.inc')) 
{
	if (isset($_SERVER['SCRIPT_FILENAME']))
	{
		$nico_include_path = dirname($_SERVER['SCRIPT_FILENAME']) . '/catalog/view/theme/moda/';
	}
	
	if (!file_exists($nico_include_path . '/nico_theme_editor/common.inc') && isset($_SERVER['DOCUMENT_ROOT']))
	{
		$nico_include_path = $_SERVER['DOCUMENT_ROOT'] . '/catalog/view/theme/moda/';
	}	
	
	if (!file_exists($nico_include_path . '/nico_theme_editor/common.inc')) $nico_include_path = dirname(__FILE__) . '/../../';
}

if (file_exists($nico_include_path . 'nico_theme_editor/common.inc')) require_once($nico_include_path . 'nico_theme_editor/common.inc');
require($nico_include_path . 'nico_theme_editor/nico_config.inc');

global $_config, $opencart_version;

$_head_append = '<meta property="og:image" content="'. $popup .'" />';
if ($opencart_version < 2000) $_head_append .= '<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/magnific/magnific-popup.css" media="screen" /><script type="text/javascript" src="catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js"></script>';

echo str_replace('</head>', $_head_append . '</head>', $header);//add facebook image preview
?>
<div class="container"  vocab="http://rdf.data-vocabulary.org/#">
    <div>
	<ul class="breadcrumb" typeof="v:Breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<li typeof="v:Breadcrumb">
	    <a rel="v:url" property="v:title" href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a> 
	    <span class="divider"></span>
	</li>
	<?php } ?>
	</ul>
   </div>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <div class="row product-info" itemscope itemtype="http://data-vocabulary.org/Product">
        <?php 
			$show_main_image_thumb = nico_get_config('show_main_image_thumb');
			$product_page_image_cols = nico_get_config('product_page_image_cols');
			$product_page_image_cols = empty($product_page_image_cols)?7:$product_page_image_cols;
			$class = 'col-sm-' . $product_page_image_cols; 
		?>
        <div class="<?php echo $class; ?>">
          <?php if ($thumb || $images) { ?>
            <?php if ($thumb) { ?>
            <div class="image"><a class="cloud-zoom" rel="adjustX:0, adjustY:0" id='zoom1' href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"><img src="<?php echo $popup; ?>" itemprop="image" title="" alt="" /></a></div>
            <?php } ?>
            <?php if ($images) { ?>
	       <div class="image-additional">
				<ul class="slides">    
				<?php foreach ($images as $image) { ?>
				<li>
					<a class="cloud-zoom-gallery item"  href="<?php echo $image['popup']; ?>" rel="useZoom:zoom1, smallImage:<?php echo $image['popup']; ?>" title=""> <img src="<?php echo $image['thumb']; ?>" title="" alt="" /></a>
				</li>
				<?php } if ($show_main_image_thumb != 1) {?>
				<li>
					<a class="cloud-zoom-gallery item"  href="<?php echo $image['popup']; ?>" rel="useZoom:zoom1, smallImage:<?php echo $popup; ?>" title=""> <img src="<?php echo $popup; ?>" title="" alt="" /></a>
				</li>
				<?php }?>
				</ul>
			</div>
            <?php } ?>
          <?php } ?>
	</div>
        <?php 
			$class = 'col-sm-' . (12 - $product_page_image_cols); 
		?>
        <div class="<?php echo $class; ?>" id="product">
          <h1  itemprop="name"><?php echo $heading_title; ?></h1>
          <ul class="list-unstyled">
            <?php if ($manufacturer) { ?>
            <li><?php echo $text_manufacturer; ?> <a href="<?php echo $manufacturers; ?>" itemprop="brand"><?php echo $manufacturer; ?></a></li>
            <?php } ?>
            <li><?php echo $text_model; ?> <?php echo $model; ?></li>
            <?php if ($reward) { ?>
            <li><?php echo $text_reward; ?> <?php echo $reward; ?></li>
            <?php } ?>
          </ul>
          <?php if ($price) { ?>
          <ul class="list-unstyled" itemprop="offerDetails" itemscope="" itemtype="http://data-vocabulary.org/Offer">
            <?php if (!$special) { ?>
            <li><?php echo $text_stock; ?> <?php echo $stock; ?></li>
            <li>
			  <span itemprop="currency" class="hide"><?php echo $_config->get('config_currency');?></span>
              <!-- span class="currency">
  			    <?php 
				preg_match('@[^\d\.\,]+@', $price, $matches);
				echo $matches[0];?>
			  </span -->
              <h2 class="price" itemprop="price">
  			    <?php echo $price
  			    /*
				preg_match('@[\d\.\,]+@', $price, $matches);
				echo $matches[0];*/?>
			  </h2>
            </li>
            <?php } else { ?>
            <li><span style="text-decoration: line-through;"><?php echo $price; ?></span></li>
            <li>
              <h2><?php echo $special; ?></h2>
            </li>
            <?php } ?>
            <?php if ($tax) { ?>
            <li><?php echo $text_tax; ?> <?php echo $tax; ?></li>
            <?php } ?>
            <?php if ($points) { ?>
            <li><?php echo $text_points; ?> <?php echo $points; ?></li>
            <?php } ?>
            <?php if ($discounts) { ?>
            <li>
              <hr>
            </li>
            <?php foreach ($discounts as $discount) { ?>
            <li><?php if ($opencart_version < 2000) echo sprintf($text_discount, $discount['quantity'], $discount['price']); else  { echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; }?></li>
            <?php } ?>
            <?php } ?>
          </ul>
          <?php } ?>
          <div>
            <?php if ($options) {?>
            <hr>
            <h3><?php echo $text_option; ?></h3>
            <?php foreach ($options as $option) { 

            $option_values = isset($option['option_value'])?$option['option_value']:$option['product_option_value'];
            $opt_value = isset($option['option_value'])?$option['option_value']:$option['value'];
			?>
            <?php if ($option['type'] == 'select') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php if (isset($option_values)) foreach ($option_values as $option_value) { ?>
                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                <?php } ?>
                </option>
                <?php } ?>
              </select>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'radio') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php if (isset($option_values)) foreach ($option_values as $option_value) { ?>
                <div class="radio">
                  <label>
                    <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                    <?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'checkbox') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php if (isset($option_values)) foreach ($option_values as $option_value) { ?>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                    <?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'image') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php if (isset($option_values)) foreach ($option_values as $option_value) { ?>
                <div class="radio">
                  <label>
                    <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                    <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'text') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $opt_value; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'textarea') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $opt_value; ?></textarea>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'file') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
              <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'date') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group date">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php if (isset($opt_value)) echo $opt_value; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'datetime') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group datetime">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $opt_value; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'time') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group time">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $opt_value; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <?php if (isset($profiles) && $profiles) { ?>
            <hr>
            <h3><?php echo $text_payment_profile ?></h3>
            <div class="form-group required">
              <select name="profile_id" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($profiles as $profile) { ?>
                <option value="<?php echo $profile['profile_id'] ?>"><?php echo $profile['name'] ?></option>
                <?php } ?>
              </select>
              <div class="help-block" id="profile-description"></div>
            </div>
            <?php } ?>

	      <?php if ($review_status) { ?>
	      <div class="rating">
			  
			  <span itemprop="review" class="hidden" itemscope itemtype="http://data-vocabulary.org/Review-aggregate">
					<span itemprop="itemreviewed"><?php echo $heading_title; ?></span>
					<span itemprop="rating"><?php echo $rating;?></span>
					<span itemprop="votes"><?php preg_match('@[\d\.\,]+@', $reviews, $matches);echo $matches[0];?></span>
			  </span>
			  
		<p>
		  <?php for ($i = 1; $i <= 5; $i++) { ?>
		  <?php if ($rating < $i) { ?>
		  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
		  <?php } else { ?>
		  <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
		  <?php } ?>
		  <?php } ?>
		  <span class="review_link">
		  <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click');   $('html, body').animate({scrollTop: $('#tab-review').offset().top}, 2000);return false;"><?php echo $reviews; ?></a> / <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $text_write; ?></a>
		  </span>
		</p>



		<hr>

	      </div>
	      <?php } ?>
	      
		<!-- AddThis Button BEGIN -->
		<div class="addthis_toolbox addthis_default_style">
		    <a class="addthis_button_facebook_like"></a> <a class="addthis_button_tweet"></a> 
		    <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a>
			<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script> 
			<br/>
			<br/>
		</div>
		<!-- AddThis Button END --> 

            <div class="form-group">
              <label class="control-label" for="input-quantity"><?php if (isset($entry_qty))echo $entry_qty; ?></label>&nbsp;
              <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="1" id="input-quantity" class="form-control" />
              <button type="button" id="button-cart" data-loading-text="<?php if (isset($text_loading)) echo $text_loading;else echo 'loading ...' ?>" class="btn btn-primary"><?php echo $button_cart; ?></button>
              
              <a href="index.php?route=checkout/nicocheckout&amp;product_id=<?php echo $product_id; ?>" id="buynow" data-loading-text="<?php if (isset($text_loading)) echo $text_loading;else echo 'loading ...' ?>" class="btn btn-primary"><?php $buynow_text = nico_get_config('buynow_text');$buynow_text = isset($buynow_text[$_config->get('config_language')])?$buynow_text[$_config->get('config_language')]:$buynow_text['en'];if (!empty($buynow_text)) echo $buynow_text; else echo 'Buy now';?></a>
              <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />

	      <div class="btn-group product_wish_compare">
		<button type="button" data-toggle="tooltip" class="btn btn-default" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product_id; ?>');"><i class="fa fa-heart"></i></button>
		<button type="button" data-toggle="tooltip" class="btn btn-default" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product_id; ?>');"><i class="fa fa-exchange"></i></button>
	      </div>
            </div>
            <?php if ($minimum > 1) { ?>
            <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
            <?php } ?>

          </div>
        <?php if (!isset($_GET['ajax']) && !isset($_POST['ajax'])) {?></div><?php }?>

	    <?php $products_tabs = nico_get_modules('product_tabs');?>
	    
	    <?php 
	    $tabs = $tab_content = '';
	    if( $products_tabs && count($products_tabs) ) { 
		    foreach ($products_tabs as $module) 
		    {
			    $start = strpos($module, '<!-- tabs start -->');
			    $end = strpos($module, '<!-- tabs end -->');
			    
			    $tabs .= substr($module, $start, $end - $start);
			    
			    $start = strpos($module, '<!-- content start -->');
			    $end = strpos($module, '<!-- content end -->');
			    $tab_content .= substr($module, $start, $end - $start);
		    }
	    } ?>


	  <div class="tabs">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a></li>
            <?php if ($attribute_groups) { ?>
            <li><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
            <?php } ?>
            <?php if ($review_status) { ?>
            <li><a href="#tab-review" data-toggle="tab"><?php echo $tab_review; ?></a></li>
            <?php } ?>
	    <?php echo $tabs;?>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-description" itemprop="description"><?php echo $description; ?></div>
            <?php if ($attribute_groups) { ?>
            <div class="tab-pane" id="tab-specification">
              <table class="table table-bordered">
                <?php foreach ($attribute_groups as $attribute_group) { ?>
                <thead>
                  <tr>
                    <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                  <tr>
                    <td><?php echo $attribute['name']; ?></td>
                    <td><?php echo $attribute['text']; ?></td>
                  </tr>
                  <?php } ?>
                </tbody>
                <?php } ?>
              </table>
            </div>
            <?php } ?>
            <?php if ($review_status) { ?>
            <div class="tab-pane" id="tab-review">
              <form class="form-horizontal">
                <div id="review"></div>
                <h2><?php echo $text_write; ?></h2>
                <div class="form-group required">
                  <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
                  <div class="col-sm-10">
                    <input type="text" name="name" value="" id="input-name" class="form-control" />
                  </div>
                </div>
                <div class="form-group required">
                  <label class="col-sm-2 control-label" for="input-review"><?php echo $entry_review; ?></label>
                  <div class="col-sm-10">
                    <textarea name="text" cols=10 rows=5 id="input-review" class="form-control"></textarea>
                    <div class="help-block"><?php echo $text_note; ?></div>
                  </div>
                </div>
                <div class="form-group required">
                  <label class="col-sm-2 control-label"><?php echo $entry_rating; ?></label>
                  <div class="col-sm-10"><?php echo $entry_bad; ?>&nbsp;
                    <input type="radio" name="rating" value="1" />
                    &nbsp;
                    <input type="radio" name="rating" value="2" />
                    &nbsp;
                    <input type="radio" name="rating" value="3" />
                    &nbsp;
                    <input type="radio" name="rating" value="4" />
                    &nbsp;
                    <input type="radio" name="rating" value="5" />
                    &nbsp;<?php echo $entry_good; ?></div>
                </div>
                <div class="form-group required">
                  <label class="col-sm-2 control-label" for="input-captcha"><?php echo $entry_captcha; ?></label>
                  <div class="col-sm-10">
                    <input type="text" name="captcha" value="" id="input-captcha" class="form-control" />
					<br/>
                    <img src="<?php if ($opencart_version >= 2000) {?>index.php?route=tool/captcha<?php } else { ?>index.php?route=product/product/captcha<?php }?>" alt="" id="captcha" /></div>
                </div>
                <div class="buttons">
                  <div class="pull-right">
                    <button type="button" id="button-review" data-loading-text="<?php if (isset($text_loading)) echo $text_loading;else echo 'loading ...' ?>" class="btn btn-primary"><?php echo $button_continue; ?></button>
                  </div>
                </div>
              </form>
            </div>
            <?php } ?>
	    <?php echo $tab_content;?>
          </div>
         </div>
      </div>
      <?php 
      if ($products) { 
      include($nico_include_path . 'template/module/nico_product.tpl');
	  ?>
      <h3><?php if (isset($text_related)) echo $text_related;else echo $tab_related;?></h3>
      <div class="row">
        <?php $i = 0; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $cols = 'col-lg-6 col-md-6 col-sm-12 col-xs-12'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $cols = 'col-lg-4 col-md-4 col-sm-6 col-xs-12'; ?>
        <?php } else { ?>
        <?php $cols = 'col-lg-3 col-md-3 col-sm-6 col-xs-12'; ?>
        <?php } ?>
        <?php foreach ($products as $product) { ?>
	<div class="<?php echo $cols;?>">
		<?php nico_product($product);?>
	</div>
        <?php if (($column_left && $column_right) && ($i % 2 == 0)) { ?>
        <div class="clearfix visible-md visible-sm"></div>
        <?php } elseif (($column_left || $column_right) && ($i % 3 == 0)) { ?>
        <div class="clearfix visible-md"></div>
        <?php } elseif ($i % 4 == 0) { ?>
        <div class="clearfix visible-md"></div>
        <?php } ?>
        <?php $i++; ?>
        <?php } ?>
      </div>
      <?php } ?>
      <?php if ($tags) { ?>
      <p><?php echo $text_tags; ?>
        <?php for ($i = 0; $i < count($tags); $i++) { ?>
        <?php if ($i < (count($tags) - 1)) { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
        <?php } else { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
        <?php } ?>
        <?php } ?>
      </p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<script><!--
$('select[name="profile_id"], input[name="quantity"]').change(function(){
	$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'profile_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#profile-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
		
			if (json['success']) {
				$('#profile-description').html(json['success']);
			}
		}
	});
});

$('#button-cart').on('click', function() {
    $.ajax({
        url: 'index.php?route=checkout/cart/add',
        type: 'post',
        data: $('#product input[type=\'text\'], #product input[type=\'date\'], #product input[type=\'datetime-local\'], #product input[type=\'time\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
        dataType: 'json',
        beforeSend: function() {
        	$('#button-cart').button('loading');
		},      
        complete: function() {
			$('#button-cart').button('reset');
        },		
        success: function(json) {
            $('.alert, .text-danger').remove();
            
            if (json['error']) {
                if (json['error']['option']) {
                    for (i in json['error']['option']) {
                        $('#input-option' + i).after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                    }
                }

				if (json['error']['profile']) {
					$('select[name=\'profile_id\']').after('<div class="text-danger">' + json['error']['profile'] + '</div>');
				}
            } 
            
            if (json['success']) {
                //$('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                $('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/moda/img/close.png" alt="" class="close" /></div>');
                $('.success').fadeIn('slow');
                    
                $('#cart-total').html(json['total']);
                
                <?php if ($opencart_version >= 2000) {?> 
                $('.cart').load('index.php?route=common/cart/info  .cart > *', function () {$(".cart .cart-info").css("visibility", "visible");});
                <?php } else { ?>
                $('.cart').load('index.php?route=module/cart .cart > *', function () {$(".cart .cart-info").css("visibility", "visible");});
                <?php } ?>

				setTimeout(function() {$('.success').fadeOut('slow');}, 5000);                
				
				//check if not popup
				if (jQuery("#cboxLoadedContent .container").length = 0)
				$('html, body').animate({ scrollTop: 0 }, 'slow');
            }   
        }
    });
});

$('#buynow').on('click', function() {
    $.ajax({
        url: 'index.php?route=checkout/cart/add',
        type: 'post',
        data: $('#product input[type=\'text\'], #product input[type=\'date\'], #product input[type=\'datetime-local\'], #product input[type=\'time\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
        dataType: 'json',
        beforeSend: function() {
        	$('#buynow').button('loading');
		},      
        complete: function() {
			$('#buynow').button('reset');
        },		
        success: function(json) {
            $('.alert, .text-danger').remove();
            
            if (json['error']) {
                if (json['error']['option']) {
                    for (i in json['error']['option']) {
                        $('#input-option' + i).after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                    }
                }

				if (json['error']['profile']) {
					$('select[name=\'profile_id\']').after('<div class="text-danger">' + json['error']['profile'] + '</div>');
				}
            } 
            
            if (json['success']) {
				
				window.location = 'index.php?route=checkout/<?php if (nico_get_config('checkout')  == '1') echo 'nicocheckout'; else echo 'checkout'; ?>';
				
                $('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    
                $('#cart-total').html(json['total']);
                
				//check if not popup
				if (jQuery("#cboxLoadedContent .container").length = 0)
				$('html, body').animate({ scrollTop: 0 }, 'slow');
            }   
        }
    });
    
    return false;
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;
	
	$('#form-upload').remove();
	
	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	$('#form-upload input[name=\'file\']').on('change', function() {
		$.ajax({
			url: <?php if ($opencart_version < 2000) {?>'index.php?route=product/product/upload'<?php } else {?>'index.php?route=tool/upload'<?php }?>,
			type: 'post',		
			dataType: 'json',
			name: 'file',
			data: new FormData($(this).parent()[0]),
			cache: false,
			contentType: false,
			processData: false,		
			beforeSend: function() {
				$(node).find('i').replaceWith('<i class="fa fa-spinner fa-spin"></i>');
				$(node).prop('disabled', true);
			},
			complete: function() {
				$(node).find('i').replaceWith('<i class="fa fa-upload"></i>');
				$(node).prop('disabled', false);			
			},		
			success: function(json) {
				if (json['error']) {
					$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
				}
							
				if (json['success']) {
					alert(json['success']);
					
					if (typeof json['code'] != 'undefined')
					$(node).parent().find('input').attr('value', json['code']);
					
					if (typeof json['file'] != 'undefined')
					$(node).parent().find('input').attr('value', json['file']);
				}
			},			
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});
});

$('#review').delegate('.pagination a', 'click', function(e) {
	e.preventDefault();
	
    $('#review').fadeOut('slow');
        
    $('#review').load(this.href);
    
    $('#review').fadeIn('slow');
});         

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
    $.ajax({
        url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
        type: 'post',
        dataType: 'json',
        data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
        beforeSend: function() {
            $('#button-review').button('loading');
        },
        complete: function() {
            $('#button-review').button('reset');
			$('#captcha').attr('src', '<?php if ($opencart_version >= 2000) {?>index.php?route=tool/captcha<?php } else { ?>index.php?route=product/product/captcha<?php }?>#'+new Date().getTime());
			$('input[name=\'captcha\']').val('');
        },
        success: function(json) {
			$('.alert-success, .alert-danger').remove();
            
			if (json['error']) {
                $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
            }
            
            if (json['success']) {
                $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
                                
                $('input[name=\'name\']').val('');
                $('textarea[name=\'text\']').val('');
                $('input[name=\'rating\']:checked').prop('checked', false);
                $('input[name=\'captcha\']').val('');
            }
        }
    });
});
<?php $product_page_image_gallery = nico_get_config('product_page_image_gallery');
if ($product_page_image_gallery == 'popup') 
{?>
$(document).ready(function() {
	$('.image-additional > ul > li, .product-info .image').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: { 
			enabled:true 
		}
	});
});
<?php } else { ?> 
$.fn.CloudZoom.defaults = {
	zoomWidth:"auto",
	zoomHeight:"auto",
	position:"inside",
	adjustX:0,
	adjustY:0,
	adjustY:"",
	tintOpacity:0.5,
	lensOpacity:0.5,
	titleOpacity:0.5,
	smoothMove:3,
	showTitle:false};				


	//add a small delay when loaded trough quickview
	if (jQuery("#cboxLoadedContent .container").length = 0)
		$('.cloud-zoom, .cloud-zoom-gallery').CloudZoom();
	else setTimeout(function ()
	{
		$('.cloud-zoom, .cloud-zoom-gallery').CloudZoom();
	}, 1000);
	 

	image_additional = $('.image-additional');
	if (image_additional.length)
	$(image_additional).flexslider({
			animation:"slide",
			direction:"horizontal",
			startAt:0,
			initDelay:0,
			slideshowSpeed:7000,
			animationSpeed:600,
			prevText:"",
			nextText:"",
			pauseText:"Pause",
			playText:"Play",
			pausePlay:false,
			controlNav:false,
			slideshow:false,
			animationLoop:true,
			randomize:false,
			smoothHeight:false,
			useCSS:true,
			pauseOnHover:true,
			pauseOnAction:true,
			touch:true,
			video:false,
			mousewheel:false,
			keyboard:false,
			itemWidth:74
			}).data('flexslider').resize();	 
<?php }?>
$('.date,.time,.datetime').datetimepicker({
	pickTime: true
});
//--></script>
<?php echo $footer;?>
