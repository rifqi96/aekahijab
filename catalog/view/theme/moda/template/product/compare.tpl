<?php 
$nico_include_path = __DIR__. '/../../';
//vqmod changes paths and the above path fails, check other paths
if (!file_exists($nico_include_path . 'nico_theme_editor/common.inc')) 
{
	if (isset($_SERVER['SCRIPT_FILENAME']))
	{
		$nico_include_path = dirname($_SERVER['SCRIPT_FILENAME']) . '/catalog/view/theme/moda/';
	}
	
	if (!file_exists($nico_include_path . '/nico_theme_editor/common.inc') && isset($_SERVER['DOCUMENT_ROOT']))
	{
		$nico_include_path = $_SERVER['DOCUMENT_ROOT'] . '/catalog/view/theme/moda/';
	}	
	
	if (!file_exists($nico_include_path . '/nico_theme_editor/common.inc')) $nico_include_path = dirname(__FILE__) . '/../../';
}

if (file_exists($nico_include_path . 'nico_theme_editor/common.inc')) require_once($nico_include_path . 'nico_theme_editor/common.inc');
global $_config;
echo $header; 
?>
<div class="container">
	        
		<div class="row">
		    <div class="col-md-12">
			    <div class="breadcrumbs">
					
						<ul class="breadcrumb">
						<?php foreach ($breadcrumbs as $breadcrumb) { ?>
						<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
						<?php } ?>
						</ul>
				</div>
			</div>
		</div>
		
		<div class="row">
		    <div class="col-md-12">
			    <div class="cat_header">
				    <h2><?php echo $heading_title; ?></h2>
			    </div>

			</div>
		</div>
		
		
	<div class="row">
	<?php if (!empty($column_left)) { 
		$cols = 9;
		?> 
		<?php echo $column_left; ?> 
		<div class="col-md-9">
	<?php } else {
		$cols = 12;?>
		<div class="col-md-12">
	<?php } ?>
		<div class="row">
			<div class="col-md-12">
			<div class="registerbox">
			<?php if ($products) { ?>
			  <table class="compare-info">
				<thead>
				  <tr>
					<td class="compare-product" colspan="<?php echo count($products) + 1; ?>"><?php echo $text_product; ?></td>
				  </tr>
				</thead>
				<tbody>
				  <tr>
					<td><?php echo $text_name; ?></td>
					<?php foreach ($products as $product) { ?>
					<td class="name"><a href="<?php echo $products[$product['product_id']]['href']; ?>"><?php echo $products[$product['product_id']]['name']; ?></a></td>
					<?php } ?>
				  </tr>
				  <tr>
					<td><?php echo $text_image; ?></td>
					<?php foreach ($products as $product) { ?>
					<td><?php if ($products[$product['product_id']]['thumb']) { ?>
					  <img src="<?php echo $products[$product['product_id']]['thumb']; ?>" alt="<?php echo $products[$product['product_id']]['name']; ?>" />
					  <?php } ?></td>
					<?php } ?>
				  </tr>
				  <tr>
					<td><?php echo $text_price; ?></td>
					<?php foreach ($products as $product) { ?>
					<td><?php if ($products[$product['product_id']]['price']) { ?>
					  <?php if (!$products[$product['product_id']]['special']) { ?>
					  <?php echo $products[$product['product_id']]['price']; ?>
					  <?php } else { ?>
					  <span class="price-old"><?php echo $products[$product['product_id']]['price']; ?></span> <span class="price-new"><?php echo $products[$product['product_id']]['special']; ?></span>
					  <?php } ?>
					  <?php } ?></td>
					<?php } ?>
				  </tr>
				  <tr>
					<td><?php echo $text_model; ?></td>
					<?php foreach ($products as $product) { ?>
					<td><?php echo $products[$product['product_id']]['model']; ?></td>
					<?php } ?>
				  </tr>
				  <tr>
					<td><?php echo $text_manufacturer; ?></td>
					<?php foreach ($products as $product) { ?>
					<td><?php echo $products[$product['product_id']]['manufacturer']; ?></td>
					<?php } ?>
				  </tr>
				  <tr>
					<td><?php echo $text_availability; ?></td>
					<?php foreach ($products as $product) { ?>
					<td><?php echo $products[$product['product_id']]['availability']; ?></td>
					<?php } ?>
				  </tr>
				  <tr>
					<td><?php echo $text_rating; ?></td>
					<?php foreach ($products as $product) { ?>
					<td><img src="catalog/view/theme/moda/img/stars-<?php echo $products[$product['product_id']]['rating']; ?>.png" alt="<?php echo $products[$product['product_id']]['reviews']; ?>" /><br />
					  <?php echo $products[$product['product_id']]['reviews']; ?></td>
					<?php } ?>
				  </tr>
				  <tr>
					<td><?php echo $text_summary; ?></td>
					<?php foreach ($products as $product) { ?>
					<td class="description"><?php echo $products[$product['product_id']]['description']; ?></td>
					<?php } ?>
				  </tr>
				  <tr>
					<td><?php echo $text_weight; ?></td>
					<?php foreach ($products as $product) { ?>
					<td><?php echo $products[$product['product_id']]['weight']; ?></td>
					<?php } ?>
				  </tr>
				  <tr>
					<td><?php echo $text_dimension; ?></td>
					<?php foreach ($products as $product) { ?>
					<td><?php echo $products[$product['product_id']]['length']; ?> x <?php echo $products[$product['product_id']]['width']; ?> x <?php echo $products[$product['product_id']]['height']; ?></td>
					<?php } ?>
				  </tr>
				</tbody>
				<?php foreach ($attribute_groups as $attribute_group) { ?>
				<thead>
				  <tr>
					<td class="compare-attribute" colspan="<?php echo count($products) + 1; ?>"><?php echo $attribute_group['name']; ?></td>
				  </tr>
				</thead>
				<?php foreach ($attribute_group['attribute'] as $key => $attribute) { ?>
				<tbody>
				  <tr>
					<td><?php echo $attribute['name']; ?></td>
					<?php foreach ($products as $product) { ?>
					<?php if (isset($products[$product['product_id']]['attribute'][$key])) { ?>
					<td><?php echo $products[$product['product_id']]['attribute'][$key]; ?></td>
					<?php } else { ?>
					<td></td>
					<?php } ?>
					<?php } ?>
				  </tr>
				</tbody>
				<?php } ?>
				<?php } ?>
				<tr>
				  <td></td>
				  <?php foreach ($products as $product) { ?>
				  <td><input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button" /></td>
				  <?php } ?>
				</tr>
				<tr>
				  <td></td>
				  <?php foreach ($products as $product) { ?>
				  <td class="remove"><a href="<?php echo $product['remove']; ?>" class="button btn btn-primary"><?php echo $button_remove; ?></a></td>
				  <?php } ?>
				</tr>
			  </table>
			  <div class="buttons">
				<div class="right"><a href="<?php echo $continue; ?>" class="button btn btn-primary"><?php echo $button_continue; ?></a></div>
			  </div>
			  <?php } else { ?>
			  <div class="content"><?php echo $text_empty; ?></div>
			  <div class="buttons">
				<div class="right"><a href="<?php echo $continue; ?>" class="button btn btn-primary"><?php echo $button_continue; ?></a></div>
			  </div>
			  <?php } ?>
			</div>
			</div>
		</div>	
	</div>	
	</div>	
</div>
<?php echo $footer; ?>
