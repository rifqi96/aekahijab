<!--cols:<?php if ($grid_cols) echo $grid_cols + $grid_offset;else echo 12;?>   -->
<?php 
global $_config, $opencart_version;
if (!$opencart_version) $opencart_version = (int)str_replace('.','',VERSION);
$nico_include_path = __DIR__. '/../../';
//vqmod changes paths and the above path fails, check other paths
if (!file_exists($nico_include_path . 'nico_theme_editor/common.inc')) 
{
	if (isset($_SERVER['SCRIPT_FILENAME']))
	{
		$nico_include_path = dirname($_SERVER['SCRIPT_FILENAME']) . '/catalog/view/theme/moda/';
	}
	
	if (!file_exists($nico_include_path . '/nico_theme_editor/common.inc') && isset($_SERVER['DOCUMENT_ROOT']))
	{
		$nico_include_path = $_SERVER['DOCUMENT_ROOT'] . '/catalog/view/theme/moda/';
	}	
	
	if (!file_exists($nico_include_path . '/nico_theme_editor/common.inc')) $nico_include_path = dirname(__FILE__) . '/../../';
}

if (file_exists($nico_include_path . 'nico_theme_editor/common.inc')) require_once($nico_include_path . 'nico_theme_editor/common.inc');

global $_config;
$_config =  $this->registry->get('config');
$module_id = 'grid_' . $layout_id . '_' . $position . '_' .  $sort_order;
//  style="min-height: <?php echo $row_height * ($max_row + 1);
ob_start();
?>
#<?php echo $module_id;?> .row {
    min-height: <?php echo $row_height * 10;?>px;
 	height: <?php echo $row_height?>vw;
 	margin:0px;
 	padding:0px;
}

@media only screen and (min-width : 768px) {
	#<?php echo $module_id;?> .row {
		height: <?php echo $row_height?>vw;
		min-height: <?php echo $row_height * 10;?>px;
	}
}

#<?php echo $module_id;?> .grid_item {
	position:absolute;
	padding:0px;
}	

#<?php echo $module_id;?> .grid_item > div, #<?php echo $module_id;?> .grid_item > div .background{
	padding-top:<?php echo $margin_top;?>px;
	padding-left:<?php echo $margin_left;?>px;
}


<?php for ($i = 0;$i<=12;$i++) {?>
#<?php echo $module_id;?> .grid_item.size-y-<?php echo $i;?>
{
	height:<?php echo $row_height * $i;?>vw;
	min-height:<?php echo $row_height * 10 * $i;?>px;
}	
<?php }?>

@media only screen and (min-width : 1400px) 
{

	#<?php echo $module_id;?> .row {
		min-height: <?php echo ($row_height - 1) * 10;?>px;
		height: <?php echo ($row_height - 1)?>vw;
	}
	
	<?php for ($i = 0;$i<=12;$i++) {?>
	#<?php echo $module_id;?> .grid_item.size-y-<?php echo $i;?>
	{
		height:<?php echo ($row_height - 1)  * $i;?>vw;
	}	
	<?php }?>
}


@media only screen and (min-width : 2000px) 
{

	#<?php echo $module_id;?> .row {
		min-height: <?php echo ($row_height - 2) * 10;?>px;
		height: <?php echo ($row_height - 2)?>vw;
	}
	
	<?php for ($i = 0;$i<=12;$i++) {?>
	#<?php echo $module_id;?> .grid_item.size-y-<?php echo $i;?>
	{
		height:<?php echo ($row_height - 2)  * $i;?>vw;
	}	
	<?php }?>
}
<?php
$nico_grid_css = ob_get_clean();
nico_add_css(str_replace(array("\r","\n", '  ',"\t"),array('','','',''), $nico_grid_css));
?>
<div class="grid_list <?php if ($grid_cols) {?>col-md-<?php echo $grid_cols;}?> <?php if ($grid_offset) {?>col-md-offset-<?php echo $grid_offset;}?> <?php if ($grid_padding) {?>no_padding<?php if ($grid_padding == 2) echo 'left';else if ($grid_padding == 3) echo 'right'; }?>"  id="<?php echo $module_id;?>">
<?php
		
		for ($i=1;$i<=$max_row;$i++)
		{
				if (isset($group[$i])) {?> <div class="row col-md-12 col-sm-12 col-xs-12"><?php
				$offset = 0;
				foreach($group[$i] as $item) 
				{
					$offset = $item['col'] - 1;
					?>
						<div class="col-md-<?php echo $item['size_x'];?> col-sm-<?php echo $item['size_x'];?> col-xs-<?php echo $item['size_x'];?> col-md-offset-<?php echo $offset;?> col-sm-offset-<?php echo $offset;?> col-xs-offset-<?php echo $offset;?> size-y-<?php echo $item['size_y'];?> grid_item">
								<div>
									<div>
										<div style="<?php if (isset($item['background'])) {?>background-color:<?php echo $item['background'];}?>;">
											<?php if (isset($item['img'])) {?><div class="background" style="background-image:url('<?php echo $item['img'];?>')">
<div><div></div></div>
</div><?php }?>
											<div class="grid_text">
											<?php if (isset($item['subtitle'])) {?><h3><?php echo $item['subtitle'];?></h3><?php }?>
											<br/>
											<?php if (isset($item['title'])) {?><h2><?php echo $item['title'];?></h2><?php }?>
											<br/>
											<?php /*if (isset($item['subtitle'])) {?><h3><?php echo $item['subtitle'];?></h3><?php }*/?>
											<?php if (isset($item['button'])) {?><a href="<?php echo htmlentities($item['url']);?>"><?php echo $item['button'];?></a><?php }?>
											</div>
										</div>
									</div>
								</div>
								
						
						</div>
					<?php
				}?></div><?php } else
				{?>
					<div class="row col-md-12 col-sm-12 col-xs-12"></div>	
				<?php 
				}
		}
?>
<div class="row col-md-12 col-sm-12 col-xs-12"></div>
</div>
