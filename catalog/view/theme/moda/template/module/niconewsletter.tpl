<!--cols:<?php if (isset($grid_cols) && $grid_cols) echo $grid_cols + $grid_offset;else echo 12;?>   -->
<div id="newsletter"  class="<?php if (isset($grid_cols) &&$grid_cols) {?>col-md-<?php echo $grid_cols;}?> <?php if (isset($grid_offset) && $grid_offset) {?>col-md-offset-<?php echo $grid_offset;}?> <?php if (isset($grid_padding) && $grid_padding) {?>no_padding<?php if (isset($grid_padding) && $grid_padding == 2) echo 'left';else if (isset($grid_padding) && $grid_padding == 3) echo 'right'; }?>">
<?php
global $_config;
?>
<div class="container">
	<div class="row">
	<div class="col-md-12">
		<div class="newsletter clearfix">

			<!-- Begin MailChimp Signup Form -->
				<form action="<?php if ($type == 1) {?>/index.php?route=account/newsletter<?php } else { ?>http://<?php echo $mailchimp_key;} ?>"
					class="validate" id="mc-embedded-subscribe-form" method="post" name="mc-embedded-subscribe-form" 
					novalidate="" target="_blank">
						<h3><?php $lang = $_config->get('config_language'); $newsletter_text = nico_get_config('newsletter_text');$newsletter_text = isset($newsletter_text[$lang])?$newsletter_text[$lang]:$newsletter_text['en'];if (!empty($newsletter_text)) echo $newsletter_text; else echo 'Newsletter';?></h3>
						<div>
						<input class="email" id="mce-EMAIL" name="EMAIL" placeholder="Email address" required="" type="email" value=""> 
						<input class="button btn btn-primary" id="mc-embedded-subscribe" name="subscribe" type="submit" value="<?php $subscribe_text = nico_get_config('subscribe_text');$subscribe_text = isset($subscribe_text[$lang])?$subscribe_text[$lang]:$subscribe_text['en'];if (!empty($subscribe_text)) echo $subscribe_text; else echo 'Subscribe';?>">&nbsp;
						<input type="hidden" value="1" name="newsletter">
						</div>
				</form>
			<!--End mc_embed_signup-->
		</div>
	</div>
	</div>
</div>
</div>
