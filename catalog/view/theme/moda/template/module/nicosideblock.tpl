<div class="screen_<?php echo $screen_position;?>">
	<div>
		<div>
			<div>
			<?php if (isset($section) && is_array($section)) foreach ($section as $slide) {?>
			<div class="sideblock" style="width:<?php echo $slide['content_width'] + 40;?>px">
				<div class="body" style="width:<?php echo $slide['content_width'];?>px;left:<?php echo $slide['width'];?>px;height:<?php echo $slide['content_height'];?>px;">
			<?php
			switch ($slide['section_type'])
			{
				/* ------------------------------ youtube ----------------------*/
				case 'youtube':
			?>  
				<iframe width="<?php echo $slide['content_width'];?>" height="<?php echo $slide['content_height'];?>" src="//www.youtube.com/embed/<?php echo $slide['youtube_id'];?>" allowfullscreen></iframe>
			<?php
				break;
				
				/* ------------------------------ vimeo ----------------------*/
				case 'vimeo':
			?>
				<iframe src="//player.vimeo.com/video/<?php echo $slide['vimeo_id'];?>"  width="<?php echo $slide['content_width'];?>" height="<?php echo $slide['content_height'];?>" allowfullscreen></iframe>
			<?php
				break;
				
				/* ------------------------------ facebook ----------------------*/
				case 'facebook':
			?>
				<div class="fb-root"></div>
				<script>(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>	

				<div class="fb-like-box" data-href="https://www.facebook.com/<?php echo $slide['facebook_id'];?>" data-width="<?php echo $slide['content_width'];?>"  data-height="<?php echo $slide['content_height'];?>" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="false"></div>	
			<?php
				break;

				/* ------------------------------ twitter ----------------------*/
				case 'twitter':
			?>

				<a class="twitter-timeline" href="https://twitter.com/twitterapi" data-widget-id="<?php echo $slide['twitter_id'];?>"></a>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
			<?php
				break;
				
				/* ------------------------------ text ----------------------*/
				case 'text':
			?>
				<?php echo html_entity_decode($slide['text']);?>
			<?php
				break;
			}	
			?>	
				</div>
				<div class="head" style="background:<?php echo $slide['background'];?>;color:<?php echo $slide['color'];?>;width:40px;height:<?php echo $slide['height'];?>px;">
					<div class="icon fa fa-<?php echo $slide['icon'];?>"></div>
					<div class="title"><?php echo $slide['icon_text'];?></div>
				</div>
			</div>
			<?php }?>
			</div>
		</div>
	</div>
</div>
