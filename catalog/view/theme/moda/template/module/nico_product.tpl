<?php
if (!function_exists('nico_product')) {

    if (!function_exists('toInt')) 
    {
	function toInt($str)
	{
	    return (int)preg_replace("/([^0-9\\.])/i", "", $str);
	}
    }


    function nico_product($product)
    {
	global $_button_cart, $_config, $sale_badge_text;
	if ($sale_badge_text == null) $sale_badge_text = nico_get_config('sale_badge_text');
    ?>
    <div class="product">
	  <?php if ($product['special'] && ($price = toInt($product['price'])) > 0) { ?>
	    <div class="product_sale">
	    <?php if (!empty($sale_badge_text)) echo $sale_badge_text; 
		else 
		{
		
		    echo '-' . round((($price - toInt($product['special'])) / $price) * 100) . '%';
		}?>
	    </div>
	  <?php } ?>
	   <div class="image<?php if (!empty($product['additional_image'])) {?> hover_img<?php }?>">
		   <a href="<?php echo $product['href']; ?>">
			<img class="thumb" src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" />
			<?php if (!empty($product['additional_image'])) {?><img  class="additional_image" src="<?php echo $product['additional_image']; ?>" alt="<?php echo $product['name']; ?>" /><?php } ?>
			</a>
			
			<div class="hover">
				 <div>
					 <a href="<?php echo $product['href']; ?>">
						<span class="add-to-cart" onclick="return addToCart('<?php echo $product['product_id']; ?>'); return false"><i class="fa fa-shopping-cart"></i></span>
						<span class="quickview"><i class="fa fa-search "></i></span>
					 </a>
				 </div>
			 </div>
		 </div>

		 

	    <div class="actions">
		<div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>				    
		<div class="rating">
		  <?php for ($i = 1; $i <= 5; $i++) { ?>
		  <?php if ($product['rating'] < $i) { ?>
		  <span class="fa"><i class="fa fa-star-o"></i></span>
		  <?php } else { ?>
		  <span class="fa"><i class="fa fa-star"></i></span>
		  <?php } ?>
		  <?php } ?>
		</div>
		<div class="description"><?php if (isset($product['description'])) echo $product['description']; ?></div>				    
		<?php if ($product['price']) { ?>
		<div class="price<?php if ($product['special']) { ?> price_sale<?php }?>">
		  <?php if (!$product['special']) { ?>
		  <p><?php echo $product['price']; ?></p>
		  <?php } else { ?>
		  <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
		  <?php } ?>
		</div>
		<?php } ?>
		<button class="btn btn-primary btn-sm" onclick="return addToCart('<?php echo $product['product_id']; ?>');"><?php echo $_button_cart; ?></button>
	    </div>
    </div>
    <?php
    }
}
