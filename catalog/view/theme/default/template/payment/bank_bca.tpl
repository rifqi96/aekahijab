<style>
	.bank{
		background-color:#FF99CC;
		font-weight:bolder;
		font-size:36px;
		color:#FFFFFF;
	}
	
	.bank:hover{
		background-color:#000000;
	}
</style>

<h2><?php echo $text_instruction; ?></h2>
<p><b><?php echo $text_description; ?></b></p>
<!--<div class="well well-sm">-->
  <div class = "bank"><?php echo $bank; ?></div>
  <?php echo $text_payment; ?>
<!--</div>-->
<div class="buttons">
  <div class="pull-right">
    <input type="button" value="<?php echo $button_confirm; ?>" id="button-confirm" class="btn btn-primary" />
  </div>
</div>
<script type="text/javascript"><!--
$('#button-confirm').on('click', function() {
	$.ajax({ 
		type: 'get',
		url: 'index.php?route=payment/bank_bca/confirm',
		cache: false,
		beforeSend: function() {
			$('#button-confirm').button('loading');
		},
		complete: function() {
			$('#button-confirm').button('reset');
		},		
		success: function() {
			location = '<?php echo $continue; ?>';
		}		
	});
});
//--></script> 
