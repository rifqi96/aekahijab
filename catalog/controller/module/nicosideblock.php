<?php
include_once('nicomodule.inc');
class ControllerModuleNicosideblock  extends NicoModule
{
	public function index($setting) 
	{
		if (!$this->is_filter_ok($setting)) return false;
		$data = $setting;
		
		$opencart2 = $data['opencart2'] = ((int)substr(VERSION,0,1) == 2);
		$lang_code = $this->language->get('code');

		if (isset($setting['section'])) foreach ($setting['section'] as $nr => $section)
		{
			$data['section'][$nr] = $section;
			$data['section'][$nr]['icon_text'] = isset($section['icon_text'][$lang_code])?$section['icon_text'][$lang_code]:$section['icon_text']['en'];
			$data['section'][$nr]['text'] = isset($section['text'][$lang_code])?$section['text'][$lang_code]:$section['text']['en'];
		}

		$data['screen_position'] = $setting['screen_position'];

		if ($opencart2)
		{
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/nicosideblock.tpl')) 
			{
				return $this->load->view($this->config->get('config_template') . '/template/module/nicosideblock.tpl', $data);
			} else {
				return $this->load->view('default/template/module/nicosideblock.tpl', $data);
			}
		} else
		{
			$this->data = $data;
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/nicosideblock.tpl')) 
			{
				$this->template = $this->config->get('config_template') . '/template/module/nicosideblock.tpl';
			} else {
				$this->template = 'default/template/module/nicosideblock.tpl';
			}

			$this->render();
		}
	}
}
?>
