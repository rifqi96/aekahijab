<?php
class NicoModule extends Controller 
{
	function is_filter_ok(&$data)
	{
		if (isset($data['filter_type']) && !empty($data['filter_type']))
		{
			$filter_type = $data['filter_type'];
			$route = (string)$this->request->get['route'];
			switch($filter_type)
			{
					case 'category':
					if (isset($this->request->get['path']))
					{
					$path = $this->request->get['path'];
					$underscore = strrpos($path, '_');
					$category_id = substr($path, $underscore?$underscore+1:0);
					if ($route == 'product/category' && 
						isset($path) && 
						$data['filter_category'] == $category_id) 
					{
						return true;
						}
					}
					//if category filter is set on product page check if product has category
					if ($route == 'product/product') 
					{
						$product_id = $this->request->get['product_id'];
						$this->load->model('catalog/product');
						
						$categories = $this->model_catalog_product->getCategories($product_id);
						foreach ($categories as $category)
						{
							if ($category['category_id'] == $data['filter_category']) return true;
						}
					}
					break;

					case 'product':
					$product_id = $this->request->get['product_id'];
					if ($route == 'product/product' && 
						$data['filter_products'] == $product_id) 
					{
						return true;
					}
					break;

					case 'manufacturer':
					$manufacturer_id = $this->request->get['manufacturer_id'];
					if ($route == 'product/manufacturer/info' && 
						$data['filter_manufacturer'] == $manufacturer_id) 
					{
						return true;
					}
					break;

					case 'information':
					$information_id = $this->request->get['information_id'];
					if ($route == 'information/information' && 
						$data['filter_information'] == $information_id) 
					{
						return true;
					}
					break;
			}
			return false;
		}
		return true;
	}
}
