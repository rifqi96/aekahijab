<?php
include_once('nicomodule.inc'); 
class ControllerModuleNicogooglemaps  extends NicoModule
{
	public function index($setting) 
	{
		if (!$this->is_filter_ok($setting)) return false;
		$data = $setting;
		if (!isset($data['position'])) $data['position'] = rand(0, 10);
		if (!isset($data['layout_id'])) $data['layout_id'] = 0;
		if (!isset($data['sort_order'])) $data['sort_order'] = rand(0, 10);
		
		$data['markers'] = $setting['section'];

		if (isset($setting['section'])) foreach ($setting['section'] as $nr => $section)
		{
			$data['markers'][$nr]['description'] = $section['description'][$this->language->get('code')];
		}
		
		$data['width'] = $setting['width'];
		$data['height'] = $setting['height'];

		$data['lat'] = $setting['lat'];
		$data['long'] = $setting['long'];
		$data['zoom'] = $setting['zoom'];

		$opencart2 = ((int)substr(VERSION,0,1) == 2);

		if ($opencart2)
		{
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/nicogooglemaps.tpl')) 
			{
				return $this->load->view($this->config->get('config_template') . '/template/module/nicogooglemaps.tpl', $data);
			} else {
				return $this->load->view('default/template/module/nicogooglemaps.tpl', $data);
			}
		} else
		{
			$this->data = $data;
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/nicogooglemaps.tpl')) 
			{
				$this->template = $this->config->get('config_template') . '/template/module/nicogooglemaps.tpl';
			} else {
				$this->template = 'default/template/module/nicogooglemaps.tpl';
			}

			$this->render();
		}
	}
}
