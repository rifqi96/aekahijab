<?php
include_once('nicomodule.inc'); 
class ControllerModuleNicoproducttabs  extends NicoModule
{
	public function index($setting) 
	{
		if (!$this->is_filter_ok($setting)) return false;

		$data = $setting;
		$data['opencart2'] = ((int)substr(VERSION,0,1) == 2);
		if ($data['opencart2'])
		{
			$this->load->language('module/nicoproducttabs');
		} else
		{
			$this->language->load('module/nicoproducttabs'); 
		}
		
		$lang_code = $this->language->get('code');

		if (isset($setting['section'])) foreach ($setting['section'] as $nr => $section)
		{
			$data['section'][$nr] = $section;
			if (isset($section['title'])) $data['section'][$nr]['title'] = isset($section['title'][$lang_code])?$section['title'][$lang_code]:$section['title']['en'];
			if (isset($section['subtitle'])) $data['section'][$nr]['subtitle'] = isset($section['subtitle'][$lang_code])?$section['subtitle'][$lang_code]:$section['subtitle']['en'];
			if (isset($section['text'])) $data['section'][$nr]['text'] =isset($section['text'][$lang_code])?$section['text'][$lang_code]:$section['text']['en'];
		}


		if ($data['opencart2'])
		{
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/nicoproducttabs.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/nicoproducttabs.tpl', $data);
			} else {
				return $this->load->view('default/template/module/nicoproducttabs.tpl', $data);
			}
		} else
		{
			$this->data = $data;
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/nicoproducttabs.tpl')) 
			{
				$this->template = $this->config->get('config_template') . '/template/module/nicoproducttabs.tpl';
			} else {
				$this->template = 'default/template/module/nicoproducttabs.tpl';
			}
		}

		$this->render();
	}
}
?>
