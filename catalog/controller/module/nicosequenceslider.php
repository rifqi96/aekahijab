<?php
include_once('nicomodule.inc');
class ControllerModuleNicosequenceslider  extends NicoModule
{
	public function index($setting) 
	{
		if (!$this->is_filter_ok($setting)) return false;

		$this->load->model('tool/nicoimage');
		$lang_code = strtolower(isset($this->session->data['language'])?$this->session->data['language']:$this->language->get('code'));
		
		$data = $setting;
		if (!isset($data['position'])) $data['position'] = rand(0, 10);
		if (!isset($data['layout_id'])) $data['layout_id'] = 0;
		if (!isset($data['sort_order'])) $data['sort_order'] = rand(0, 10);

		if (isset($setting['section'])) foreach ($setting['section'] as $nr => $section)
		{
			$data['section'][$nr] = $section;
			$data['section'][$nr]['title'] = isset($section['title'][$lang_code])?$section['title'][$lang_code]:isset($section['title']['en'])?$section['title']['en']:'';
			$data['section'][$nr]['subtitle'] = isset($section['subtitle'][$lang_code])?$section['subtitle'][$lang_code]:isset($section['subtitle']['en'])?$section['subtitle']['en']:'';
			$data['section'][$nr]['text'] = isset($section['text'][$lang_code])?$section['text'][$lang_code]:isset($section['text']['en'])?$section['text']['en']:'';
			$data['section'][$nr]['button'] = isset($section['button'][$lang_code])?$section['button'][$lang_code]:isset($section['button']['en'])?$section['button']['en']:'';
			if ($section['image_image']) $data['section'][$nr]['image'] = $this->model_tool_nicoimage->cropsize($section['image_image'], $setting['image_width'], $setting['image_height']);//$this->config->get('config_url') . 'image/' . $section['image_image'];//$this->model_tool_nicoimage->resize($section['image_image'], 500, 500);
			$data['section'][$nr]['background'] = $this->model_tool_nicoimage->cropsize($section['background_image'], $setting['background_image_width'], $setting['background_image_height']);
		}

		$opencart2 = ((int)substr(VERSION,0,1) == 2);

		if ($opencart2)
		{
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/nicosequenceslider.tpl')) 
			{
				return $this->load->view($this->config->get('config_template') . '/template/module/nicosequenceslider.tpl', $data);
			} else {
				return $this->load->view('default/template/module/nicosequenceslider.tpl', $data);
			}
		} else
		{
			$this->data = $data;
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/nicosequenceslider.tpl')) 
			{
				$this->template = $this->config->get('config_template') . '/template/module/nicosequenceslider.tpl';
			} else {
				$this->template = 'default/template/module/nicosequenceslider.tpl';
			}

			$this->render();
		}
	}
}
?>
