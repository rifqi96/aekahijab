<?php
include_once('nicomodule.inc'); 
class ControllerModuleNicobanner extends NicoModule
{
	public function index($setting) 
	{
		if (!$this->is_filter_ok($setting)) return false;
		
		$this->language->load('module/nicobanner'); 
		$this->load->model('tool/nicoimage');
		$lang_code = $this->language->get('code');
		$data = $setting;
		if (!isset($data['position'])) $data['position'] = rand(0, 10);
		if (!isset($data['layout_id'])) $data['layout_id'] = 0;
		if (!isset($data['sort_order'])) $data['sort_order'] = rand(0, 10);

		if (isset($setting['section'])) foreach ($setting['section'] as $nr => $section)
		{
			$data['section'][$nr] = $section;
			if (isset($section['title'])) $data['section'][$nr]['title'] = isset($section['title'][$lang_code])?$section['title'][$lang_code]:$section['title']['en'];
			if (isset($section['subtitle'])) $data['section'][$nr]['subtitle'] = isset($section['subtitle'][$lang_code])?$section['subtitle'][$lang_code]:$section['subtitle']['en'];
			if (isset($section['text'])) $data['section'][$nr]['text'] = isset($section['text'][$lang_code])?$section['text'][$lang_code]:$section['text']['en'];
			if (isset($section['image_image'])) $data['section'][$nr]['image'] = $this->model_tool_nicoimage->cropsize($section['image_image'], $setting['image_width'], $setting['image_height']);
//			$data['section'][$nr]['image'] = $this->model_tool_nicoimage->resize($section['image_image'], 500, 400);
		}

		$data['type'] = $setting['module_type'];
		$data['cols'] = $setting['module_cols'];
		
		$opencart2 = ((int)substr(VERSION,0,1) == 2);

		if ($opencart2)
		{
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/nicobanner.tpl')) 
			{
				return $this->load->view($this->config->get('config_template') . '/template/module/nicobanner.tpl', $data);
			} else {
				return $this->load->view('default/template/module/nicobanner.tpl', $data);
			}
		} else
		{
			$this->data = $data;
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/nicobanner.tpl')) 
			{
				$this->template = $this->config->get('config_template') . '/template/module/nicobanner.tpl';
			} else {
				$this->template = 'default/template/module/nicobanner.tpl';
			}

			$this->render();
		}
	}
}
?>
