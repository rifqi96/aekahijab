<?php
include_once('nicomodule.inc');
class ControllerModuleNicomegamenu  extends NicoModule
{
	function get_products($product_list)
	{
		if (!$this->is_filter_ok($setting)) return false;

		$this->load->model('tool/image');

		$products_list = explode(',', $product_list);

		$this->load->model('catalog/product');

		foreach ($products_list as $product_id) 
		{
			$_product = $this->model_catalog_product->getProduct((int)$product_id);

			if ($_product['image']) 
			{
				$image = $this->model_tool_image->resize($_product['image'], 200, 160);
			} else {
				$image = false;
			}


			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) 
			{
				$price = $this->currency->format($this->tax->calculate($_product['price'], $_product['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$price = false;
			}
					
			if (isset($_product['special']) && (float)$_product['special']) 
			{
				$special = $this->currency->format($this->tax->calculate($_product['special'], $_product['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$special = false;
			}
			
			if ($this->config->get('config_review_status') && isset($_product['rating'])) 
			{
				$rating = $_product['rating'];
			} else {
				$rating = false;
			}

			
			$product = 
			array(
				'product_id' => $_product['product_id'],
				'thumb'   	 => $image,
				'name'    	 => $_product['name'],
				
				'price'   	 => $price,
				'special' 	 => $special,
				'rating'     => $rating,
				'href'    	 => $this->url->link('product/product', 'product_id=' . $_product['product_id']),
			);
			
			$products[] = $product;					
		}



		return $products;
	}

	function get_categories($category)
	{
		$this->load->model('catalog/category');
		$this->load->model('tool/image');
		
		$categories = $this->model_catalog_category->getCategories($category);
		
		if ($categories)
		foreach ($categories as &$category)
		{
			$_category = array();
			$_category['name'] = $category['name'];
			$_category['category_id'] = $category['category_id'];
			$_category['url'] = $this->url->link('product/category', 'path=' . $category['category_id']);
			$_category['thumb'] = '';
			if (!empty($category['image'])) $_category['thumb'] = $this->model_tool_image->resize($category['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));			
			$_category['children'] = $this->model_catalog_category->getCategories($category['category_id']);
			
			foreach($_category['children'] as &$child)
			{
				$child['url'] = $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']);	
			}
			
			$category = $_category;
		}
		
		return $categories;
	}
	
	function get_products_category($category_id, $limit = 4)
	{
		
		switch($category_id)
		{
			case -3:
				$products = $this->model_catalog_product->getBestSellerProducts($limit);
			break;
			case -2:
				$products = $this->model_catalog_product->getPopularProducts($limit);
			break;
			case -1:
				$products = $this->model_catalog_product->getLatestProducts($limit);
			break;
			default:
			 $products = $this->model_catalog_product->getProducts(array('filter_category_id' => $category_id, 'start' => 0,'limit' => $limit));
		}
		

		foreach ($products as &$product) 
		{
		
			if ($product['image']) 
			{
				$product['thumb'] = $this->model_tool_image->resize($product['image'], 200, 160);
			}

			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) 
			{
				$product['price'] = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
			} 
					
			if (isset($product['special']) && (float)$product['special']) 
			{
				$product['special'] = $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$special = false;
			}
			
			if ($this->config->get('config_review_status') && isset($product['rating'])) 
			{
				//$rating = $product['rating'];
			}

			$product['href'] = $this->url->link('product/product', 'product_id=' . $product['product_id']);
		}
		return $products;
	}
	
	function get_manufacturers($data)
	{
		$this->load->model('catalog/manufacturer');
		$this->load->model('tool/image');
		
		$manufacturers = $this->model_catalog_manufacturer->getManufacturers($data);

		foreach ($manufacturers as &$manufacturer)
		{
			if (!empty($manufacturer['image'])) $manufacturer['thumb'] = $this->model_tool_image->resize($manufacturer['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));			
			$manufacturer['url'] = $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $manufacturer['manufacturer_id']);
		}
		
		return $manufacturers;
	}	
	
	public function index($setting) 
	{
		$data = array();
		$data = $setting;
		
		$data['menu'] = json_decode(html_entity_decode($setting['menu']), true);
		$data['_this'] = $this;
		$data['type'] = $setting['type'];
		$data['button_cart'] = $this->language->get('button_cart');
		

		$opencart2 = ((int)substr(VERSION,0,1) == 2);

		if ($opencart2)
		{
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/nicomegamenu.tpl')) 
			{
				return $this->load->view($this->config->get('config_template') . '/template/module/nicomegamenu.tpl', $data);
			} else {
				return $this->load->view('default/template/module/nicomegamenu.tpl', $data);
			}
		} else
		{
			$this->data = $data;
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/nicomegamenu.tpl')) 
			{
				$this->template = $this->config->get('config_template') . '/template/module/nicomegamenu.tpl';
			} else {
				$this->template = 'default/template/module/nicomegamenu.tpl';
			}

			$this->render();
		}
	}
}
