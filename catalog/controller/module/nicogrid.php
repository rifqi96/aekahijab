<?php
include_once('nicomodule.inc');
class ControllerModuleNicogrid  extends NicoModule
{
	public function index($setting) 
	{
		if (!$this->is_filter_ok($setting)) return false;

		$this->load->model('tool/nicoimage');
		$this->load->model('tool/image');

		$data = $setting;
		if (!isset($data['position'])) $data['position'] = rand(0, 10);
		if (!isset($data['layout_id'])) $data['layout_id'] = 0;
		if (!isset($data['sort_order'])) $data['sort_order'] = rand(0, 10);

		$data['grid'] = json_decode(html_entity_decode($setting['grid']), true);
		$code = str_replace('-', '$', $this->language->get('code'));

		$max_row = 0;
		$rows = array();
		if (!isset($data['row_height'])) $data['row_height'] = 5;

		$resize_method = 0;
		$resize_factor = 15;
		if (isset($setting['resize_method']))
		{
			if ($setting['resize_method'] == 'cropresize')
			{
				$resize_method = 1;
			} else
			if ($setting['resize_method'] == 'original')
			{
				$resize_method = 2;
			}
		}

		if (isset($setting['resize_factor']))
		{
			$resize_factor = $setting['resize_factor'];
		}

		foreach ($data['grid'] as &$item)
		{
			//var_dump($item);
			$max_row = ($max_row < $item['row'])?$item['row']:$max_row;
			$grid[$item['row']][] = &$item;
			
			if (isset($item['img']))
			{
					if ($resize_method == 1)
					$item['img'] = $this->model_tool_nicoimage->cropsize($item['img'], $data['row_height'] * $resize_factor * $item['size_x'] , $data['row_height'] * $resize_factor * $item['size_y']);
					else if ($resize_method == 2)
					{
						if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
							$item['img'] = $this->config->get('config_ssl') . 'image/' . $item['img'];
						} else {
							$item['img'] = $this->config->get('config_url') . 'image/' . $item['img'];
						}	
						$item['img'] = $item['img'];
					}
					else
					$item['img'] = $this->model_tool_image->resize($item['img'], $data['row_height'] * $resize_factor * $item['size_x'] , $data['row_height'] * $resize_factor * $item['size_y']);
			}
			
			//if (isset($item['img'])) $item['img'] = $this->model_tool_nicoimage->cropsize($item['img'], $data['row_height'] * 15 * $item['size_x'] , $data['row_height'] * 15 * $item['size_y']);
			
			$item['title'] = isset($item['title_' . $code])?$item['title_' . $code]:'';
			$item['subtitle'] = isset($item['subtitle_' . $code])?$item['subtitle_' . $code]:'';
			$item['button'] = isset($item['button_' . $code])?$item['button_' . $code]:'';
			$item['url'] = isset($item['url_' . $code])?$item['url_' . $code]:'';
		}

		$data['max_row'] = $max_row;
		$data['group'] = $grid;
		
		
		$data['_this'] = $this;
		$data['button_cart'] = $this->language->get('button_cart');

		$opencart_version = (int)str_replace('.','',VERSION);

		if ($opencart_version >= 2000)
		{
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/nicogrid.tpl')) 
			{
				return $this->load->view($this->config->get('config_template') . '/template/module/nicogrid.tpl', $data);
			} else {
				return $this->load->view('default/template/module/nicogrid.tpl', $data);
			}
		} else
		{
			$this->data = $data;
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/nicogrid.tpl')) 
			{
				$this->template = $this->config->get('config_template') . '/template/module/nicogrid.tpl';
			} else {
				$this->template = 'default/template/module/nicogrid.tpl';
			}

			$this->render();
		}
	}
}
