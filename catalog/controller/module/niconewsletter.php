<?php
include_once('nicomodule.inc'); 
class ControllerModuleNiconewsletter extends NicoModule
{
	public function index($setting) 
	{
		if (!$this->is_filter_ok($setting)) return false;

		$this->load->model('tool/image');
		
		$data = $setting;
		if (!isset($data['position'])) $data['position'] = rand(0, 10);
		if (!isset($data['layout_id'])) $data['layout_id'] = 0;
		if (!isset($data['sort_order'])) $data['sort_order'] = rand(0, 10);

		$data['type'] = $setting['type'];
		$data['mailchimp_key'] = $setting['mailchimp_key'];

		$opencart2 = ((int)substr(VERSION,0,1) == 2);

		if ($opencart2)
		{
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/niconewsletter.tpl')) 
			{
				return $this->load->view($this->config->get('config_template') . '/template/module/niconewsletter.tpl', $data);
			} else {
				return $this->load->view('default/template/module/niconewsletter.tpl', $data);
			}
		} else
		{
			$this->data = $data;
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/niconewsletter.tpl')) 
			{
				$this->template = $this->config->get('config_template') . '/template/module/niconewsletter.tpl';
			} else {
				$this->template = 'default/template/module/niconewsletter.tpl';
			}

			$this->render();
		}
	}
}
?>
