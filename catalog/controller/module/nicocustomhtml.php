<?php
include_once('nicomodule.inc');
class ControllerModuleNicocustomhtml  extends NicoModule
{
	public function index($setting) 
	{
		if (!$this->is_filter_ok($setting)) return false;
		$this->load->model('tool/image');
		$opencart2 = ((int)substr(VERSION,0,1) == 2);
		$lang_code = $this->language->get('code');
		$data = $setting;		
		if (!isset($data['position'])) $data['position'] = rand(0, 10);
		if (!isset($data['layout_id'])) $data['layout_id'] = 0;
		if (!isset($data['sort_order'])) $data['sort_order'] = rand(0, 10);
		
		$data['content'] = isset($setting['content'][$lang_code])?html_entity_decode($setting['content'][$lang_code]):'';

		if ($opencart2)
		{
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/nicocustomhtml.tpl')) 
			{
				return $this->load->view($this->config->get('config_template') . '/template/module/nicocustomhtml.tpl', $data);
			} else {
				return $this->load->view('default/template/module/nicocustomhtml.tpl', $data);
			}
		} else
		{
			$this->data = $data;
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/nicocustomhtml.tpl')) 
			{
				$this->template = $this->config->get('config_template') . '/template/module/nicocustomhtml.tpl';
			} else {
				$this->template = 'default/template/module/nicocustomhtml.tpl';
			}

			$this->render();
		}
	}
}
?>
