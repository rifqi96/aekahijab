<?php
class NicoModule extends Controller 
{
	function init(&$data)
	{
		global $_nico_module;

		$data['opencart_version'] = (int)str_replace('.','',VERSION);

  		$data['breadcrumbs'] = array();

		$data['modules'] = array();
		
		if ($data['opencart_version'] > 1564)
		{
			$data['home'] = 'common/dashboard';
		} else
		{
			$data['home'] = 'common/home';
		}

		$data['layouts'] = $this->layout();
		//$data['position'] = $this->position();
		
		if ($data['opencart_version'] > 1564)
		{
			$this->language->load('module/' . $_nico_module);
		} else
		{
			$this->load->language('module/'. $_nico_module);
		}

		if ($data['opencart_version'] > 2000 && $_nico_module != 'nicocontrolpanel')
		{
			$this->load->model('extension/module');

			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
				if (!isset($this->request->get['module_id'])) {
					$this->model_extension_module->addModule($_nico_module, $this->request->post);
				} else {
					$this->model_extension_module->editModule($this->request->get['module_id'], $this->request->post);
				}
							
				$this->session->data['success'] = $this->language->get('text_success');

				$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
			}
		} else
		{
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {			
			$this->model_setting_setting->editSetting($_nico_module, $this->request->post);		
			$this->session->data['success'] = $this->language->get('text_success');
			if ($data['opencart_version'] > 1564)
			{
				$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
			} else
			{
				$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
			}

		}
				
			if (isset($this->request->post[$_nico_module  . '_status'])) {
				$data[$_nico_module  . '_status'] = $this->request->post[$_nico_module  . '_status'];
			} else {
				$data[$_nico_module  . '_status'] = $this->config->get($_nico_module  . '_status');
			}
		}

		$this->document->setTitle(strip_tags($this->language->get('heading_title')));

		$data['heading_title'] = strip_tags($this->language->get('heading_title'));		
		
		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->error['image'])) {
			$data['error_image'] = $this->error['image'];
		} else {
			$data['error_image'] = array();
		}
				

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link($data['home'], 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/' . $_nico_module, 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		

		if (!isset($this->request->get['module_id'])) {
		$data['action'] = $this->url->link('module/' . $_nico_module, 'token=' . $this->session->data['token'], 'SSL');
		} else {
			$data['action'] = $this->url->link('module/' . $_nico_module, 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], 'SSL');
		}
		
		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		$data['token'] = $this->session->data['token'];

		if ($data['opencart_version'] > 2000 && $_nico_module != 'nicocontrolpanel')
		{
			if (isset($this->request->get['module_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) 
			{
				$module_info = $this->model_extension_module->getModule($this->request->get['module_id']);
				$data = array_merge($data, $module_info);
			}
		} else
		{
		if (isset($this->request->post[$_nico_module . '_module'])) {
			$data['modules'] = $this->request->post[$_nico_module . '_module'];
		} elseif ($this->config->get($_nico_module . '_module')) { 
			$data['modules'] = $this->config->get($_nico_module . '_module');
		}		
		}

		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($module_info)) {
			$data['name'] = $module_info['name'];
		} else {
			$data['name'] = '';
		}

		$this->load->model('localisation/language');
		
		$data['languages'] = $this->model_localisation_language->getLanguages();
		$data['module'] = &$data;
		if ($data['opencart_version'] > 1564)
		{
			$data['header'] = $this->load->controller('common/header');
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['footer'] = $this->load->controller('common/footer');

			$data['text_edit'] = $this->language->get('text_edit');
			$data['text_enabled'] = $this->language->get('text_enabled');
			$data['text_disabled'] = $this->language->get('text_disabled');
			
			$data['entry_heading'] = $this->language->get('entry_heading');
			$data['entry_name'] = $this->language->get('entry_name');
			$data['entry_description'] = $this->language->get('entry_description');
			$data['entry_status'] = $this->language->get('entry_status');
			
			$data['button_save'] = $this->language->get('button_save');
			$data['button_cancel'] = $this->language->get('button_cancel');
			$data['button_module_add'] = $this->language->get('button_module_add');
			$data['button_remove'] = $this->language->get('button_remove');

			$data['tab_module'] = $this->language->get('tab_module');
		} else
		{
			$this->children = array(
				'common/header',
				'common/footer'
			);
		}
	}
	
   function layout()
   {
	    
	   $_layouts = array(99999 => 'All pages');
	   
		$this->load->model('design/layout');

		$layouts = $this->model_design_layout->getLayouts();	   
		if (is_array($layouts))
		foreach ($layouts as $layout) 
		{
			$_layouts[$layout['layout_id']] = $layout['name'];
		}	

		return $_layouts;
	}
	
	/*
   function position()
   {
	  return array(
			'menu' => 'Menu',
			'slideshow' => 'Slideshow',
			'footer_top' => 'Footer top',
			'content_top' => 'Content top',
			'content_bottom' => 'Content bottom',
			'column_left' => 'Column left',
			'column_right' => 'Column right',

			'menu' => 'Menu',
			'slideshow' => 'Slideshow',
			'footer_top' => 'Footer top',
			);
	}*/
	
	function products(&$data)
	{
		$data['products'] = array();
							
		$this->load->model('catalog/product');

		if (isset($data['modules']) && !empty($data['modules']))
		foreach ($data['modules'] as $module_row => $module)
		{
			if (isset($module['section']))
			foreach ($module['section'] as $section_nr => $section)
			{
				$products[$module_row][$section_nr] = explode(',', $section['product_list']);
			}
		
		if (isset($products))
		foreach ($products as $module_row => $sections) 
		{
			foreach ($sections as $section_nr => $section)
			{
				foreach ($section as $product_id) 
				{
					$product_info = $this->model_catalog_product->getProduct($product_id);
					
					if ($product_info) {
						$data['products'][$module_row][$section_nr][] = array(
							'product_id' => $product_info['product_id'],
							'name'       => $product_info['name']
						);
					}
				}	
			}
		}

		} else
		{
			if (isset($data['section']) && !empty($data['section']))
			foreach ($data['section'] as $section_nr => $section)
			{
				$products[$section_nr] = explode(',', $section['product_list']);
			}


			if (isset($products))
			foreach ($products as $section_nr => $section)
			{
				foreach ($section as $product_id) 
				{
					$product_info = $this->model_catalog_product->getProduct($product_id);
					
					if ($product_info) {
						$data['products'][$section_nr][] = array(
							'product_id' => $product_info['product_id'],
							'name'       => $product_info['name']
						);
					}
				}	
			}
		}

		
	}

	function categories(&$data)
	{
		//categories

		$data['categories'] = array();

		$this->load->model('catalog/category');

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			//$total = $this->model_catalog_product->getTotalProducts(array('filter_category_id' => $category['category_id']));

			$children_data = array();

			$children = $this->model_catalog_category->getCategories($category['category_id']);

			
			foreach ($children as $child) {
				//$product_total = $this->model_catalog_product->getTotalProducts($data);

				//$total += $product_total;
				/*
				$children_data[] = array(
					'category_id' => $child['category_id'],
					'name'        => $child['name'],
					'href'        => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])	
				);		
				*/ 
			}

			$data['categories'][$category['category_id']] = $category['name'];
		}
			
	}
	
	function manufacturers(&$data)
	{
		//manufacturers
		
		$this->load->model('catalog/manufacturer');
		
		$data['manufacturers'] = array();

		$manufacturer_total = $this->model_catalog_manufacturer->getTotalManufacturers();
	
		$results = $this->model_catalog_manufacturer->getManufacturers();
 
    	foreach ($results as $result) {
			$action = array();
			$data['manufacturers'][$result['manufacturer_id']] = $result['name'];
		}
	}	
}
?>
