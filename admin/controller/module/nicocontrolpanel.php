<?php
include('nicomodule.inc');
global $_nico_module;
$_nico_module = 'nicocontrolpanel';

class ControllerModuleNicocontrolpanel extends NicoModule 
{
	private $error = array(); 
	
	public function index() {   
		$data = array();
		$this->categories($data);
		$this->manufacturers($data);
		
		$this->init($data);
				
		if ($data['opencart_version'] > 1564)
		{
			$this->response->setOutput($this->load->view('module/nicocontrolpanel.tpl', $data));
		} else
		{
			$this->template = 'module/nicocontrolpanel.tpl';
			$this->data = $data;
			$this->response->setOutput($this->render());
		}
	}
	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/nicocontrolpanel')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>
