<?php
include('nicomodule.inc');
global $_nico_module;
$_nico_module = 'nicogrid';

class ControllerModuleNicogrid extends NicoModule {
	private $error = array(); 
	
	public function index() 
	{   
		//echo $this->config->get('config_template'); 
		$this->init($data);
		$this->categories($data);
		$this->manufacturers($data);
		
		$this->load->model('tool/image');
		//$this->load->model('tool/nicoimage');
		
		//var_dump($data);
		//var_dump($_POST);
		//var_dump($data['modules'][1]['menu']);
		foreach($data['modules'] as $nr => $module)
		{
			$json = json_decode(html_entity_decode($data['modules'][$nr]['grid']));
			//var_dump($data['modules'][$nr]['grid']);
			//var_dump($json);
			foreach ($json as $grid_element)
			{
				//generate thumb if not available
				$this->model_tool_image->resize(utf8_substr(DIR_IMAGE . $grid_element->img, utf8_strlen(DIR_IMAGE)), 100, 100);
			}
			//'thumb' => $this->model_tool_image->resize(utf8_substr($image, utf8_strlen(DIR_IMAGE)), 100, 100),
			//if (isset($item['img'])) $item['img'] = $this->model_tool_nicoimage->cropsize($item['img'], 100 * $item['size_x'] , 100 * $item['size_y']);
		}

		
		
		/*
		
		$data['modules'][1] = array('menu' =>
		  array (
			0 => 
			array (
			  'url' => 'index.php?route=pavblog/blogs',
			  'text' => 
			  array (
				'ro' => 'Blog',
				'en' => 'Blog',
			  ),
			),
			1 => 
			array (
			  'url' => '/index.php?route=product/category&amp;path=20_27',
			  'text' => 
			  array (
				'en' => 'Custom menu',
			  ),
			  'children' => 
			  array (
				0 => 
				array (
				  'url' => '/index.php?route=product/category&amp;path=20_27',
				  'text' => 
				  array (
					'en' => 'Subcat 1',
				  ),
				),
				1 => 
				array (
				  'url' => '/index.php?route=product/category&amp;path=20_27',
				  'text' => 
				  array (
					'en' => 'Subcat 2',
				  ),
				),
				2 => 
				array (
				  'url' => '/index.php?route=product/category&amp;path=20_27',
				  'text' => 
				  array (
					'en' => 'Subcat 3',
				  ),
				  
				    'children' => 
			  array (
				0 => 
				array (
				  'url' => '/index.php?route=product/category&amp;path=20_27',
				  'text' => 
				  array (
					'en' => 'Subcat 1',
				  ),
				),
				1 => 
				array (
				  'url' => '/index.php?route=product/category&amp;path=20_27',
				  'text' => 
				  array (
					'en' => 'Subcat 2',
				  ),
				),
				2 => 
				array (
				  'url' => '/index.php?route=product/category&amp;path=20_27',
				  'text' => 
				  array (
					'en' => 'Subcat 3 3',
				  ),
				)),
			  
				  
				),
			  ),
			),
			2 => 
			array (
			  'url' => '/index.php?route=product/category&amp;path=20_27',
			  'text' => 
			  array (
				'en' => 'Custom link',
			  ),
			),
			3 => 
			array (
			  'text' => 
			  array (
				'en' => 'Categories',
			  ),
			  'children' => 
			  array (
				0 => 
				array (
				  'categories' => 'true',
				),
			  ),
			),
		  ));
		  */
//		  var_dump($data);
				
		if ($data['opencart_version'] > 1564)
		{
			$this->response->setOutput($this->load->view('module/nicogrid.tpl', $data));
		} else
		{
			$this->template = 'module/nicogrid.tpl';
			$this->data = &$data;
			$this->response->setOutput($this->render());
		}
	}
	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/nicogrid')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
				
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>
