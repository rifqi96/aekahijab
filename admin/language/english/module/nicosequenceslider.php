<?php
global $nico_theme_name, $nico_theme_positions, $theme_name, $db, $_config, $nico_config, $load, $registry, $opencart_version;
if (!isset($opencart_version)) $opencart_version = (int)str_replace('.','',VERSION);
$db = $registry->get('db');
if (!$db)
{
	$db = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PREFIX);
}
$query = $db->query('SELECT value FROM ' . DB_PREFIX . 'setting WHERE `key` = \'config_template\' AND `store_id` = 0 LIMIT 1');
$theme_name = $query->rows[0]['value'];

$theme_config_file = dirname($_SERVER['SCRIPT_FILENAME']) . '/../catalog/view/theme/' . $theme_name . '/nico_theme_editor/theme_config.inc';
if (file_exists($theme_config_file)) include_once($theme_config_file);
if (empty($nico_theme_name)) $nico_theme_name = '<span style="color:red">Nico theme not installed</span>';

// Heading
$_['heading_title']       = '<b style="color:#3860BB;"><img style="vertical-align:middle;margin:0px 10px 0px 0px;width:16px;" src="view/image/nicomodules/sequence.png"> ' .  $nico_theme_name . ' - Sequencejs Slider</b>';
$_['entry_name']       = 'Module Name';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module nicosequenceslider!';
if ($opencart_version > 1564)
$_['text_edit']         = 'Edit '. $nico_theme_name.' Sequence Slider';
else
$_['text_edit']         = 'Edit';


// Error 
$_['error_permission']    = 'Warning: You do not have permission to modify module nicosequenceslider!';
$_['error_image']         = 'Image width &amp; height dimensions required!';

$_['entry_status']      = 'Status';
$_['entry_heading']     = 'Heading Title';
$_['entry_description'] = 'Content';
