<?php 
global $module_name,  $autocomplete_products, $module_row, $module_name, $module_config, $config_size, $languages, $section_config, $section_text, $_modules, $nico_theme_positions, $theme_name, $opencart_version;
$_modules = $modules;
$module_name = 'nicocontrolpanel';
$section_text = 'slide';
$theme_config = dirname($_SERVER['SCRIPT_FILENAME']) . '/../catalog/view/theme/' . $theme_name . '/nico_theme_editor/panel_config.inc';

if (!file_exists($theme_config))
{
	echo $header;
	die('<h2 style="text-align:center;padding:30px;">Theme not installed!</h2>' . $footer);
}


include($theme_config);

if ($opencart_version >= 2010) 
{
	require('nicomodule2.tpl');
} else
if ($opencart_version >= 2000) 
{
	require('nicomodule2.tpl');
} else
{
	require('nicomodule.tpl');
}
