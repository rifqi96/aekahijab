<?php 
global $module_name,  $autocomplete_products, $module_row, $module_name, $module_config, $config_size, $languages, $section_config, $section_text, $font_awesome_icons, $nico_theme_positions, $opencart_version;
$autocomplete_products = $products;

$module_name = 'nicocustomproducts';
$section_text = 'section';

$module_config = array(
	'module_cols_xs'=>array('select', 
				array('12' => '1',
					  '6' => '2',
					  '4' => '3',
					  '3' => '4',
					  '2' => '6',
					  '1' => '12',
					  ),
				'Products per row<span class="help">(extra small devices, phones)</span>', 12
			),
			
	'module_cols_sm'=>array('select', 
				array('12' => '1',
					  '6' => '2',
					  '4' => '3',
					  '3' => '4',
					  '2' => '6',
					  '1' => '12',
					  ),
				'Products per row<span class="help">(small devices, tablets)</span>', 12
			),			

	'module_cols_md'=>array('select', 
				array('12' => '1',
					  '6' => '2',
					  '4' => '3',
					  '3' => '4',
					  '2' => '6',
					  '1' => '12',
					  ),
				'Products per row<span class="help">(medium devices, desktops)</span>', 3
			),			

	'module_cols_lg'=>array('select', 
				array('12' => '1',
					  '6' => '2',
					  '4' => '3',
					  '3' => '4',
					  '2' => '6',
					  '1' => '12',
					  ),
				'Products per row<span class="help">(large devices, desktops)</span>', 3
			),			


			
	'additional_image'=> array('select', 
					array(
						0 => 'Disabled',
						1 => 'Enabled'
						),
					'Change image on hover'
				),

	'resize_method'=>array('select', 
					array('default' => 'Opencart default',
						  'cropresize' => 'Crop and resize',
						  ),
					'Product image resize method'
				),
	'image_width'=>array('input',null,'Image width', 200),
	'image_height'=>array('input',null,'Image Height', 256),

	'type'=>array('select', 
					array('filter' => 'Filter',
						  'tabs' => 'Tabs',
						  /*'accordion' => 'Accordion',*/
						  'carousel' => 'Carousel',
						  'tabs_carousel' => 'Tabs + Carousel',
						  'grid' => 'Grid',
						  'list' => 'List',
						  ),
					'Type'
				),
	'title'=>array('multilanguage',null,'Module title'),
	'section' => array('section'),
);

$section_config = array(
	'title'=>array('multilanguage',null,'Section title'),
	'section_type'=>array('select', 
					array('autocomplete' => 'Autocomplete',
						  'category' => 'Category',
						  'manufacturer' => 'Manufacturer',
						  'latest' => 'Latest',
						  'bestsellers' => 'Bestsellers',
						  'specials' => 'Specials',
						  'popular' => 'Popular',
						  'recentlyviewed' => 'Recentlyviewed',
						  'random' => 'Random',
						  'alsobought' => 'Alsobought',
						  'related' => 'Related',
						  ),
					'Get products from'
				),

	'autocomplete_section'=>array('section_type'),
	'product_list'=>array('autocomplete',null,'Products'),


	'category_section'=>array('section_type'),
	'category'=>array('select', $categories,
			'Category'
		),
		
	'category_sort'=>array('select', 
			array('asc' => 'ASC',
				  'desc' => 'DESC'
				  ),
			'Sort order'
		),
	'category_limit'=>array('input',null,'Limit'),


	'manufacturer_section'=>array('section_type'),
	'manufacturer'=>array('select', $manufacturers,
			'Manufacturer'
		),
	'manufacturer_sort'=>array('select', 
			array('asc' => 'ASC',
				  'desc' => 'DESC'
				  ),
			'Sort order'
		),
	'manufacturer_limit'=>array('input',null,'Limit', 5),


	'latest_section'=>array('section_type'),
	'latest_limit'=>array('input',null,'Limit', 5),

	'bestsellers_section'=>array('section_type'),
	'bestsellers_limit'=>array('input',null,'Limit', 5),


	'specials_section'=>array('section_type'),
	'specials_limit'=>array('input',null,'Limit', 5),


	'popular_section'=>array('section_type'),
	'popular_limit'=>array('input',null,'Limit', 5),


	'recentlyviewed_section'=>array('section_type'),
	'recentlyviewed_limit'=>array('input',null,'Limit', 5),

	'random_section'=>array('section_type'),
	'random_limit'=>array('input',null,'Limit', 5),
	'random_cache'=>array('select', 
				array('true' => 'True',
					  'false' => 'False'
					  ),
				'Cache results', 'true'
			),
	'related_section'=>array('section_type'),
	'related_limit'=>array('input',null,'Limit', 5),

	'alsobought_section'=>array('section_type'),
	'alsobought_limit'=>array('input',null,'Limit',5),
);				

if ($opencart_version >= 2010) 
{
	require('nicomodule2010.tpl');
} else
if ($opencart_version >= 2000) 
{
	require('nicomodule2.tpl');
} else
{
	require('nicomodule.tpl');
}
