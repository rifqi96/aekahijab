<?php 
global $module_name,  $autocomplete_products, $module_row, $module_name, $module_config, $config_size, $languages, $section_config, $section_text, $font_awesome_icons, $nico_theme_positions, $opencart_version;
if (!isset($opencart_version)) $opencart_version = (int)str_replace('.','',VERSION);
$categs = $categories;
$module_name = 'nicogrid';

$module_config = array(
	'resize_method'=>array('select', 
					array('default' => 'Opencart default',
						  'cropresize' => 'Crop and resize',
						  'original' => 'Original size',
						  ),
					'Background image resize method'
				),

	'resize_method_top_image'=>array('select', 
					array('default' => 'Opencart default',
						  'cropresize' => 'Crop and resize',
						  'original' => 'Original size',
						  ),
					'Image resize method'
				),


	'resize_factor'=>array('input',null,'Resize factor <span class="help">(higher values makes images sharper but increases image size)</span>', 15),
	'row_height'=>array('input',null,'Row height factor', 5),
/*
	'width'=>array('input',null,'Width', 1140),
	'height'=>array('input',null,'Height', 700),

	'medium_width'=>array('input',null,'Medium width', 992),
	'medium_height'=>array('input',null,'Height', 609),

	'small_width'=>array('input',null,'Small width', 768),
	'small_height'=>array('input',null,'Height', 472),

	'extrasmall_width'=>array('input',null,'Extra small width', 480),
	'extrasmall_height'=>array('input',null,'Height', 295),

	'tiny_width'=>array('input',null,'Tiny width', 320),
	'tiny_height'=>array('input',null,'Height', 196),
*/
	'margin_top'=>array('input',null,'Margin top', 5),
	'margin_left'=>array('input',null,'Margin left', 5),

	'section' => array('custom', 'grid', 'Grid'),
);


function grid($module)
{
	global $languages, $module_row, $categs, $_module_row, $opencart_version;
	if ($module !== true)
	{
		$_module_row = $module_row;
	} else
	{
		$_module_row = '__MODULE_ROW__';
	}
	
	?>
	<p>You can drag and drop and resize the boxes</p>
	
	<div class="nico_grid" row="<?php echo $_module_row;?>">

	    <div class="gridster">
		<ul>
		</ul>
	    </div>

	</div>
	
	<a class="btn btn-primary nico_add_grid_item">Add new grid element</a>
	<input class="grid_data" type="hidden" name="<?php if ($opencart_version > 2000) {?>grid<?php } else {?>nicogrid_module[<?php echo $_module_row;?>][grid]<?php }?>" value="<?php if (isset($module['grid']) && $module['grid']) echo $module['grid'];?>">
	<?php
}

ob_start();
?>

<div class="grid_template" style="display:none">
    <div>
	<!-- select class="grid_element_type">
	    <option value="image">image</option>
	    <option value="color">color</option>
	</select -->
	<div class="image">
			<?php if ($opencart_version > 1564) { ?>
			<a href="#" onclick="window.getSelection().removeAllRanges();" id="thumb-#col-#row-image" class="img-thumbnail" data-toggle="image" data-original-title="">
				<img src="../image/cache/no_image-100x100.png" data-placeholder=="../image/cache/no_image-100x100.png" alt="" title="" id="thumb_#col-#row-image" class="thumb thumb-img" />
			</a>
		    <input type="hidden" class="img"  id="input-#col-#row-image" value="#img">                
		    <input type="hidden" class="img"  id="image#col-#row-image" value="#img">                
		    <input type="hidden" class="thumb" id="thumb_#col-#row-image" value="#thumb">                
		    <!-- input type="hidden" class="thumb" id="thumb_#col-#row-image" value="#thumb" -->                
			<?php } else { ?>
		    <img id="thumb#col-#row-image" class="thumb-img" alt="" src="../image/cache/no_image-100x100.jpg">
		    <input type="hidden" class="img"  id="image#col-#row-image" value="#img">                
		    <input type="hidden" class="thumb" id="thumb_#col-#row-image" value="#thumb">                
		    <a onclick="image_upload('#col', '#row','image');">Browse</a>&nbsp;&nbsp;|&nbsp;&nbsp;
		    <a onclick="$('#thumb#col-#row-image').attr('src', '../image/cache/no_image-100x100.jpg'); $('#image#col-#row-image, #thumb_#col-#row-image').attr('value', '');">Clear</a>                
			<?php } ?>
	</div>
	<!-- div class="color">
	    <input type="text" class="colorpicker" size=8 value="#clor" color="#clor" style="background-color:#clor"/>
	</div -->

	<input type="hidden" _name="top_image" value="#top_image">
	<input type="hidden" _name="top_thumb" value="#top_thumb">
	<input type="hidden" _name="image_position" value="#image_position">
	<input type="hidden" _name="background" value="#background">
	
	<?php foreach ($languages as $language) { ?>
	<input type="hidden" _name="subtitle_<?php echo str_replace('-', '$',$language['code']); ?>" value="#subtitle_<?php echo str_replace('-', '$',$language['code']); ?>">
	<input type="hidden" _name="title_<?php echo str_replace('-', '$',$language['code']); ?>" value="#title_<?php echo str_replace('-', '$',$language['code']); ?>">
	<input type="hidden" _name="button_<?php echo str_replace('-', '$',$language['code']); ?>" value="#button_<?php echo str_replace('-', '$',$language['code']); ?>">
	<input type="hidden" _name="url_<?php echo str_replace('-', '$',$language['code']); ?>" value="#url_<?php echo str_replace('-', '$',$language['code']); ?>">
    <?php } ?> 
    
	<a href="#" class="edit_text btn btn btn-primary">edit data</a><br/>
    <a href="#" class="remove_grid_element">remove</a>
    </div>
</div>


  <!-- Modal -->
  <div class="modal" id="gridmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none">
    <div class="modal-dialog">
      <div class="modal-content">
		<div>
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Grid item</h4>
        </div>
        <div class="modal-body">

		<table>
		<?php foreach ($languages as $language) { ?>
		<tr>
		<td><label for="name"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <span><?php echo $language['name']; ?> - Subtitle</span></label></td>
		<td><input type="text" _name="subtitle_<?php echo str_replace('-', '$',$language['code']); ?>" id="subtitle" class="text ui-widget-content ui-corner-all"></td>
		</tr>	
		<tr>
		<td><label for="title"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <span><?php echo $language['name']; ?> - Title</span></label></td>
		<td><input type="text" _name="title_<?php echo str_replace('-', '$',$language['code']); ?>" id="title" value="" class="text ui-widget-content ui-corner-all"></td>
		</tr><tr>
		<td><label for="title"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <span><?php echo $language['name']; ?> - Button</span></label></td>
		<td><input type="text" _name="button_<?php echo str_replace('-', '$',$language['code']); ?>" id="button" value="" class="text ui-widget-content ui-corner-all"></td>
		</tr><tr>
		<td><label for="title"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <span><?php echo $language['name']; ?> - Url</span></label></td>
		<td><input type="text" _name="url_<?php echo str_replace('-', '$',$language['code']); ?>" id="url" value="" class="text ui-widget-content ui-corner-all"></td>
		</tr>
		<?php } ?> 

		</tr><tr>
		<td><label for="title">Top image</span></label></td>
		<td>
			<?php if ($opencart_version > 1564) { ?>
			<a href="#" onclick="window.getSelection().removeAllRanges();" id="thumb-1-1-top_image" class="img-thumbnail" data-toggle="image" data-original-title="">
				<img src="../image/cache/no_image-100x100.png" data-placeholder=="../image/cache/no_image-100x100.png" alt="" title="" id="thumb_1-1-top_image" class="thumb thumb-img" />
			</a>
		    <input type="hidden" _name="top_image" class="img"  id="input-1-1-top_image" value="#img">                
		    <input type="hidden" _name="top_image" class="img"  id="image-1-1-top_image" value="#top_img">                
		    <input type="hidden" _name="top_thumb" class="thumb" id="thumb-1-1-top_image" value="#top_thumb">                
		    <!-- input type="hidden" class="thumb" id="thumb_1-1-image" value="#thumb" -->                
			<?php } else { ?>
		    <img id="thumb1-1-top_image" class="thumb-img" alt="" src="../image/cache/no_image-100x100.jpg">
		    <input type="hidden" _name="top_image" class="img"  id="image1-1-top_image" value="#top_img">                
		    <input type="hidden" _name="top_thumb" class="thumb" id="thumb_1-1-top_image" value="#top_thumb">                
		    <a onclick="image_upload('1', '1','top_image');">Browse</a>&nbsp;&nbsp;|&nbsp;&nbsp;
		    <a onclick="$('#thumb1-1-top_image').attr('src', '../image/cache/no_image-100x100.jpg'); $('#image1-1-top_image, #thumb_1-1-top_').attr('value', '');">Clear</a>                
			<?php } ?>
			
			</td>
		</tr><tr>
		<td><label for="title">Image position</span></label></td>
		<td>
			<select _name="image_position">
				<option value="top_left">Top left</option>
				<option value="top_left">Top right</option>
				<option value="bottom_left">Bottom left</option>
				<option value="bottom_right">Bottom right</option>
				<option value="top_center">Top center</option>
				<option value="top_bottom">Top bottom</option>
			</select>
		</td>
		</tr>
		<td><label for="title">Background color</span></label></td>
		<td>
			<input type="text" class="colorpicker" _name="background" value=""/>
		</td>
		</tr>
		</table>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->


<div id="dialog-form" title="Edit grid item text" style="display:none">
<form>
<table>
<?php foreach ($languages as $language) { ?>
<tr>
<td><label for="name"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <span><?php echo $language['name']; ?> - Subtitle</span></label></td>
<td><input type="text" _name="subtitle_<?php echo str_replace('-', '$',$language['code']); ?>" id="subtitle" class="text ui-widget-content ui-corner-all"></td>
</tr>	
<tr>
<td><label for="title"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <span><?php echo $language['name']; ?> - Title</span></label></td>
<td><input type="text" _name="title_<?php echo str_replace('-', '$',$language['code']); ?>" id="title" value="" class="text ui-widget-content ui-corner-all"></td>
</tr><tr>
<td><label for="title"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <span><?php echo $language['name']; ?> - Button</span></label></td>
<td><input type="text" _name="button_<?php echo str_replace('-', '$',$language['code']); ?>" id="button" value="" class="text ui-widget-content ui-corner-all"></td>
</tr><tr>
<td><label for="title"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <span><?php echo $language['name']; ?> - Url</span></label></td>
<td><input type="text" _name="url_<?php echo str_replace('-', '$',$language['code']); ?>" id="url" value="" class="text ui-widget-content ui-corner-all"></td>
</tr>
<?php } ?> 
</table>
</form>
</div>

<link rel="stylesheet" href="view/javascript/jquery/gridster/jquery.gridster.min.css" type="text/css" />
<script src="view/javascript/jquery/gridster/jquery.gridster.min.js"></script>
<script type="text/javascript">
   	  var reg = /^.*image\//i;
      var grid_template = jQuery(".grid_template").html();
      var current_edit_node;
      //var gridster;

      // same object than generated with gridster.serialize() method
      var serialization = [
        {
            col: 1,
            row: 1,
            size_x: 2,
            size_y: 2
        }
      ];
      
      function grid(element, serialization)
      {
	gridster = $(".gridster ul", element).data('gridster'); 
	
	if (typeof gridster == 'undefined' || !gridster)
	{
	    gridster = $(".gridster ul", element).gridster({
	      widget_base_dimensions: [95, 95],
	      widget_margins: [2, 2],
	      min_cols:12,
	      max_cols:12,
	      helper: 'clone',
	      resize: 
	      {
		enabled: true
	      },
	      serialize_params: function ($w, wgd) {
		return {
		    id: wgd.el[0].id,
		    col: wgd.col,
		    row: wgd.row,
		    size_y:wgd.size_y,
		    size_x:wgd.size_x,
		    top_img: $("input[_name='top_image']",$w).val(),
		    top_thumb: $("input[_name='top_thumb']",$w).val().replace(reg,'image/'),
		    image_position: $("input[_name='image_position']",$w).val(),
			background: $("input[_name='background']",$w).val(),
		    img: $('.img',$w).val(),
		    thumb: $('img[id^=thumb]',$w).prop("src").replace(reg,'image/'),
		    color: $('.colorpicker',$w).attr('color'),
		    //type: $('.grid_element_type',$w).val(),
		    <?php foreach ($languages as $language) { ?>
			subtitle_<?php echo str_replace('-', '$',$language['code']); ?>:$("input[_name='subtitle_<?php echo str_replace('-', '$',$language['code']); ?>']",$w).val(),
			title_<?php echo str_replace('-', '$',$language['code']); ?>:$("input[_name='title_<?php echo str_replace('-', '$',$language['code']); ?>']",$w).val(),
			button_<?php echo str_replace('-', '$',$language['code']); ?>:$("input[_name='button_<?php echo str_replace('-', '$',$language['code']); ?>']",$w).val(),
			url_<?php echo str_replace('-', '$',$language['code']); ?>:$("input[_name='url_<?php echo str_replace('-', '$',$language['code']); ?>']",$w).val(),
			<?php } ?>
		};
	    }
	    }).data('gridster');
	    
	    if (serialization)
	    {
		//console.dir(serialization);
		//gridster.remove_all_widgets();
		$.each(serialization, function() {
			this.thumb = this.thumb.replace(reg,'../image/');
		    if (!this.color) this.color = 'rgba(255,255,255,1)';
   			<?php if ($opencart_version > 1564) { ?>
		    if (!this.thumb) this.thumb = '../image/cache/no_image-100x100.png';
		    if (!this.top_thumb) this.top_thumb = '../image/cache/no_image-100x100.png';
		    <?php } else {?>
		    if (!this.thumb) this.thumb = '../image/cache/no_image-100x100.jpg';
		    if (!this.top_thumb) this.top_thumb = '../image/cache/no_image-100x100.jpg';
		    <?php } ?>
		    li = gridster.add_widget('<li>' + 
			grid_template.
			    replace(/#row/g, this.row).
			    replace(/#col/g, this.col).
			    //replace(/#clor/g, this.color).
			    replace(/#top_image/g, this.top_img).
			    replace(/#top_thumb/g, this.top_thumb).
			    replace(/#image_position/g, this.image_position).
			    replace(/#background/g, this.background).
			    replace(/#img/g, this.img).
			    replace(/#thumb/g, this.thumb).
			    <?php foreach ($languages as $language) { ?>
			    replace(/#title_<?php echo str_replace('-', '\$',$language['code']); ?>/g, this.title_<?php echo str_replace('-', '$',$language['code']); ?>).
			    replace(/#subtitle_<?php echo str_replace('-', '\$',$language['code']); ?>/g, this.subtitle_<?php echo str_replace('-', '$',$language['code']); ?>).
			    replace(/#button_<?php echo str_replace('-', '\$',$language['code']); ?>/g, this.button_<?php echo str_replace('-', '$',$language['code']); ?>).
			    replace(/#url_<?php echo str_replace('-', '\$',$language['code']); ?>/g, this.url_<?php echo str_replace('-', '$',$language['code']); ?>).
			    <?php } ?>
				<?php if ($opencart_version > 1564) { ?>
			    replace(/..\/image\/cache\/no_image-100x100.png/g, this.thumb) 
				<?php } else {?>
			    replace(/src=".*\/image\/cache\/no_image-100x100.jpg/g, 'src="' + this.thumb)
				<?php } ?>
			+ '</li>', 
			this.size_x, this.size_y, this.col, this.row);
			
			jQuery('select', li).val(this.type);
			//jQuery('> div > div', li).hide();
			jQuery('> div > div.' + this.type, li).show();
			if (this.background && this.background != 'undefined') li.css('background-color',this.background);
		});
	    }
	} 
	
	return gridster;
      }

    

      $(function(){
	
	$.each(jQuery('.grid_data'), function() 
	{
	    if (this.value) grid(jQuery('.nico_grid', this.parentNode), JSON.parse(this.value));    
	});

	    /*gridData = gridster.serialize();

            gridster.remove_all_widgets();
            $.each(serialization, function() {
                gridster.add_widget('<li>' + grid_template.replace(/#row/g, this.col).replace(/#col/g, this.col) + '</li>', this.size_x, this.size_y, this.col, this.row);
            });*/

	    jQuery("body").delegate(".nico_add_grid_item", "click", function() 
	    {
		gridster = grid(jQuery('.nico_grid', this.parentNode));
		
		gridster.add_widget.apply(gridster,  ['<li>' + grid_template.replace(/#row/g, Math.floor(Math.random() * 1000)).replace(/#col/g, Math.floor(Math.random() * 1000)) + '</li>', 1, 2]);
//		$('.gridster .colorpicker').colorpicker(picker_options);	
	    });
	    
	    jQuery(".gridster").delegate('.remove_grid_element', "click", function(e) 
	    {
			gridster.remove_widget(this.parentNode.parentNode);
			return false;
	    });
	    
	    /*jQuery(".gridster").delegate('.grid_element_type', "change", function() 
	    {
		parent = this.parentNode;
		jQuery('div', parent).hide();
		jQuery('.' + this.value, parent).show();
	    });*/
	    
//	    $('.gridster .colorpicker').colorpicker(picker_options);	
	    
      });
      
      
      function grid_modal(action)
      {
		  if (action == "show")
		  {
			jQuery("#gridmodal").show();  
		  } else
		  {
			jQuery("#gridmodal").hide();
		  }
	  }
			  
		<?php /* if ($opencart_version > 1564) { ?>
		<?php } else {?>
		$( "#dialog-form" ).dialog({
		autoOpen: false,
		height: 300,
		width: 350,
		modal: true,
		buttons: {
		"Ok": function() 
		{
			jQuery("#dialog-form input").each(function () 
			{
				//console.log('input[name=\'' + this.name + '\']');
				jQuery('input[_name=\'' + jQuery(this).attr('_name') + '\']', current_edit_node).val(this.value);
			});
			
			$( this ).dialog( "close" );
		}
		},
		Cancel: function() {
			$( this ).dialog( "close" );
		},
		close: function() {
		}
		});
		<?php } */?>
		
		jQuery("body").delegate("#gridmodal .btn-primary", "click", function(e) 
		{
			jQuery("#gridmodal input,#gridmodal select").each(function () 
			{
				//console.log('input[name=\'' + this.name + '\']');
				jQuery('input[_name=\'' + jQuery(this).attr('_name') + '\']', current_edit_node).val(this.value);
			});
			
			jQuery('input[_name=\'top_image\']', current_edit_node).val(jQuery("#gridmodal input#input-1-1-top_image, #gridmodal input#image1-1-top_image").val());
			thumb = jQuery("#gridmodal img#thumb_1-1-top_image").attr("src");
			if (!thumb) thumb = jQuery("#gridmodal input#thumb_1-1-top_image").val();
			jQuery('input[_name=\'top_thumb\']', current_edit_node).val(thumb);
			jQuery(current_edit_node).css('background-color',jQuery("#gridmodal input[_name='background']").val());
			grid_modal("hide");
			//$("#gridmodal" ).modal( "hide" );
		});
		
		jQuery("body").delegate("#gridmodal .close, #gridmodal .btn-close", "click", function(e) 
		{
			grid_modal("hide");
			//$("#gridmodal" ).modal( "hide" );
		});
		
		jQuery("body").delegate(".edit_text", "click", function(e) 
		{
			current_edit_node = this.parentNode;
			jQuery("#gridmodal input, #gridmodal select").each(function () 
			{
				this.value = jQuery('input[_name=\'' + jQuery(this).attr('_name') + '\']', current_edit_node).val();
			});
			
/*					    <img id="top_thumb#col-#row-image" class="thumb-img" alt="" src="../image/cache/no_image-100x100.jpg">
		    <input type="hidden" _name="top_image" class="img"  id="image#col-#row-image" value="#top_img">                
		    <input type="hidden" _name="top_thumb" class="thumb" id="thumb_#col-#row-image" value="#top_thumb">                
		    <a onclick="image_upload('#col', '#row','image');">Browse</a>&nbsp;&nbsp;|&nbsp;&nbsp;
		    <a onclick="$('#top_thumb#col-#row-image').attr('src', '../image/cache/no_image-100x100.jpg'); $('#top_image#col-#row-image, #top_thumb_#col-#row-image').attr('value', '');">Clear</a>                
*/
			top_thumb = jQuery('input[_name=\'top_thumb\']', current_edit_node).val();
			if (!top_thumb || top_thumb == 'undefined' || top_thumb == '#top_thumb') top_thumb = '../image/cache/no_image-100x100.<?php if ($opencart_version > 1564) { ?>png<?php } else {?>jpg<?php }?>';
			jQuery("#gridmodal td > img, #gridmodal td > a > img").attr("src", top_thumb.replace(reg,'../image/'));
			
			grid_modal("show");
			<?php if ($opencart_version > 1564) { ?>
			//$("#gridmodal" ).modal( "show" );
			<?php } else {?>
			//$("#dialog-form" ).dialog( "open" );
			<?php }?>				
			
			e.preventDefault();
			return false;
		});      

    function before_save()
    {
	
	$(".nico_grid").each(function() 
	{
	    row = jQuery(this).attr("row");
	    var gridster = $(".gridster ul", this).gridster().data('gridster');
	    gridData = gridster.serialize();

	    function sort_grid(a, b){
	      if (a.row > b.row)
		return 1;
	      else if (a.row < b.row)
		return -1;
	      else if (a.col > b.col)
		return 1;
	      else if (a.col < b.col)
		return -1;
	    }
	    
	    gridData.sort(sort_grid);
	    
	    jQuery('<?php if ($opencart_version > 2000) {?>[name="grid"]<?php } else {?>[name="nicogrid_module[' + row + '][grid]"]<?php }?>').val(JSON.stringify(gridData));
	});

	$('#form').submit();
	return false;
    }	      
    </script>
<style>

.gridster, .gridster ul {
    margin: 0;
    padding: 0;
}
ul {
    list-style-type: none;
}
.controls {
    margin-bottom: 20px;
}
.gridster ul {
    background-color: #EFEFEF;
}
.gridster li {
    font-size: 1em;
    font-weight: bold;
    line-height: 100%;
    text-align: center;
}
.gridster {
    margin: 0 auto;
    opacity: 0.8;
    transition: opacity 0.6s ease 0s;
}
.gridster .gs-w {
    background: none repeat scroll 0 0 #fff;
    cursor: pointer;
}

.gridster li a
{
	margin-top:3px;
	font-size:11px;
	display:inline-block;
}
		
.gridster li .image img
{
	display:block;
	margin:0px auto;
	max-width:90%;
}
.gridster li .image
{
	border:none;
	display:block;
	padding:0px 0px 2px;
}


.gridster .player {
    background: none repeat scroll 0 0 #BBBBBB;
}
.gridster .preview-holder {
    background: none repeat scroll 0 0 #FF0000 !important;
    border: medium none !important;
}

.gs-w > div
{
    padding:5px 0px;
}

.gs-w > div > .color
{
    display:none;
}

.nico_grid 
{
  background: #fafafa;
  padding:10px;
}

.btn {
    -moz-user-select: none;
    background-image: none;
    border: 1px solid rgba(0, 0, 0, 0);
    border-radius: 4px;
    cursor: pointer;
    display: inline-block;
    font-size: 14px;
    font-weight: normal;
    line-height: 1.42857;
    margin-bottom: 0;
    padding: 6px 12px;
    text-align: center;
    vertical-align: middle;
    border: medium none;
    border-radius: 4px;
    font-size: 12px;
    text-decoration:none;
}    

.btn-primary  
{
    background-color: #7ABCE7;
    border-bottom: 1px solid #3498DB;
    text-shadow: 0 -1px #3498DB;
    color: #FFFFFF;
}

.img-thumbnail
{
	max-width:90%;
}

#gridmodal
{
	width:100%;
	height:100%;
	position:absolute;
	position:fixed;
	background:#fff;
	background:rgba(255,255,255,0.7);
	top:0px;
	left:0px;
	display:table;
	text-align:center;
    opacity:1;
}

#gridmodal .modal-dialog
{
	display:table-row;
	vertical-align:middle;
	text-align:center;
}

#gridmodal .modal-content
{
	display:table-cell;
	vertical-align:middle;
	text-align:center;
	background:transparent;
	 box-shadow: none;
}

#gridmodal .modal-content > div
{
	text-align:left;
	width:500px;
	margin:0px auto;
	background:#fff;
	padding:0px 20px 30px;
	border:2px solid #ccc;
}

#gridmodal .modal-content > div .modal-footer
{
	text-align:right;
}

#gridmodal .modal-content > div .close
{
	float:right;
	background:#eee;
	border:none;
}


#gridmodal .modal-content > div table
{
	width:100%;
}

#gridmodal .modal-content > div td > img
{
	display:block;
}

.ui-dialog-buttonset, .ui-dialog-titlebar
{
	text-align:right;
}

.ui-dialog-titlebar > span
{
	float:left;
	margin:5px 10px;
}

.ui-button
{
	margin:5px 10px;
	padding:5px 10px;
}

.ui-dialog-content td
{
	padding:5px;
}

.ui-widget-overlay
{
	background:rgba(255,255,255, 0.7);
    display: block;
    height: 100%;
    left: 0;
    position: absolute;
    top: 0;
    width: 100%;
    z-index: 0;	
}

.ui-dialog
{
	background:#eee;
	border:2px solid #ccc;
}

.ui-dialog-content
{
	background:#fff;
}
</style>    
<?php
$module_js = ob_get_contents();
ob_end_clean();

if ($opencart_version >= 2010) 
{
	require('nicomodule2010.tpl');
} else
if ($opencart_version >= 2000) 
{
	require('nicomodule2.tpl');
} else
{
	require('nicomodule.tpl');
}
